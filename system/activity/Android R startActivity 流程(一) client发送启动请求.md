本篇分析client启动Activity到ActivityTaskManagerService处理启动的流程

# Context#startActivity
通常,使用 Context#startActivity 来启动一个Activity, 此方法定义如下,它是一个抽象方法.Context是一个抽象类,它的实现类有ContextImpl和Activity等.
```
/**
/// @frameworks/base/core/java/android/content/Context.java
   * Same as {@link #startActivity(Intent, Bundle)} with no options
   * specified.
   *
   * @param intent The description of the activity to start.
   *
   * @throws ActivityNotFoundException &nbsp;
   *`
   * @see #startActivity(Intent, Bundle)
   * @see PackageManager#resolveActivity
   */
  public abstract void startActivity(@RequiresPermission Intent intent);
```

## ContextImpl#startActivity
对于非Activity上下文的情况启动Activity
```
/// @frameworks/base/core/java/android/app/ContextImpl.java
@Override
public void startActivity(Intent intent) {
    warnIfCallingFromSystemProcess();
    startActivity(intent, null);
}

// 调用重载方法
@Override
public void startActivity(Intent intent, Bundle options) {
    warnIfCallingFromSystemProcess();

    // Calling start activity from outside an activity without FLAG_ACTIVITY_NEW_TASK is
    // generally not allowed, except if the caller specifies the task id the activity should
    // be launched in. A bug was existed between N and O-MR1 which allowed this to work. We
    // maintain this for backwards compatibility.
    final int targetSdkVersion = getApplicationInfo().targetSdkVersion;

    if ((intent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK) == 0
            && (targetSdkVersion < Build.VERSION_CODES.N
                    || targetSdkVersion >= Build.VERSION_CODES.P)
            && (options == null
                    || ActivityOptions.fromBundle(options).getLaunchTaskId() == -1)) { // 在Activity之外的上下文中启动Activity,需要指定Intent.FLAG_ACTIVITY_NEW_TASK
        throw new AndroidRuntimeException(
                "Calling startActivity() from outside of an Activity "
                        + " context requires the FLAG_ACTIVITY_NEW_TASK flag."
                        + " Is this really what you want?");
    }
    // mMainThread 即ActivityThread
    mMainThread.getInstrumentation().execStartActivity(
            getOuterContext(), mMainThread.getApplicationThread(), null,
            (Activity) null, intent, -1, options);
}

```

## Activity#startActivity
在Activity的上下文中启动Activity
```
/// @frameworks/base/core/java/android/app/Activity.java
@Override
public void startActivity(Intent intent) {
    this.startActivity(intent, null); // 调用重载方法,多一个Bundle参数
}

/**
 * Launch a new activity.  You will not receive any information about when
 * the activity exits.  This implementation overrides the base version,
 * providing information about
 * the activity performing the launch.  Because of this additional
 * information, the {@link Intent#FLAG_ACTIVITY_NEW_TASK} launch flag is not
 * required; if not specified, the new activity will be added to the
 * task of the caller.
 *
 * <p>This method throws {@link android.content.ActivityNotFoundException}
 * if there was no Activity found to run the given Intent.
 *
 * @param intent The intent to start.
 * @param options Additional options for how the Activity should be started.
 * See {@link android.content.Context#startActivity(Intent, Bundle)}
 * Context.startActivity(Intent, Bundle)} for more details.
 *
 * @throws android.content.ActivityNotFoundException
 *
 * @see #startActivity(Intent)
 * @see #startActivityForResult
 */
@Override
public void startActivity(Intent intent, @Nullable Bundle options) { // 复写了基类的方法,不需要指定FLAG_ACTIVITY_NEW_TASK
    if (mIntent != null && mIntent.hasExtra(AutofillManager.EXTRA_RESTORE_SESSION_TOKEN)
            && mIntent.hasExtra(AutofillManager.EXTRA_RESTORE_CROSS_ACTIVITY)) {
        if (TextUtils.equals(getPackageName(),
                intent.resolveActivity(getPackageManager()).getPackageName())) {
            // Apply Autofill restore mechanism on the started activity by startActivity()
            final IBinder token =
                    mIntent.getIBinderExtra(AutofillManager.EXTRA_RESTORE_SESSION_TOKEN);
            // Remove restore ability from current activity
            mIntent.removeExtra(AutofillManager.EXTRA_RESTORE_SESSION_TOKEN);
            mIntent.removeExtra(AutofillManager.EXTRA_RESTORE_CROSS_ACTIVITY);
            // Put restore token
            intent.putExtra(AutofillManager.EXTRA_RESTORE_SESSION_TOKEN, token);
            intent.putExtra(AutofillManager.EXTRA_RESTORE_CROSS_ACTIVITY, true);
        }
    }
    if (options != null) { // 有options,一些启动Activity的配选项
        startActivityForResult(intent, -1, options); // 注意,requestCode=-1
    } else {
        // Note we want to go through this call for compatibility with
        // applications that may have overridden the method.
        startActivityForResult(intent, -1);
    }
}
```
可以发现, startActivity 实际上是调用了 startActivityForResult方法, 只不过给requestCode传递了-1

### Activity#startActivityForResult
此方法用于启动新Activity,并根据requestCode判断是否需要返回结果:
- requestCode < 0 , 不需要返回结果
- requestCode >= 0 , 需要返回结果,并且onActivityResult()会被调用
```
public void startActivityForResult(@RequiresPermission Intent intent, int requestCode) {
    startActivityForResult(intent, requestCode, null);
}

/**
 * Launch an activity for which you would like a result when it finished.
 * When this activity exits, your
 * onActivityResult() method will be called with the given requestCode.
 * Using a negative requestCode is the same as calling
 * {@link #startActivity} (the activity is not launched as a sub-activity).
 *
 * <p>Note that this method should only be used with Intent protocols
 * that are defined to return a result.  In other protocols (such as
 * {@link Intent#ACTION_MAIN} or {@link Intent#ACTION_VIEW}), you may
 * not get the result when you expect.  For example, if the activity you
 * are launching uses {@link Intent#FLAG_ACTIVITY_NEW_TASK}, it will not
 * run in your task and thus you will immediately receive a cancel result.
 *
 * <p>As a special case, if you call startActivityForResult() with a requestCode
 * >= 0 during the initial onCreate(Bundle savedInstanceState)/onResume() of your
 * activity, then your window will not be displayed until a result is
 * returned back from the started activity.  This is to avoid visible
 * flickering when redirecting to another activity.
 *
 * <p>This method throws {@link android.content.ActivityNotFoundException}
 * if there was no Activity found to run the given Intent.
 *
 * @param intent The intent to start.
 * @param requestCode If >= 0, this code will be returned in
 *                    onActivityResult() when the activity exits.
 * @param options Additional options for how the Activity should be started.
 * See {@link android.content.Context#startActivity(Intent, Bundle)}
 * Context.startActivity(Intent, Bundle)} for more details.
 *
 * @throws android.content.ActivityNotFoundException
 *
 * @see #startActivity
 */
public void startActivityForResult(@RequiresPermission Intent intent, int requestCode,
        @Nullable Bundle options) {
    if (mParent == null) {  // 历史遗留问题，目前基本不存在Activity嵌套， mParent为null
        options = transferSpringboardActivityOptions(options);
        Instrumentation.ActivityResult ar =
            mInstrumentation.execStartActivity(
                this, mMainThread.getApplicationThread(), mToken, this,
                intent, requestCode, options);  // 执行Instrumentation.execStartActivity 进行启动Activity
        if (ar != null) { // 处理result
            mMainThread.sendActivityResult(
                mToken, mEmbeddedID, requestCode, ar.getResultCode(),
                ar.getResultData());
        }
        if (requestCode >= 0) {
            // If this start is requesting a result, we can avoid making
            // the activity visible until the result is received.  Setting
            // this code during onCreate(Bundle savedInstanceState) or onResume() will keep the
            // activity hidden during this time, to avoid flickering.
            // This can only be done when a result is requested because
            // that guarantees we will get information back when the
            // activity is finished, no matter what happens to it.
            mStartedActivity = true;
        }

        cancelInputsAndStartExitTransition(options);
        // TODO Consider clearing/flushing other event sources and events for child windows.
    } else {
        if (options != null) {
            mParent.startActivityFromChild(this, intent, requestCode, options);
        } else {
            // Note we want to go through this method for compatibility with
            // existing applications that may have overridden it.
            mParent.startActivityFromChild(this, intent, requestCode);
        }
    }
}
```

可见,上面2种启动Activity的方式,最终都调用了Instrumentation.execStartActivity来启动Activity

# Instrumentation#execStartActivity
```
/// @frameworks/base/core/java/android/app/Instrumentation.java
/* @param who The Context from which the activity is being started.
* @param contextThread The main thread of the Context from which the activity
*                      is being started. IApplicationThread 对象, 表示调用方是哪个应用
* @param token Internal token identifying to the system who is starting
*              the activity; may be null.  ActivityRecord$Token , 用于在系统内部查找对应的ActivityRecord
* @param target Which activity is performing the start (and thus receiving
*               any result); may be null if this call is not being made
*               from an activity.
* @param intent The actual Intent to start.
* @param requestCode Identifier for this request's result; less than zero
*                    if the caller is not expecting a result.
* @param options Addition options.
*
* @return To force the return of a particular result, return an
*         ActivityResult object containing the desired data; otherwise
*         return null.  The default implementation always returns null.
*
* @throws android.content.ActivityNotFoundException
* {@hide}
*/
@UnsupportedAppUsage
public ActivityResult execStartActivity(
       Context who, IBinder contextThread, IBinder token, Activity target,
       Intent intent, int requestCode, Bundle options) {
   android.util.SeempLog.record_str(377, intent.toString());
   IApplicationThread whoThread = (IApplicationThread) contextThread;
   Uri referrer = target != null ? target.onProvideReferrer() : null;
   if (referrer != null) {
       intent.putExtra(Intent.EXTRA_REFERRER, referrer);
   }
   if (mActivityMonitors != null) { // ActivityMonitor实现,用于监控与修改应用启动Activity
       synchronized (mSync) {
           final int N = mActivityMonitors.size();
           for (int i=0; i<N; i++) {
               final ActivityMonitor am = mActivityMonitors.get(i);
               ActivityResult result = null;
               if (am.ignoreMatchingSpecificIntents()) {
                   result = am.onStartActivity(intent);
               }
               if (result != null) {
                   am.mHits++;
                   return result;
               } else if (am.match(who, null, intent)) {
                   am.mHits++;
                   if (am.isBlocking()) {
                       return requestCode >= 0 ? am.getResult() : null;
                   }
                   break;
               }
           }
       }
   }
   try {
       intent.migrateExtraStreamToClipData(who);
       intent.prepareToLeaveProcess(who);
       int result = ActivityTaskManager.getService().startActivity(whoThread,
               who.getBasePackageName(), who.getAttributionTag(), intent,
               intent.resolveTypeIfNeeded(who.getContentResolver()), token,
               target != null ? target.mEmbeddedID : null, requestCode, 0, null, options); // 调用 ActivityTaskManagerService来启动Activity
       checkStartActivityResult(result, intent);
   } catch (RemoteException e) {
       throw new RuntimeException("Failure from system", e);
   }
   return null;
}
```

# ActivityTaskManager.getService().startActivity
ActivityTaskManager.getService()获取的实际上是ActivityTaskManagerService的Binder代理对象,通过此对象调用startActivity,将通过Binder发起一个IPC调用.最终调用 ActivityTaskManagerService#startActivity来启动Activity

## ActivityTaskManager.getService()
```
/** @hide */
public static IActivityTaskManager getService() {
    return IActivityTaskManagerSingleton.get();  // 单例,调用get()将触发创建,其create方法将被调用
}

@UnsupportedAppUsage(trackingBug = 129726065)
private static final Singleton<IActivityTaskManager> IActivityTaskManagerSingleton =
        new Singleton<IActivityTaskManager>() {
            @Override
            protected IActivityTaskManager create() {  // 调用create方法时创建代理对象
                final IBinder b = ServiceManager.getService(Context.ACTIVITY_TASK_SERVICE); // 获取的ActivityTaskManagerService的Binder代理对象
                return IActivityTaskManager.Stub.asInterface(b); // 转换为对应的代理对象,
            }
        };

```

## IActivityTaskManager.Stub.asInterface
```
/// gen/android/app/IActivityTaskManager.java
/**
 * Cast an IBinder object into an android.app.IActivityTaskManager interface,
 * generating a proxy if needed.
 */
public static android.app.IActivityTaskManager asInterface(android.os.IBinder obj)
{
  if ((obj==null)) {
    return null;
  }
  android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
  if (((iin!=null)&&(iin instanceof android.app.IActivityTaskManager))) {// 同进程
    return ((android.app.IActivityTaskManager)iin);
  }
  return new android.app.IActivityTaskManager.Stub.Proxy(obj);  // 非同一进程, 将创建 Proxy对象
}
```
在此处,对于app而言,通常与SystemServer进程不在同一进程, 因此,ActivityTaskManager.getService()将创建 IActivityTaskManager.Stub.Proxy 对象,调用ActivityTaskManager.getService().startActivity 实际上调用了 IActivityTaskManager.Stub.Proxy 的 startActivity方法

##  IActivityTaskManager.Stub.Proxy#startActivity
```
@Override public int startActivity(android.app.IApplicationThread caller, java.lang.String callingPackage, java.lang.String callingFeatureId, android.content.Intent intent, java.lang.String resolvedType, android.os.IBinder resultTo, java.lang.String resultWho, int requestCode, int flags, android.app.ProfilerInfo profilerInfo, android.os.Bundle options) throws android.os.RemoteException
{
  android.os.Parcel _data = android.os.Parcel.obtain();
  android.os.Parcel _reply = android.os.Parcel.obtain();
  int _result;
  try {
    _data.writeInterfaceToken(DESCRIPTOR);
    _data.writeStrongBinder((((caller!=null))?(caller.asBinder()):(null)));
    _data.writeString(callingPackage);
    _data.writeString(callingFeatureId);
    if ((intent!=null)) {
      _data.writeInt(1);
      intent.writeToParcel(_data, 0);
    }
    else {
      _data.writeInt(0);
    }
    _data.writeString(resolvedType);
    _data.writeStrongBinder(resultTo);
    _data.writeString(resultWho);
    _data.writeInt(requestCode);
    _data.writeInt(flags);
    if ((profilerInfo!=null)) {
      _data.writeInt(1);
      profilerInfo.writeToParcel(_data, 0);
    }
    else {
      _data.writeInt(0);
    }
    if ((options!=null)) {
      _data.writeInt(1);
      options.writeToParcel(_data, 0);
    }
    else {
      _data.writeInt(0);
    }
    boolean _status = mRemote.transact(Stub.TRANSACTION_startActivity, _data, _reply, 0); // 向ATMS发起Binder调用,启动码是TRANSACTION_startActivity
    if (!_status && getDefaultImpl() != null) {
      return getDefaultImpl().startActivity(caller, callingPackage, callingFeatureId, intent, resolvedType, resultTo, resultWho, requestCode, flags, profilerInfo, options);
    }
    _reply.readException();  // 读取权限
    _result = _reply.readInt();
  }
  finally {
    _reply.recycle();
    _data.recycle();
  }
  return _result;
}
```

接下来,服务端将会处理此请求,然后IActivityTaskManager.Stub的onTransact方法将会被调用,然后根据startActivity的code执行相关启动Activity操作

# IActivityTaskManager.Stub#onTransact

```
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
  java.lang.String descriptor = DESCRIPTOR;
  switch (code)
  {
    ...
    case TRANSACTION_startActivity:  // 执行启动Activity
    {
      data.enforceInterface(descriptor);
      android.app.IApplicationThread _arg0;
      _arg0 = android.app.IApplicationThread.Stub.asInterface(data.readStrongBinder());
      java.lang.String _arg1;
      _arg1 = data.readString();
      java.lang.String _arg2;
      _arg2 = data.readString();
      android.content.Intent _arg3;
      if ((0!=data.readInt())) {
        _arg3 = android.content.Intent.CREATOR.createFromParcel(data);
      }
      else {
        _arg3 = null;
      }
      java.lang.String _arg4;
      _arg4 = data.readString();
      android.os.IBinder _arg5;
      _arg5 = data.readStrongBinder();
      java.lang.String _arg6;
      _arg6 = data.readString();
      int _arg7;
      _arg7 = data.readInt();
      int _arg8;
      _arg8 = data.readInt();
      android.app.ProfilerInfo _arg9;
      if ((0!=data.readInt())) {
        _arg9 = android.app.ProfilerInfo.CREATOR.createFromParcel(data);
      }
      else {
        _arg9 = null;
      }
      android.os.Bundle _arg10;
      if ((0!=data.readInt())) {
        _arg10 = android.os.Bundle.CREATOR.createFromParcel(data);
      }
      else {
        _arg10 = null;
      }
      int _result = this.startActivity(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7, _arg8, _arg9, _arg10); // 回调服务的startActivity
      reply.writeNoException();
      reply.writeInt(_result);
      return true;
    }
    ...
}
```

ActivityTaskManagerService 继承自 IActivityTaskManager.Stub , 因此this.startActivity即调用了它的startActivity方法

# ActivityTaskManagerService#startActivity
```
@Override
  public final int startActivity(IApplicationThread caller, String callingPackage,
          String callingFeatureId, Intent intent, String resolvedType, IBinder resultTo,
          String resultWho, int requestCode, int startFlags, ProfilerInfo profilerInfo,
          Bundle bOptions) {
      return startActivityAsUser(caller, callingPackage, callingFeatureId, intent, resolvedType,
              resultTo, resultWho, requestCode, startFlags, profilerInfo, bOptions,
              UserHandle.getCallingUserId());
  }
```

# 总结
从client到ActivityTaskManagerService启动Activity的流程如下:

> ContextImpl/Activity#startActivity -> Instrumentation#execStartActivity -> IActivityTaskManager.Stub.Proxy#startActivity ->
 IActivityTaskManager.Stub#onTransact -> ActivityTaskManagerService#startActivity
