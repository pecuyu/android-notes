# 从launcher启动dialer
log是Android10. 在Android11上有些log从am_变成wm_，比如：
- am_create_task     ->   wm_create_task
- am_create_activity ->   wm_create_activity
- am_focused_stack   ->   wm_focused_stack

生命周期的 am_ 变成了 wm_

```
// 创建stack，并pause SearchLauncher
11-03 09:09:44.672  1062  2552 I wm_stack_created: 2
11-03 09:09:44.677  1062  2552 I wm_task_created: [286,2]
11-03 09:09:44.679  1062  2552 I wm_task_moved: [286,0,2147483647]
11-03 09:09:44.679  1062  2552 I am_focused_stack: [0,0,2,0,reuseOrNewTask]
11-03 09:09:44.680  1062  2552 I am_create_task: [0,286]
11-03 09:09:44.680  1062  2552 I am_create_activity: [0,208663792,286,com.android.dialer/.main.impl.MainActivity,android.intent.action.MAIN,NULL,NULL,270532608]
11-03 09:09:44.680  1062  2552 I wm_task_moved: [286,0,2147483647]
// pause SearchLauncher activity
11-03 09:09:44.683  1062  2552 I am_pause_activity: [0,3026089,com.android.launcher3/com.android.searchlauncher.SearchLauncher,userLeaving=true]
11-03 09:09:44.698  1934  1934 I am_on_top_resumed_lost_called: [0,com.android.searchlauncher.SearchLauncher,topStateChangedWhenResumed]
11-03 09:09:44.700  1934  1934 I am_on_paused_called: [0,com.android.searchlauncher.SearchLauncher,performPause]
11-03 09:09:44.706  1062  5337 I am_add_to_stopping: [0,3026089,com.android.launcher3/com.android.searchlauncher.SearchLauncher,makeInvisible]
// 启动 dialer 进程
11-03 09:09:44.775  1062  1282 I am_proc_start: [0,5538,10103,com.android.dialer,activity,{com.android.dialer/com.android.dialer.main.impl.MainActivity}]
11-03 09:09:44.880  1062  2583 I am_proc_bound: [0,5538,com.android.dialer]
// 启动 dialer MainActivity
11-03 09:09:44.894  1062  2583 I am_restart_activity: [0,208663792,286,com.android.dialer/.main.impl.MainActivity]
11-03 09:09:44.898  1062  2583 I am_set_resumed_activity: [0,com.android.dialer/.main.impl.MainActivity,minimalResumeActivityLocked]
11-03 09:09:45.452  5538  5538 I am_on_create_called: [0,com.android.dialer.main.impl.MainActivity,performCreate]
11-03 09:09:45.564  5538  5538 I am_on_start_called: [0,com.android.dialer.main.impl.MainActivity,handleStartActivity]
11-03 09:09:45.588  5538  5538 I am_on_resume_called: [0,com.android.dialer.main.impl.MainActivity,RESUME_ACTIVITY]

11-03 09:09:45.646  5538  5538 I am_on_top_resumed_gained_called: [0,com.android.dialer.main.impl.MainActivity,topStateChangedWhenResumed]

```
