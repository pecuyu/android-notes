接上一篇，接下来执行Resume Activity操作，而此时的activity状态有很多可能：
- activity已经处于resumed
- activity已经创建但是非resumed状态
- activity还没创建
- activity所在进程还未创建
- 等等

---
接下来看 ActivityStack#resumeTopActivityUncheckedLocked 方法

# ActivityStack#resumeTopActivityUncheckedLocked

```
// @frameworks/base/services/core/java/com/android/server/wm/ActivityStack.java
/**
 * Ensure that the top activity in the stack is resumed.
 *
 * @param prev The previously resumed activity, for when in the process
 * of pausing; can be null to call from elsewhere.
 * @param options Activity options.
 *
 * @return Returns true if something is being resumed, or false if
 * nothing happened.
 *
 * NOTE: It is not safe to call this method directly as it can cause an activity in a
 *       non-focused stack to be resumed.
 *       Use {@link RootWindowContainer#resumeFocusedStacksTopActivities} to resume the
 *       right activity for the current system state.
 */
@GuardedBy("mService")
boolean resumeTopActivityUncheckedLocked(ActivityRecord prev, ActivityOptions options) {
    if (mInResumeTopActivity) { // 正在执行resume操作则返回
        // Don't even start recursing.
        return false;
    }

    boolean result = false;
    try {
        // Protect against recursion.
        mInResumeTopActivity = true;
        result = resumeTopActivityInnerLocked(prev, options); // 执行resume

        // When resuming the top activity, it may be necessary to pause the top activity (for
        // example, returning to the lock screen. We suppress the normal pause logic in
        // {@link #resumeTopActivityUncheckedLocked}, since the top activity is resumed at the
        // end. We call the {@link ActivityStackSupervisor#checkReadyForSleepLocked} again here
        // to ensure any necessary pause logic occurs. In the case where the Activity will be
        // shown regardless of the lock screen, the call to
        // {@link ActivityStackSupervisor#checkReadyForSleepLocked} is skipped.
        final ActivityRecord next = topRunningActivity(true /* focusableOnly */);
        if (next == null || !next.canTurnScreenOn()) {
            checkReadyForSleep();
        }
    } finally {
        mInResumeTopActivity = false;
    }

    return result;
}

```

# ActivityStack#resumeTopActivityUncheckedLocked

```
@GuardedBy("mService")
private boolean resumeTopActivityInnerLocked(ActivityRecord prev, ActivityOptions options) {
    if (!mAtmService.isBooting() && !mAtmService.isBooted()) {
        // Not ready yet!
        return false;
    }

    // Find the next top-most activity to resume in this stack that is not finishing and is
    // focusable. If it is not focusable, we will fall into the case below to resume the
    // top activity in the next focusable task.
    ActivityRecord next = topRunningActivity(true /* focusableOnly */); // 最顶上的activity

    final boolean hasRunningActivity = next != null;

    // TODO: Maybe this entire condition can get removed?
    if (hasRunningActivity && !isAttached()) {
        return false;
    }

    mRootWindowContainer.cancelInitializingActivities();

    // Remember how we'll process this pause/resume situation, and ensure
    // that the state is reset however we wind up proceeding.
    boolean userLeaving = mStackSupervisor.mUserLeaving;
    mStackSupervisor.mUserLeaving = false;

    if (!hasRunningActivity) { // 此stack没有待启动activity，寻找其他可能启动
        // There are no activities left in the stack, let's look somewhere else.
        return resumeNextFocusableActivityWhenStackIsEmpty(prev, options);
    }

    next.delayedResume = false;
    final TaskDisplayArea taskDisplayArea = getDisplayArea();

    // If the top activity is the resumed one, nothing to do.
    if (mResumedActivity == next && next.isState(RESUMED)
            && taskDisplayArea.allResumedActivitiesComplete()) {  // 待启动activity已经是resumed
        // Make sure we have executed any pending transitions, since there
        // should be nothing left to do at this point.
        executeAppTransition(options);
        if (DEBUG_STATES) Slog.d(TAG_STATES,
                "resumeTopActivityLocked: Top activity resumed " + next);
        return false;
    }

    if (!next.canResumeByCompat()) {
        return false;
    }

    // If we are currently pausing an activity, then don't do anything until that is done.
    final boolean allPausedComplete = mRootWindowContainer.allPausedActivitiesComplete();
    if (!allPausedComplete) { // 判断所有待pause的activity是否已经完成，为完成需要等待
        if (DEBUG_SWITCH || DEBUG_PAUSE || DEBUG_STATES) {
            Slog.v(TAG_PAUSE, "resumeTopActivityLocked: Skip resume: some activity pausing.");
        }
        return false;
    }

    // If we are sleeping, and there is no resumed activity, and the top activity is paused,
    // well that is the state we want.
    if (shouldSleepOrShutDownActivities()   // 将休眠或关机
            && mLastPausedActivity == next  // 上一个paused activity是top activity
            && mRootWindowContainer.allPausedActivitiesComplete() /* 已完成所有待pause activity */ ) {
        // If the current top activity may be able to occlude keyguard but the occluded state
        // has not been set, update visibility and check again if we should continue to resume.
        boolean nothingToResume = true;
        if (!mAtmService.mShuttingDown) { // 非关机中
            final boolean canShowWhenLocked = !mTopActivityOccludesKeyguard
                    && next.canShowWhenLocked();  // 是否可以在锁屏时显示
            final boolean mayDismissKeyguard = mTopDismissingKeyguardActivity != next
                    && next.containsDismissKeyguardWindow(); //是否需要dismiss keyguard

            if (canShowWhenLocked || mayDismissKeyguard) {
                ensureActivitiesVisible(null /* starting */, 0 /* configChanges */,
                        !PRESERVE_WINDOWS);
                nothingToResume = shouldSleepActivities();
            } else if (next.currentLaunchCanTurnScreenOn() && next.canTurnScreenOn()) { // 是否此启动可以亮屏
                nothingToResume = false;
            }
        }
        if (nothingToResume) {
            // Make sure we have executed any pending transitions, since there
            // should be nothing left to do at this point.
            executeAppTransition(options);
            if (DEBUG_STATES) Slog.d(TAG_STATES,
                    "resumeTopActivityLocked: Going to sleep and all paused");
            return false;
        }
    }

    // Make sure that the user who owns this activity is started.  If not,
    // we will just leave it as is because someone should be bringing
    // another user's activities to the top of the stack.
    if (!mAtmService.mAmInternal.hasStartedUserState(next.mUserId)) {
        Slog.w(TAG, "Skipping resume of top activity " + next
                + ": user " + next.mUserId + " is stopped");
        return false;
    }

    // The activity may be waiting for stop, but that is no longer
    // appropriate for it.
    mStackSupervisor.mStoppingActivities.remove(next); // 从stopping列表移除
    next.setSleeping(false);
    next.launching = true;

    if (DEBUG_SWITCH) Slog.v(TAG_SWITCH, "Resuming " + next);

    // 通知activity状态变化
    next.callServiceTrackeronActivityStatechange(RESUMED, true);
    if (mActivityTrigger != null) {
        mActivityTrigger.activityResumeTrigger(next.intent, next.info, next.info.applicationInfo,
                next.occludesParent());
    }

    if (mActivityPluginDelegate != null && getWindowingMode() != WINDOWING_MODE_UNDEFINED) {
        mActivityPluginDelegate.activityInvokeNotification
            (next.info.applicationInfo.packageName, getWindowingMode() == WINDOWING_MODE_FULLSCREEN);
    }

    // If we are currently pausing an activity, then don't do anything until that is done.
    if (!mRootWindowContainer.allPausedActivitiesComplete()) { // 在pause activity中
        if (DEBUG_SWITCH || DEBUG_PAUSE || DEBUG_STATES) Slog.v(TAG_PAUSE,
                "resumeTopActivityLocked: Skip resume: some activity pausing.");

        return false;
    }

    mStackSupervisor.setLaunchSource(next.info.applicationInfo.uid);

    ActivityRecord lastResumed = null;
    final ActivityStack lastFocusedStack = taskDisplayArea.getLastFocusedStack();
    if (lastFocusedStack != null && lastFocusedStack != this) {
        // So, why aren't we using prev here??? See the param comment on the method. prev doesn't
        // represent the last resumed activity. However, the last focus stack does if it isn't null.
        lastResumed = lastFocusedStack.mResumedActivity;
        // 多窗口下设置userLeaving为false
        if (userLeaving && inMultiWindowMode() && lastFocusedStack.shouldBeVisible(next)) {
            // The user isn't leaving if this stack is the multi-window mode and the last
            // focused stack should still be visible.
            if(DEBUG_USER_LEAVING) Slog.i(TAG_USER_LEAVING, "Overriding userLeaving to false"
                    + " next=" + next + " lastResumed=" + lastResumed);
            userLeaving = false;
        }
    }

    // resume新activity之前，停止所有stack中的activity
    boolean pausing = taskDisplayArea.pauseBackStacks(userLeaving, next);
    if (mResumedActivity != null) { // 执行pause 当前resumed的activity操作
        if (DEBUG_STATES) Slog.d(TAG_STATES,
                "resumeTopActivityLocked: Pausing " + mResumedActivity);
        pausing |= startPausingLocked(userLeaving, false /* uiSleeping */, next);
    }
    if (pausing) { // 还在pausing
        if (DEBUG_SWITCH || DEBUG_STATES) Slog.v(TAG_STATES,
                "resumeTopActivityLocked: Skip resume: need to start pausing");
        // At this point we want to put the upcoming activity's process
        // at the top of the LRU list, since we know we will be needing it
        // very soon and it would be a waste to let it get killed if it
        // happens to be sitting towards the end.
        if (next.attachedToProcess()) {// 如果process存在，调整process到LRU list顶
            next.app.updateProcessInfo(false /* updateServiceConnectionActivities */,
                    true /* activityChange */, false /* updateOomAdj */,
                    false /* addPendingTopUid */);
        } else if (!next.isProcessRunning()) { // process不存在，则需要启动进程
            // Since the start-process is asynchronous, if we already know the process of next
            // activity isn't running, we can start the process earlier to save the time to wait
            // for the current activity to be paused.
            final boolean isTop = this == taskDisplayArea.getFocusedStack();
            mAtmService.startProcessAsync(next, false /* knownToBeDead */, isTop,
                    isTop ? "pre-top-activity" : "pre-activity"); // 执行异步启动进程操作，节省启动时间
        }
        if (lastResumed != null) {
            lastResumed.setWillCloseOrEnterPip(true);
        }
        return true;
    } else if (mResumedActivity == next && next.isState(RESUMED)
            && taskDisplayArea.allResumedActivitiesComplete()) { // 已处于RESUMED
        // It is possible for the activity to be resumed when we paused back stacks above if the
        // next activity doesn't have to wait for pause to complete.
        // So, nothing else to-do except:
        // Make sure we have executed any pending transitions, since there
        // should be nothing left to do at this point.
        executeAppTransition(options);
        if (DEBUG_STATES) Slog.d(TAG_STATES,
                "resumeTopActivityLocked: Top activity resumed (dontWaitForPause) " + next);
        return true;
    }

    // If the most recent activity was noHistory but was only stopped rather
    // than stopped+finished because the device went to sleep, we need to make
    // sure to finish it as we're making a new activity topmost.
    if (shouldSleepActivities() && mLastNoHistoryActivity != null &&
            !mLastNoHistoryActivity.finishing) { // finish mLastNoHistoryActivity
        if (DEBUG_STATES) Slog.d(TAG_STATES,
                "no-history finish of " + mLastNoHistoryActivity + " on new resume");
        mLastNoHistoryActivity.finishIfPossible("resume-no-history", false /* oomAdj */);
        mLastNoHistoryActivity = null;
    }

    if (prev != null && prev != next && next.nowVisible) { // prev在finishing 则hide

        // The next activity is already visible, so hide the previous
        // activity's windows right now so we can show the new one ASAP.
        // We only do this if the previous is finishing, which should mean
        // it is on top of the one being resumed so hiding it quickly
        // is good.  Otherwise, we want to do the normal route of allowing
        // the resumed activity to be shown so we can decide if the
        // previous should actually be hidden depending on whether the
        // new one is found to be full-screen or not.
        if (prev.finishing) {
            prev.setVisibility(false);
            if (DEBUG_SWITCH) Slog.v(TAG_SWITCH,
                    "Not waiting for visible to hide: " + prev
                    + ", nowVisible=" + next.nowVisible);
        } else {
            if (DEBUG_SWITCH) Slog.v(TAG_SWITCH,
                    "Previous already visible but still waiting to hide: " + prev
                    + ", nowVisible=" + next.nowVisible);
        }

    }

    // Launching this app's activity, make sure the app is no longer
    // considered stopped.
    try { // 修改app stopped 状态为false
        mAtmService.getPackageManager().setPackageStoppedState(
                next.packageName, false, next.mUserId); /* TODO: Verify if correct userid */
    } catch (RemoteException e1) {
    } catch (IllegalArgumentException e) {
        Slog.w(TAG, "Failed trying to unstop package "
                + next.packageName + ": " + e);
    }

    // We are starting up the next activity, so tell the window manager
    // that the previous one will be hidden soon.  This way it can know
    // to ignore it when computing the desired screen orientation.
    boolean anim = true;
    final DisplayContent dc = taskDisplayArea.mDisplayContent;
    if (mPerf == null) {
        mPerf = new BoostFramework();
    }
    if (prev != null) {
        if (prev.finishing) { // prev finishing，执行关闭动画
            if (DEBUG_TRANSITION) Slog.v(TAG_TRANSITION,
                    "Prepare close transition: prev=" + prev);
            if (mStackSupervisor.mNoAnimActivities.contains(prev)) {
                anim = false;
                dc.prepareAppTransition(TRANSIT_NONE, false);
            } else {
                mWmService.prepareAppTransition(prev.getTask() == next.getTask()
                        ? TRANSIT_ACTIVITY_CLOSE
                        : TRANSIT_TASK_CLOSE, false);
                if(prev.getTask() != next.getTask() && mPerf != null) {
                   mPerf.perfHint(BoostFramework.VENDOR_HINT_ANIM_BOOST, next.packageName);
                }
                dc.prepareAppTransition(
                        prev.getTask() == next.getTask() ? TRANSIT_ACTIVITY_CLOSE
                                : TRANSIT_TASK_CLOSE, false);
            }
            prev.setVisibility(false);
        } else { // 执行open动画
            if (DEBUG_TRANSITION) Slog.v(TAG_TRANSITION,
                    "Prepare open transition: prev=" + prev);
            if (mStackSupervisor.mNoAnimActivities.contains(next)) {
                anim = false;
                dc.prepareAppTransition(TRANSIT_NONE, false);
            } else {
                mWmService.prepareAppTransition(prev.getTask() == next.getTask()
                        ? TRANSIT_ACTIVITY_OPEN
                        : next.mLaunchTaskBehind
                                ? TRANSIT_TASK_OPEN_BEHIND
                                : TRANSIT_TASK_OPEN, false);
                if(prev.getTask() != next.getTask() && mPerf != null) {
                   mPerf.perfHint(BoostFramework.VENDOR_HINT_ANIM_BOOST, next.packageName);
                }
                dc.prepareAppTransition(
                        prev.getTask() == next.getTask() ? TRANSIT_ACTIVITY_OPEN
                                : next.mLaunchTaskBehind ? TRANSIT_TASK_OPEN_BEHIND
                                        : TRANSIT_TASK_OPEN, false);
            }
        }
    } else {
        if (DEBUG_TRANSITION) Slog.v(TAG_TRANSITION, "Prepare open transition: no previous");
        if (mStackSupervisor.mNoAnimActivities.contains(next)) {
            anim = false;
            dc.prepareAppTransition(TRANSIT_NONE, false);
        } else {
            dc.prepareAppTransition(TRANSIT_ACTIVITY_OPEN, false);
        }
    }

    if (anim) {
        next.applyOptionsLocked();
    } else {
        next.clearOptionsLocked();
    }

    mStackSupervisor.mNoAnimActivities.clear();

    if (next.attachedToProcess()) { // 此activity已经启动过
        if (DEBUG_SWITCH) Slog.v(TAG_SWITCH, "Resume running: " + next
                + " stopped=" + next.stopped
                + " visibleRequested=" + next.mVisibleRequested);

        // If the previous activity is translucent, force a visibility update of
        // the next activity, so that it's added to WM's opening app list, and
        // transition animation can be set up properly.
        // For example, pressing Home button with a translucent activity in focus.
        // Launcher is already visible in this case. If we don't add it to opening
        // apps, maybeUpdateTransitToWallpaper() will fail to identify this as a
        // TRANSIT_WALLPAPER_OPEN animation, and run some funny animation.
        final boolean lastActivityTranslucent = lastFocusedStack != null
                && (lastFocusedStack.inMultiWindowMode()
                || (lastFocusedStack.mLastPausedActivity != null
                && !lastFocusedStack.mLastPausedActivity.occludesParent())); // 上一个是透明activity

        // This activity is now becoming visible.
        if (!next.mVisibleRequested || next.stopped || lastActivityTranslucent) {
            next.setVisibility(true); // 设置visibility为true
        }

        // schedule launch ticks to collect information about slow apps.
        next.startLaunchTickingLocked();

        ActivityRecord lastResumedActivity =
                lastFocusedStack == null ? null : lastFocusedStack.mResumedActivity;
        final ActivityState lastState = next.getState();

        mAtmService.updateCpuStats();

        if (DEBUG_STATES) Slog.v(TAG_STATES, "Moving to RESUMED: " + next
                + " (in existing)");
        next.setState(RESUMED, "resumeTopActivityInnerLocked"); // 设置RESUMED状态

        next.app.updateProcessInfo(false /* updateServiceConnectionActivities */,
                true /* activityChange */, true /* updateOomAdj */,
                true /* addPendingTopUid */); // 更新process信息

        // Have the window manager re-evaluate the orientation of
        // the screen based on the new activity order.
        boolean notUpdated = true;

        // Activity should also be visible if set mLaunchTaskBehind to true (see
        // ActivityRecord#shouldBeVisibleIgnoringKeyguard()).
        if (shouldBeVisible(next)) {
            // We have special rotation behavior when here is some active activity that
            // requests specific orientation or Keyguard is locked. Make sure all activity
            // visibilities are set correctly as well as the transition is updated if needed
            // to get the correct rotation behavior. Otherwise the following call to update
            // the orientation may cause incorrect configurations delivered to client as a
            // result of invisible window resize.
            // TODO: Remove this once visibilities are set correctly immediately when
            // starting an activity. // 确保更新visibility和更新配置
            notUpdated = !mRootWindowContainer.ensureVisibilityAndConfig(next, getDisplayId(),
                    true /* markFrozenIfConfigChanged */, false /* deferResume */);
        }

        if (notUpdated) { // 配置变化无法保存instance，重新启动
            // The configuration update wasn't able to keep the existing
            // instance of the activity, and instead started a new one.
            // We should be all done, but let's just make sure our activity
            // is still at the top and schedule another run if something
            // weird happened.
            ActivityRecord nextNext = topRunningActivity();
            if (DEBUG_SWITCH || DEBUG_STATES) Slog.i(TAG_STATES,
                    "Activity config changed during resume: " + next
                            + ", new next: " + nextNext);
            if (nextNext != next) {
                // Do over!
                mStackSupervisor.scheduleResumeTopActivities();
            }
            if (!next.mVisibleRequested || next.stopped) {
                next.setVisibility(true);
            }
            next.completeResumeLocked();
            return true;
        }

        try { // 接下来执行启动activity的transaction
            final ClientTransaction transaction =
                    ClientTransaction.obtain(next.app.getThread(), next.appToken);
            // Deliver all pending results.
            ArrayList<ResultInfo> a = next.results;
            if (a != null) {
                final int N = a.size();
                if (!next.finishing && N > 0) {
                    if (DEBUG_RESULTS) Slog.v(TAG_RESULTS,
                            "Delivering results to " + next + ": " + a);
                    transaction.addCallback(ActivityResultItem.obtain(a)); // 添加result条目
                }
            }

            if (next.newIntents != null) { // 添加NewIntentItem
                transaction.addCallback(
                        NewIntentItem.obtain(next.newIntents, true /* resume */));
            }

            // Well the app will no longer be stopped.
            // Clear app token stopped state in window manager if needed.
            next.notifyAppResumed(next.stopped);
            // 打印event log： wm_resume_activity
            EventLogTags.writeWmResumeActivity(next.mUserId, System.identityHashCode(next),
                    next.getTask().mTaskId, next.shortComponentName);

            next.setSleeping(false);
            mAtmService.getAppWarningsLocked().onResumeActivity(next);
            next.app.setPendingUiCleanAndForceProcessStateUpTo(mAtmService.mTopProcessState);
            next.clearOptionsLocked();
            transaction.setLifecycleStateRequest(
                    ResumeActivityItem.obtain(next.app.getReportedProcState(),
                            dc.isNextTransitionForward())); // 设置ResumeActivityItem
            mAtmService.getLifecycleManager().scheduleTransaction(transaction); // 执行transaction

            if (DEBUG_STATES) Slog.d(TAG_STATES, "resumeTopActivityLocked: Resumed "
                    + next);
        } catch (Exception e) {
            // Whoops, need to restart this activity!
            if (DEBUG_STATES) Slog.v(TAG_STATES, "Resume failed; resetting state to "
                    + lastState + ": " + next);
            next.callServiceTrackeronActivityStatechange(lastState, true);
            next.setState(lastState, "resumeTopActivityInnerLocked");

            // lastResumedActivity being non-null implies there is a lastStack present.
            if (lastResumedActivity != null) {
                lastResumedActivity.callServiceTrackeronActivityStatechange(RESUMED, true);
                lastResumedActivity.setState(RESUMED, "resumeTopActivityInnerLocked");
            }

            Slog.i(TAG, "Restarting because process died: " + next);
            if (!next.hasBeenLaunched) {
                next.hasBeenLaunched = true;
            } else  if (SHOW_APP_STARTING_PREVIEW && lastFocusedStack != null
                    && lastFocusedStack.isTopStackInDisplayArea()) {
                next.showStartingWindow(null /* prev */, false /* newTask */,
                        false /* taskSwitch */);
            }
            mStackSupervisor.startSpecificActivity(next, true, false);
            return true;
        }

        // From this point on, if something goes wrong there is no way
        // to recover the activity.
        try {
            next.completeResumeLocked();
        } catch (Exception e) {
            // If any exception gets thrown, toss away this
            // activity and try the next one.
            Slog.w(TAG, "Exception thrown during resume of " + next, e);
            next.finishIfPossible("resume-exception", true /* oomAdj */);
            return true;
        }
    } else { // 需要启动activity
        // Whoops, need to restart this activity!
        if (!next.hasBeenLaunched) {
            next.hasBeenLaunched = true;
        } else {
            if (SHOW_APP_STARTING_PREVIEW) {
                next.showStartingWindow(null /* prev */, false /* newTask */,
                        false /* taskSwich */);
            }
            if (DEBUG_SWITCH) Slog.v(TAG_SWITCH, "Restarting: " + next);
        }
        if (DEBUG_STATES) Slog.d(TAG_STATES, "resumeTopActivityLocked: Restarting " + next);
        mStackSupervisor.startSpecificActivity(next, true, true);
    }

    return true;
}

```

## TaskDisplayArea#pauseBackStacks
暂停所有stack中resumed的activity

```
/// frameworks/base/services/core/java/com/android/server/wm/TaskDisplayArea.java
/**
 * Pause all activities in either all of the stacks or just the back stacks. This is done before
 * resuming a new activity and to make sure that previously active activities are
 * paused in stacks that are no longer visible or in pinned windowing mode. This does not
 * pause activities in visible stacks, so if an activity is launched within the same stack/task,
 * then we should explicitly pause that stack's top activity.
 * @param userLeaving Passed to pauseActivity() to indicate whether to call onUserLeaving().
 * @param resuming The resuming activity.
 * @return {@code true} if any activity was paused as a result of this call.
 */
boolean pauseBackStacks(boolean userLeaving, ActivityRecord resuming) {
    boolean someActivityPaused = false;
    for (int stackNdx = getStackCount() - 1; stackNdx >= 0; --stackNdx) {
        final ActivityStack stack = getStackAt(stackNdx);
        final ActivityRecord resumedActivity = stack.getResumedActivity();
        if (resumedActivity != null
                && (stack.getVisibility(resuming) != STACK_VISIBILITY_VISIBLE
                || !stack.isTopActivityFocusable())) { // 停止非visible或顶部activity非Focusable 栈中resumedActivity
            if (DEBUG_STATES) {
                Slog.d(TAG_STATES, "pauseBackStacks: stack=" + stack
                        + " mResumedActivity=" + resumedActivity);
            }
            someActivityPaused |= stack.startPausingLocked(userLeaving, false /* uiSleeping*/,
                    resuming);
        }
    }
    return someActivityPaused;
}

```

## ActivityStack#startPausingLocked


```
/// frameworks/base/services/core/java/com/android/server/wm/ActivityStack.java
/**
 * Start pausing the currently resumed activity.  It is an error to call this if there
 * is already an activity being paused or there is no resumed activity.
 *
 * @param userLeaving True if this should result in an onUserLeaving to the current activity.
 * @param uiSleeping True if this is happening with the user interface going to sleep (the
 * screen turning off).
 * @param resuming The activity we are currently trying to resume or null if this is not being
 *                 called as part of resuming the top activity, so we shouldn't try to instigate
 *                 a resume here if not null.
 * @return Returns true if an activity now is in the PAUSING state, and we are waiting for
 * it to tell us when it is done.
 */
final boolean startPausingLocked(boolean userLeaving, boolean uiSleeping,
        ActivityRecord resuming) {
    if (mPausingActivity != null) { // 已经在执行pause操作
        Slog.wtf(TAG, "Going to pause when pause is already pending for " + mPausingActivity
                + " state=" + mPausingActivity.getState());
        if (!shouldSleepActivities()) {
            // Avoid recursion among check for sleep and complete pause during sleeping.
            // Because activity will be paused immediately after resume, just let pause
            // be completed by the order of activity paused from clients.
            completePauseLocked(false, resuming);
        }
    }
    ActivityRecord prev = mResumedActivity;

    if (prev == null) { // 不存在resumed activity
        if (resuming == null) {
            Slog.wtf(TAG, "Trying to pause when nothing is resumed");
            mRootWindowContainer.resumeFocusedStacksTopActivities();
        }
        return false;
    }

    if (prev == resuming) { // 要pause的activity正在进行resume
        Slog.wtf(TAG, "Trying to pause activity that is in process of being resumed");
        return false;
    }

    if (DEBUG_STATES) Slog.v(TAG_STATES, "Moving to PAUSING: " + prev);
    else if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Start pausing: " + prev);

    prev.callServiceTrackeronActivityStatechange(PAUSING, true);
    if (mActivityTrigger != null) {
        mActivityTrigger.activityPauseTrigger(prev.intent, prev.info, prev.info.applicationInfo);
    }

    if (mActivityPluginDelegate != null && getWindowingMode() != WINDOWING_MODE_UNDEFINED) {
        mActivityPluginDelegate.activitySuspendNotification
            (prev.info.applicationInfo.packageName, getWindowingMode() == WINDOWING_MODE_FULLSCREEN, true);
    }
    mPausingActivity = prev;  // 设置为pausing activity
    mLastPausedActivity = prev;
    mLastNoHistoryActivity = prev.isNoHistory() ? prev : null;
    prev.setState(PAUSING, "startPausingLocked");
    prev.getTask().touchActiveTime();
    clearLaunchTime(prev);

    mAtmService.updateCpuStats();

    boolean pauseImmediately = false;
    if (resuming != null && (resuming.info.flags & FLAG_RESUME_WHILE_PAUSING) != 0) {
        // If the flag RESUME_WHILE_PAUSING is set, then continue to schedule the previous
        // activity to be paused, while at the same time resuming the new resume activity
        // only if the previous activity can't go into Pip since we want to give Pip
        // activities a chance to enter Pip before resuming the next activity.
        final boolean lastResumedCanPip = prev != null && prev.checkEnterPictureInPictureState(
                "shouldResumeWhilePausing", userLeaving);
        if (!lastResumedCanPip) {
            pauseImmediately = true;
        }
    }

    if (prev.attachedToProcess()) { // 所属进程存在
        if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Enqueueing pending pause: " + prev);
        try {// 打event tag: wm_pause_activity
            EventLogTags.writeWmPauseActivity(prev.mUserId, System.identityHashCode(prev),
                    prev.shortComponentName, "userLeaving=" + userLeaving);
            // 执行 onPause      
            mAtmService.getLifecycleManager().scheduleTransaction(prev.app.getThread(),
                    prev.appToken, PauseActivityItem.obtain(prev.finishing, userLeaving,
                            prev.configChangeFlags, pauseImmediately));
        } catch (Exception e) {
            // Ignore exception, if process died other code will cleanup.
            Slog.w(TAG, "Exception thrown during pause", e);
            mPausingActivity = null;
            mLastPausedActivity = null;
            mLastNoHistoryActivity = null;
        }
    } else {
        mPausingActivity = null;
        mLastPausedActivity = null;
        mLastNoHistoryActivity = null;
    }

    // If we are not going to sleep, we want to ensure the device is
    // awake until the next activity is started.
    if (!uiSleeping && !mAtmService.isSleepingOrShuttingDownLocked()) {
        mStackSupervisor.acquireLaunchWakelock();
    }

    if (mPausingActivity != null) {
        // Have the window manager pause its key dispatching until the new
        // activity has started.  If we're pausing the activity just because
        // the screen is being turned off and the UI is sleeping, don't interrupt
        // key dispatch; the same activity will pick it up again on wakeup.
        if (!uiSleeping) { // 停止事件派发
            prev.pauseKeyDispatchingLocked();
        } else if (DEBUG_PAUSE) {
             Slog.v(TAG_PAUSE, "Key dispatch not paused for screen off");
        }

        if (pauseImmediately) { // 不需等待，直接结束pause流程
            // If the caller said they don't want to wait for the pause, then complete
            // the pause now.
            completePauseLocked(false, resuming);
            return false;

        } else {
            prev.schedulePauseTimeout();
            return true;
        }

    } else {
        // This activity failed to schedule the
        // pause, so just treat it as being paused now.
        if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Activity not running, resuming next.");
        if (resuming == null) {
            mRootWindowContainer.resumeFocusedStacksTopActivities();
        }
        return false;
    }
}

```

## IActivityTaskManager#activityPaused
当activity执行onPause后，会向系统服务ActivityTaskManagerService上报完成操作
```
/// frameworks/base/core/java/android/app/servertransaction/PauseActivityItem.java
@Override
public void postExecute(ClientTransactionHandler client, IBinder token,
        PendingTransactionActions pendingActions) {
    if (mDontReport) {
        return;
    }
    try {
        // TODO(lifecycler): Use interface callback instead of AMS.
        ActivityTaskManager.getService().activityPaused(token);
    } catch (RemoteException ex) {
        throw ex.rethrowFromSystemServer();
    }
}

```

### ActivityTaskManagerService#activityPaused
```
/// frameworks/base/services/core/java/com/android/server/wm/ActivityTaskManagerService.java
@Override
public final void activityPaused(IBinder token) {
    final long origId = Binder.clearCallingIdentity();
    synchronized (mGlobalLock) {
        Trace.traceBegin(TRACE_TAG_WINDOW_MANAGER, "activityPaused");
        final ActivityRecord r = ActivityRecord.forTokenLocked(token);
        if (r != null) {
            r.activityPaused(false); // ActivityRecord执行activityPaused
        }
        Trace.traceEnd(TRACE_TAG_WINDOW_MANAGER);
    }
    Binder.restoreCallingIdentity(origId);
}

```

### ActivityRecord#activityPaused

```
void activityPaused(boolean timeout) {
    if (DEBUG_PAUSE) Slog.v(TAG_PAUSE,
            "Activity paused: token=" + appToken + ", timeout=" + timeout);

    final ActivityStack stack = getStack();

    if (stack != null) {
        removePauseTimeout();

        if (stack.mPausingActivity == this) {
            if (DEBUG_STATES) Slog.v(TAG_STATES, "Moving to PAUSED: " + this
                    + (timeout ? " (due to timeout)" : " (pause complete)"));
            mAtmService.deferWindowLayout();
            try { // stack 完成 pause 操作
                stack.completePauseLocked(true /* resumeNext */, null /* resumingActivity */);
            } finally {
                mAtmService.continueWindowLayout();
            }
            return;
        } else { // 此activity非mPausingActivity
            EventLogTags.writeWmFailedToPause(mUserId, System.identityHashCode(this),
                    shortComponentName, stack.mPausingActivity != null
                            ? stack.mPausingActivity.shortComponentName : "(none)");
            if (isState(PAUSING)) {
                callServiceTrackeronActivityStatechange(PAUSED, true);
                setState(PAUSED, "activityPausedLocked");
                if (finishing) {
                    if (DEBUG_PAUSE) Slog.v(TAG,
                            "Executing finish of failed to pause activity: " + this);
                    completeFinishing("activityPausedLocked");
                }
            }
        }
    }

    mRootWindowContainer.ensureActivitiesVisible(null, 0, !PRESERVE_WINDOWS);
}

```

## ActivityStack#completePauseLocked

```
/// frameworks/base/services/core/java/com/android/server/wm/ActivityStack.java
@VisibleForTesting
void completePauseLocked(boolean resumeNext, ActivityRecord resuming) {
    ActivityRecord prev = mPausingActivity;
    if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Complete pause: " + prev);

    if (prev != null) { // 处理完成待pause的activity状态变化
        prev.setWillCloseOrEnterPip(false);
        final boolean wasStopping = prev.isState(STOPPING);
        prev.callServiceTrackeronActivityStatechange(PAUSED, true);
        prev.setState(PAUSED, "completePausedLocked");  // 设置PAUSED
        if (prev.finishing) {
            if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Executing finish of activity: " + prev);
            prev = prev.completeFinishing("completePausedLocked");
        } else if (prev.hasProcess()) {
            if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Enqueue pending stop if needed: " + prev
                    + " wasStopping=" + wasStopping
                    + " visibleRequested=" + prev.mVisibleRequested);
            if (prev.deferRelaunchUntilPaused) {
                // Complete the deferred relaunch that was waiting for pause to complete.
                if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "Re-launching after pause: " + prev);
                prev.relaunchActivityLocked(prev.preserveWindowOnDeferredRelaunch);
            } else if (wasStopping) {
                // We are also stopping, the stop request must have gone soon after the pause.
                // We can't clobber it, because the stop confirmation will not be handled.
                // We don't need to schedule another stop, we only need to let it happen.
                prev.callServiceTrackeronActivityStatechange(STOPPING, true);
                prev.setState(STOPPING, "completePausedLocked");
            } else if (!prev.mVisibleRequested || shouldSleepOrShutDownActivities()) {
                // Clear out any deferred client hide we might currently have.
                prev.setDeferHidingClient(false);
                // If we were visible then resumeTopActivities will release resources before
                // stopping.
                prev.addToStopping(true /* scheduleIdle */, false /* idleDelayed */,
                        "completePauseLocked");
            }
        } else {
            if (DEBUG_PAUSE) Slog.v(TAG_PAUSE, "App died during pause, not stopping: " + prev);
            prev = null;
        }
        // It is possible the activity was freezing the screen before it was paused.
        // In that case go ahead and remove the freeze this activity has on the screen
        // since it is no longer visible.
        if (prev != null) {
            prev.stopFreezingScreenLocked(true /*force*/);
        }
        mPausingActivity = null;
    }

    if (resumeNext) { // resume  下一个activity
        final ActivityStack topStack = mRootWindowContainer.getTopDisplayFocusedStack();
        if (topStack != null && !topStack.shouldSleepOrShutDownActivities()) {
            mRootWindowContainer.resumeFocusedStacksTopActivities(topStack, prev, null);
        } else {
            checkReadyForSleep();
            final ActivityRecord top = topStack != null ? topStack.topRunningActivity() : null;
            if (top == null || (prev != null && top != prev)) {
                // If there are no more activities available to run, do resume anyway to start
                // something. Also if the top activity on the stack is not the just paused
                // activity, we need to go ahead and resume it to ensure we complete an
                // in-flight app switch.
                mRootWindowContainer.resumeFocusedStacksTopActivities();
            }
        }
    }

    if (prev != null) {
        prev.resumeKeyDispatchingLocked(); // 恢复key事件派发

        if (prev.hasProcess() && prev.cpuTimeAtResume > 0) {
            final long diff = prev.app.getCpuTime() - prev.cpuTimeAtResume;
            if (diff > 0) {
                final Runnable r = PooledLambda.obtainRunnable(
                        ActivityManagerInternal::updateForegroundTimeIfOnBattery,
                        mAtmService.mAmInternal, prev.info.packageName,
                        prev.info.applicationInfo.uid,
                        diff);
                mAtmService.mH.post(r);
            }
        }
        prev.cpuTimeAtResume = 0; // reset it
    }
    // 确保所有可见activity的状态
    mRootWindowContainer.ensureActivitiesVisible(resuming, 0, !PRESERVE_WINDOWS);

    // Notify when the task stack has changed, but only if visibilities changed (not just
    // focus). Also if there is an active pinned stack - we always want to notify it about
    // task stack changes, because its positioning may depend on it.
    if (mStackSupervisor.mAppVisibilitiesChangedSinceLastPause
            || (getDisplayArea() != null && getDisplayArea().hasPinnedTask())) {
        mAtmService.getTaskChangeNotificationController().notifyTaskStackChanged();
        mStackSupervisor.mAppVisibilitiesChangedSinceLastPause = false;
    }
}

```

在pause完成activity后，就调用mRootWindowContainer.resumeFocusedStacksTopActivities继续resume过程。回到resumeTopActivityInnerLocked，当执行pauseBackStacks完成pause操作后，如果有需要pause的activity，则会去检查需要启动的activity进程是否存在，若不存在则执行异步启动进程

```
if (pausing) {
    if (DEBUG_SWITCH || DEBUG_STATES) Slog.v(TAG_STATES,
            "resumeTopActivityLocked: Skip resume: need to start pausing");
    // At this point we want to put the upcoming activity's process
    // at the top of the LRU list, since we know we will be needing it
    // very soon and it would be a waste to let it get killed if it
    // happens to be sitting towards the end.
    if (next.attachedToProcess()) {
        next.app.updateProcessInfo(false /* updateServiceConnectionActivities */,
                true /* activityChange */, false /* updateOomAdj */,
                false /* addPendingTopUid */);
    } else if (!next.isProcessRunning()) {
        // Since the start-process is asynchronous, if we already know the process of next
        // activity isn't running, we can start the process earlier to save the time to wait
        // for the current activity to be paused.
        final boolean isTop = this == taskDisplayArea.getFocusedStack();
        mAtmService.startProcessAsync(next, false /* knownToBeDead */, isTop,
                isTop ? "pre-top-activity" : "pre-activity"); // 异步启动进程
    }
    if (lastResumed != null) {
        lastResumed.setWillCloseOrEnterPip(true);
    }
    return true;
} else if (mResumedActivity == next && next.isState(RESUMED)
        && taskDisplayArea.allResumedActivitiesComplete()) {
    // It is possible for the activity to be resumed when we paused back stacks above if the
    // next activity doesn't have to wait for pause to complete.
    // So, nothing else to-do except:
    // Make sure we have executed any pending transitions, since there
    // should be nothing left to do at this point.
    executeAppTransition(options);
    if (DEBUG_STATES) Slog.d(TAG_STATES,
            "resumeTopActivityLocked: Top activity resumed (dontWaitForPause) " + next);
    return true;
}


```

## ActivityStack resume已存在activity
若activity已经存在，则尝试 resume 操作

```
@resumeTopActivityUncheckedLocked

if (next.attachedToProcess()) {
    if (DEBUG_SWITCH) Slog.v(TAG_SWITCH, "Resume running: " + next
            + " stopped=" + next.stopped
            + " visibleRequested=" + next.mVisibleRequested);
    ...        
    // schedule launch ticks to collect information about slow apps.
    next.startLaunchTickingLocked();

    ActivityRecord lastResumedActivity =
            lastFocusedStack == null ? null : lastFocusedStack.mResumedActivity;
    final ActivityState lastState = next.getState();

    mAtmService.updateCpuStats();

    if (DEBUG_STATES) Slog.v(TAG_STATES, "Moving to RESUMED: " + next
            + " (in existing)");
    next.setState(RESUMED, "resumeTopActivityInnerLocked");

    next.app.updateProcessInfo(false /* updateServiceConnectionActivities */,
            true /* activityChange */, true /* updateOomAdj */,
            true /* addPendingTopUid */);
    ...

    try {
        final ClientTransaction transaction =
                ClientTransaction.obtain(next.app.getThread(), next.appToken);
        // Deliver all pending results.
        ArrayList<ResultInfo> a = next.results;
        if (a != null) {
            final int N = a.size();
            if (!next.finishing && N > 0) { // 添加ActivityResultItem ，回调activity的onActivityResult
                if (DEBUG_RESULTS) Slog.v(TAG_RESULTS,
                        "Delivering results to " + next + ": " + a);
                transaction.addCallback(ActivityResultItem.obtain(a));
            }
        }

        if (next.newIntents != null) { // 添加NewIntentItem，回调activity的onNewIntent
            transaction.addCallback(
                    NewIntentItem.obtain(next.newIntents, true /* resume */));
        }

        // Well the app will no longer be stopped.
        // Clear app token stopped state in window manager if needed.
        next.notifyAppResumed(next.stopped);
        // 打event log： 30007 wm_resume_activity (User|1|5),(Token|1|5),(Task ID|1|5),(Component Name|3)
        EventLogTags.writeWmResumeActivity(next.mUserId, System.identityHashCode(next),
                next.getTask().mTaskId, next.shortComponentName);

        next.setSleeping(false);
        mAtmService.getAppWarningsLocked().onResumeActivity(next);
        next.app.setPendingUiCleanAndForceProcessStateUpTo(mAtmService.mTopProcessState);
        next.clearOptionsLocked();
        transaction.setLifecycleStateRequest(
                ResumeActivityItem.obtain(next.app.getReportedProcState(),
                        dc.isNextTransitionForward()));  // 添加ResumeActivityItem，最终执行onResume
        mAtmService.getLifecycleManager().scheduleTransaction(transaction);  // 执行resume transaction操作

        if (DEBUG_STATES) Slog.d(TAG_STATES, "resumeTopActivityLocked: Resumed "
                + next);
        } catch (Exception e) { ... }

```

## ActivityStack 启动新 activity

```
    if (next.attachedToProcess()) { // activity已存在
      ...
    } else { // 不存在则启动新activity
        // Whoops, need to restart this activity!
        if (!next.hasBeenLaunched) {
            next.hasBeenLaunched = true;
        } else {
            if (SHOW_APP_STARTING_PREVIEW) {
                next.showStartingWindow(null /* prev */, false /* newTask */,
                        false /* taskSwich */);
            }
            if (DEBUG_SWITCH) Slog.v(TAG_SWITCH, "Restarting: " + next);
        }
        if (DEBUG_STATES) Slog.d(TAG_STATES, "resumeTopActivityLocked: Restarting " + next);
        mStackSupervisor.startSpecificActivity(next, true, true);
    }

```
启动Activity两种情况:
- 当进程存在时,会直接调用startSpecificActivity
- 当进程不存在时先启动进程, 当进程启动完成会通过attachApplication向系统报告完成启动,之后系统会再去继续启动Activity,此流程会再次调用startSpecificActivity


# ActivityStackSupervisor#startSpecificActivity

```
/// @frameworks/base/services/core/java/com/android/server/wm/ActivityStackSupervisor.java
  void startSpecificActivity(ActivityRecord r, boolean andResume, boolean checkConfig) {
      // Is this activity's application already running?
      final WindowProcessController wpc =
              mService.getProcessController(r.processName, r.info.applicationInfo.uid);

      boolean knownToBeDead = false;
      if (wpc != null && wpc.hasThread()) { // 进程已经存在
          try {
              if (mPerfBoost != null) {
                  Slog.i(TAG, "The Process " + r.processName + " Already Exists in BG. So sending its PID: " + wpc.getPid());
                  mPerfBoost.perfHint(BoostFramework.VENDOR_HINT_FIRST_LAUNCH_BOOST, r.processName, wpc.getPid(), BoostFramework.Launch.TYPE_START_APP_FROM_BG);
              }
              realStartActivityLocked(r, wpc, andResume, checkConfig); // 启动新activity
              return;
          } catch (RemoteException e) {
              Slog.w(TAG, "Exception when starting activity "
                      + r.intent.getComponent().flattenToShortString(), e);
          }

          // If a dead object exception was thrown -- fall through to
          // restart the application.
          knownToBeDead = true;
      }

      r.notifyUnknownVisibilityLaunchedForKeyguardTransition();

      final boolean isTop = andResume && r.isTopRunningActivity();
      // 启动activity所在进程
      mService.startProcessAsync(r, knownToBeDead, isTop, isTop ? "top-activity" : "activity");
  }

```

## ActivityStackSupervisor#realStartActivityLocked

```
/// @frameworks/base/services/core/java/com/android/server/wm/ActivityStackSupervisor.java
boolean realStartActivityLocked(ActivityRecord r, WindowProcessController proc,
        boolean andResume, boolean checkConfig) throws RemoteException {

    if (!mRootWindowContainer.allPausedActivitiesComplete()) { // 所有pause activity需要完成
        // While there are activities pausing we skipping starting any new activities until
        // pauses are complete. NOTE: that we also do this for activities that are starting in
        // the paused state because they will first be resumed then paused on the client side.
        if (DEBUG_SWITCH || DEBUG_PAUSE || DEBUG_STATES) Slog.v(TAG_PAUSE,
                "realStartActivityLocked: Skipping start of r=" + r
                + " some activities pausing...");
        return false;
    }

    final Task task = r.getTask();
    final ActivityStack stack = task.getStack();

    beginDeferResume();

    try {
        r.startFreezingScreenLocked(proc, 0);

        // schedule launch ticks to collect information about slow apps.
        r.startLaunchTickingLocked();

        r.setProcess(proc); // 设置process信息

        // Ensure activity is allowed to be resumed after process has set.
        if (andResume && !r.canResumeByCompat()) {
            andResume = false;
        }

        r.notifyUnknownVisibilityLaunchedForKeyguardTransition();

        // Have the window manager re-evaluate the orientation of the screen based on the new
        // activity order.  Note that as a result of this, it can call back into the activity
        // manager with a new orientation.  We don't care about that, because the activity is
        // not currently running so we are just restarting it anyway.
        if (checkConfig) {
            // Deferring resume here because we're going to launch new activity shortly.
            // We don't want to perform a redundant launch of the same record while ensuring
            // configurations and trying to resume top activity of focused stack.
            mRootWindowContainer.ensureVisibilityAndConfig(r, r.getDisplayId(),
                    false /* markFrozenIfConfigChanged */, true /* deferResume */);
        }

        if (r.getRootTask().checkKeyguardVisibility(r, true /* shouldBeVisible */,
                true /* isTop */) && r.allowMoveToFront()) {
            // We only set the visibility to true if the activity is not being launched in
            // background, and is allowed to be visible based on keyguard state. This avoids
            // setting this into motion in window manager that is later cancelled due to later
            // calls to ensure visible activities that set visibility back to false.
            r.setVisibility(true);
        }

        final int applicationInfoUid =
                (r.info.applicationInfo != null) ? r.info.applicationInfo.uid : -1;
        if ((r.mUserId != proc.mUserId) || (r.info.applicationInfo.uid != applicationInfoUid)) {
            Slog.wtf(TAG,
                    "User ID for activity changing for " + r
                            + " appInfo.uid=" + r.info.applicationInfo.uid
                            + " info.ai.uid=" + applicationInfoUid
                            + " old=" + r.app + " new=" + proc);
        }

        r.launchCount++;
        r.lastLaunchTime = SystemClock.uptimeMillis();
        proc.setLastActivityLaunchTime(r.lastLaunchTime);

        if (DEBUG_ALL) Slog.v(TAG, "Launching: " + r);

        final LockTaskController lockTaskController = mService.getLockTaskController();
        if (task.mLockTaskAuth == LOCK_TASK_AUTH_LAUNCHABLE
                || task.mLockTaskAuth == LOCK_TASK_AUTH_LAUNCHABLE_PRIV
                || (task.mLockTaskAuth == LOCK_TASK_AUTH_WHITELISTED
                        && lockTaskController.getLockTaskModeState()
                                == LOCK_TASK_MODE_LOCKED)) {
            lockTaskController.startLockTaskMode(task, false, 0 /* blank UID */);
        }

        try {
            if (!proc.hasThread()) {
                throw new RemoteException();
            }
            List<ResultInfo> results = null;
            List<ReferrerIntent> newIntents = null;
            if (andResume) {
                // We don't need to deliver new intents and/or set results if activity is going
                // to pause immediately after launch.
                results = r.results;
                newIntents = r.newIntents;
            }
            if (DEBUG_SWITCH) Slog.v(TAG_SWITCH,
                    "Launching: " + r + " savedState=" + r.getSavedState()
                            + " with results=" + results + " newIntents=" + newIntents
                            + " andResume=" + andResume);
            // 打event log: 30006 wm_restart_activity (User|1|5),(Token|1|5),(Task ID|1|5),(Component Name|3)
            EventLogTags.writeWmRestartActivity(r.mUserId, System.identityHashCode(r),
                    task.mTaskId, r.shortComponentName);
            if (r.isActivityTypeHome()) {
                // Home process is the root process of the task.
                updateHomeProcess(task.getBottomMostActivity().app);
            }
            mService.getPackageManagerInternalLocked().notifyPackageUse(
                    r.intent.getComponent().getPackageName(), NOTIFY_PACKAGE_USE_ACTIVITY);
            r.setSleeping(false);
            r.forceNewConfig = false;
            mService.getAppWarningsLocked().onStartActivity(r);
            r.compat = mService.compatibilityInfoForPackageLocked(r.info.applicationInfo);

            // Because we could be starting an Activity in the system process this may not go
            // across a Binder interface which would create a new Configuration. Consequently
            // we have to always create a new Configuration here.

            final MergedConfiguration mergedConfiguration = new MergedConfiguration(
                    proc.getConfiguration(), r.getMergedOverrideConfiguration());
            r.setLastReportedConfiguration(mergedConfiguration);

            logIfTransactionTooLarge(r.intent, r.getSavedState());


            // Create activity launch transaction.
            final ClientTransaction clientTransaction = ClientTransaction.obtain(
                    proc.getThread(), r.appToken);

            final DisplayContent dc = r.getDisplay().mDisplayContent;
            // 添加 LaunchActivityItem
            clientTransaction.addCallback(LaunchActivityItem.obtain(new Intent(r.intent),
                    System.identityHashCode(r), r.info,
                    // TODO: Have this take the merged configuration instead of separate global
                    // and override configs.
                    mergedConfiguration.getGlobalConfiguration(),
                    mergedConfiguration.getOverrideConfiguration(), r.compat,
                    r.launchedFromPackage, task.voiceInteractor, proc.getReportedProcState(),
                    r.getSavedState(), r.getPersistentSavedState(), results, newIntents,
                    dc.isNextTransitionForward(), proc.createProfilerInfoIfNeeded(),
                    r.assistToken, r.createFixedRotationAdjustmentsIfNeeded()));

            // Set desired final state.
            final ActivityLifecycleItem lifecycleItem;
            if (andResume) { // 需要resume，则添加ResumeActivityItem
                lifecycleItem = ResumeActivityItem.obtain(dc.isNextTransitionForward());
            } else { // 否则添加PauseActivityItem
                lifecycleItem = PauseActivityItem.obtain();
            }
            clientTransaction.setLifecycleStateRequest(lifecycleItem);

            // Schedule transaction. //  执行launch transaction
            mService.getLifecycleManager().scheduleTransaction(clientTransaction);

            if ((proc.mInfo.privateFlags & ApplicationInfo.PRIVATE_FLAG_CANT_SAVE_STATE) != 0
                    && mService.mHasHeavyWeightFeature) {
                // This may be a heavy-weight process! Note that the package manager will ensure
                // that only activity can run in the main process of the .apk, which is the only
                // thing that will be considered heavy-weight.
                if (proc.mName.equals(proc.mInfo.packageName)) {
                    if (mService.mHeavyWeightProcess != null
                            && mService.mHeavyWeightProcess != proc) {
                        Slog.w(TAG, "Starting new heavy weight process " + proc
                                + " when already running "
                                + mService.mHeavyWeightProcess);
                    }
                    mService.setHeavyWeightProcess(r);
                }
            }

        } catch (RemoteException e) {
            if (r.launchFailed) {
                // This is the second time we failed -- finish activity and give up.
                Slog.e(TAG, "Second failure launching "
                        + r.intent.getComponent().flattenToShortString() + ", giving up", e);
                proc.appDied("2nd-crash");
                r.finishIfPossible("2nd-crash", false /* oomAdj */);
                return false;
            }

            // This is the first time we failed -- restart process and
            // retry.
            r.launchFailed = true;
            proc.removeActivity(r);
            throw e;
        }
    } finally {
        endDeferResume();
    }

    r.launchFailed = false;

    // TODO(lifecycler): Resume or pause requests are done as part of launch transaction,
    // so updating the state should be done accordingly.
    if (andResume && readyToResume()) {
        // As part of the process of launching, ActivityThread also performs
        // a resume.
        stack.minimalResumeActivityLocked(r);
    } else {
        // This activity is not starting in the resumed state... which should look like we asked
        // it to pause+stop (but remain visible), and it has done so and reported back the
        // current icicle and other state.
        if (DEBUG_STATES) Slog.v(TAG_STATES,
                "Moving to PAUSED: " + r + " (starting in paused state)");
        r.setState(PAUSED, "realStartActivityLocked");
        mRootWindowContainer.executeAppTransitionForAllDisplay();
    }
    // Perform OOM scoring after the activity state is set, so the process can be updated with
    // the latest state.
    proc.onStartActivity(mService.mTopProcessState, r.info);

    // Launch the new version setup screen if needed.  We do this -after-
    // launching the initial activity (that is, home), so that it can have
    // a chance to initialize itself while in the background, making the
    // switch back to it faster and look better.
    if (mRootWindowContainer.isTopDisplayFocusedStack(stack)) {
        mService.getActivityStartController().startSetupActivity();
    }

    // Update any services we are bound to that might care about whether
    // their client may have activities.
    if (r.app != null) {
        r.app.updateServiceConnectionActivities();
    }

    return true;
}

```

接下来会执行transaction,通过binder向app发送请求, 之后app执行相关启动Activity逻辑

# ClientLifecycleManager#scheduleTransaction

```
/// @frameworks/base/services/core/java/com/android/server/wm/ClientLifecycleManager.java
/**
 * Schedule a transaction, which may consist of multiple callbacks and a lifecycle request.
 * @param transaction A sequence of client transaction items.
 * @throws RemoteException
 *
 * @see ClientTransaction
 */
void scheduleTransaction(ClientTransaction transaction) throws RemoteException {
    final IApplicationThread client = transaction.getClient();
    transaction.schedule(); // 回调app端
    if (!(client instanceof Binder)) {
        // If client is not an instance of Binder - it's a remote call and at this point it is
        // safe to recycle the object. All objects used for local calls will be recycled after
        // the transaction is executed on client in ActivityThread.
        transaction.recycle();
    }
}

```

## ClientTransaction#schedule


```
/// @frameworks/base/core/java/android/app/servertransaction/ClientTransaction.java
/**
 * Schedule the transaction after it was initialized. It will be send to client and all its
 * individual parts will be applied in the following sequence:
 * 1. The client calls {@link #preExecute(ClientTransactionHandler)}, which triggers all work
 *    that needs to be done before actually scheduling the transaction for callbacks and
 *    lifecycle state request.
 * 2. The transaction message is scheduled.
 * 3. The client calls {@link TransactionExecutor#execute(ClientTransaction)}, which executes
 *    all callbacks and necessary lifecycle transitions.
 */
public void schedule() throws RemoteException {
    mClient.scheduleTransaction(this);  // 调用IApplicationThread#scheduleTransaction
}

```

接下来进入app端，会调用 IApplicationThread.Stub#scheduleTransaction

## IApplicationThread.Stub#scheduleTransaction

```
/// @frameworks/base/core/java/android/app/ActivityThread.java
@Override
public void scheduleTransaction(ClientTransaction transaction) throws RemoteException {
    ActivityThread.this.scheduleTransaction(transaction);
}

```

接下来调用ActivityThread#scheduleTransaction，而接下来调用ActivityThread实际上继承ClientTransactionHandler，因此实际调用 ClientTransactionHandler#scheduleTransaction

### ClientTransactionHandler#scheduleTransaction

```
/// @frameworks/base/core/java/android/app/ClientTransactionHandler.java
/** Prepare and schedule transaction for execution. */
void scheduleTransaction(ClientTransaction transaction) {
    transaction.preExecute(this);
    sendMessage(ActivityThread.H.EXECUTE_TRANSACTION, transaction); // 向handler中投递消息
}

```

### ActivityThread#H#handleMessage

```
case EXECUTE_TRANSACTION:
    final ClientTransaction transaction = (ClientTransaction) msg.obj;
    mTransactionExecutor.execute(transaction);
    if (isSystem()) {
        // Client transactions inside system process are recycled on the client side
        // instead of ClientLifecycleManager to avoid being cleared before this
        // message is handled.
        transaction.recycle();
    }
    // TODO(lifecycler): Recycle locally scheduled transactions.
    break;

```

## TransactionExecutor#execute

```
/// @frameworks/base/core/java/android/app/servertransaction/TransactionExecutor.java
/**
 * Resolve transaction.
 * First all callbacks will be executed in the order they appear in the list. If a callback
 * requires a certain pre- or post-execution state, the client will be transitioned accordingly.
 * Then the client will cycle to the final lifecycle state if provided. Otherwise, it will
 * either remain in the initial state, or last state needed by a callback.
 */
public void execute(ClientTransaction transaction) {
    if (DEBUG_RESOLVER) Slog.d(TAG, tId(transaction) + "Start resolving transaction");

    final IBinder token = transaction.getActivityToken();
    if (token != null) {
        final Map<IBinder, ClientTransactionItem> activitiesToBeDestroyed =
                mTransactionHandler.getActivitiesToBeDestroyed();
        final ClientTransactionItem destroyItem = activitiesToBeDestroyed.get(token);
        if (destroyItem != null) { // 处理destroy
            if (transaction.getLifecycleStateRequest() == destroyItem) {
                // It is going to execute the transaction that will destroy activity with the
                // token, so the corresponding to-be-destroyed record can be removed.
                activitiesToBeDestroyed.remove(token);
            }
            if (mTransactionHandler.getActivityClient(token) == null) {
                // The activity has not been created but has been requested to destroy, so all
                // transactions for the token are just like being cancelled.
                Slog.w(TAG, tId(transaction) + "Skip pre-destroyed transaction:\n"
                        + transactionToString(transaction, mTransactionHandler));
                return;
            }
        }
    }

    if (DEBUG_RESOLVER) Slog.d(TAG, transactionToString(transaction, mTransactionHandler));

    executeCallbacks(transaction); // 执行callback, 如LaunchActivityItem

    executeLifecycleState(transaction); // 执行LifecycleState, 如ResumeActivityItem或PauseActivityItem
    mPendingActions.clear();
    if (DEBUG_RESOLVER) Slog.d(TAG, tId(transaction) + "End resolving transaction");
}

```

## TransactionExecutor#executeLifecycleState

```
/// @frameworks/base/core/java/android/app/servertransaction/TransactionExecutor.java
/** Transition to the final state if requested by the transaction. */
private void executeLifecycleState(ClientTransaction transaction) {
    final ActivityLifecycleItem lifecycleItem = transaction.getLifecycleStateRequest();
    if (lifecycleItem == null) {
        // No lifecycle request, return early.
        return;
    }

    final IBinder token = transaction.getActivityToken();
    final ActivityClientRecord r = mTransactionHandler.getActivityClient(token);
    if (DEBUG_RESOLVER) {
        Slog.d(TAG, tId(transaction) + "Resolving lifecycle state: "
                + lifecycleItem + " for activity: "
                + getShortActivityName(token, mTransactionHandler));
    }

    if (r == null) {
        // Ignore requests for non-existent client records for now.
        return;
    }

    // Cycle to the state right before the final requested state.
    cycleToPath(r, lifecycleItem.getTargetState(), true /* excludeLastState */, transaction);

    // Execute the final transition with proper parameters.
    lifecycleItem.execute(mTransactionHandler, token, mPendingActions);
    lifecycleItem.postExecute(mTransactionHandler, token, mPendingActions);
}

```

上面设置的LifecycleStateRequest是ResumeActivityItem, 则执行的是 ResumeActivityItem#execute

### ResumeActivityItem#execute

```
/// @frameworks/base/core/java/android/app/servertransaction/ResumeActivityItem.java
@Override
public void execute(ClientTransactionHandler client, IBinder token,
        PendingTransactionActions pendingActions) {
    Trace.traceBegin(TRACE_TAG_ACTIVITY_MANAGER, "activityResume");
    // 此处的 client 实际上是 ActivityThread
    client.handleResumeActivity(token, true /* finalStateRequest */, mIsForward,
            "RESUME_ACTIVITY");
    Trace.traceEnd(TRACE_TAG_ACTIVITY_MANAGER);
}

```

接下来执行的是 ActivityThread#handleResumeActivity

### ActivityThread#handleResumeActivity

```
@Override
public void handleResumeActivity(IBinder token, boolean finalStateRequest, boolean isForward,
        String reason) {
    // If we are getting ready to gc after going to the background, well
    // we are back active so skip it.
    unscheduleGcIdler();
    mSomeActivitiesChanged = true;

    // TODO Push resumeArgs into the activity for consideration // 执行resume
    final ActivityClientRecord r = performResumeActivity(token, finalStateRequest, reason);
    if (r == null) {
        // We didn't actually resume the activity, so skipping any follow-up actions.
        return;
    }
    if (mActivitiesToBeDestroyed.containsKey(token)) {
        // Although the activity is resumed, it is going to be destroyed. So the following
        // UI operations are unnecessary and also prevents exception because its token may
        // be gone that window manager cannot recognize it. All necessary cleanup actions
        // performed below will be done while handling destruction.
        return;
    }

    final Activity a = r.activity;

    if (localLOGV) {
        Slog.v(TAG, "Resume " + r + " started activity: " + a.mStartedActivity
                + ", hideForNow: " + r.hideForNow + ", finished: " + a.mFinished);
    }

    final int forwardBit = isForward
            ? WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION : 0;

    // If the window hasn't yet been added to the window manager,
    // and this guy didn't finish itself or start another activity,
    // then go ahead and add the window.
    boolean willBeVisible = !a.mStartedActivity;
    if (!willBeVisible) {
        try {
            willBeVisible = ActivityTaskManager.getService().willActivityBeVisible(
                    a.getActivityToken());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }
    // 添加window显示
    if (r.window == null && !a.mFinished && willBeVisible) {
        r.window = r.activity.getWindow();
        View decor = r.window.getDecorView();
        decor.setVisibility(View.INVISIBLE);
        ViewManager wm = a.getWindowManager();
        WindowManager.LayoutParams l = r.window.getAttributes();
        a.mDecor = decor;
        l.type = WindowManager.LayoutParams.TYPE_BASE_APPLICATION;
        l.softInputMode |= forwardBit;
        if (r.mPreserveWindow) {
            a.mWindowAdded = true;
            r.mPreserveWindow = false;
            // Normally the ViewRoot sets up callbacks with the Activity
            // in addView->ViewRootImpl#setView. If we are instead reusing
            // the decor view we have to notify the view root that the
            // callbacks may have changed.
            ViewRootImpl impl = decor.getViewRootImpl();
            if (impl != null) {
                impl.notifyChildRebuilt();
            }
        }
        if (a.mVisibleFromClient) {
            if (!a.mWindowAdded) {
                a.mWindowAdded = true;
                wm.addView(decor, l);
            } else {
                // The activity will get a callback for this {@link LayoutParams} change
                // earlier. However, at that time the decor will not be set (this is set
                // in this method), so no action will be taken. This call ensures the
                // callback occurs with the decor set.
                a.onWindowAttributesChanged(l);
            }
        }

        // If the window has already been added, but during resume
        // we started another activity, then don't yet make the
        // window visible.
    } else if (!willBeVisible) {
        if (localLOGV) Slog.v(TAG, "Launch " + r + " mStartedActivity set");
        r.hideForNow = true;
    }

    // Get rid of anything left hanging around.
    cleanUpPendingRemoveWindows(r, false /* force */);

    // The window is now visible if it has been added, we are not
    // simply finishing, and we are not starting another activity.
    if (!r.activity.mFinished && willBeVisible && r.activity.mDecor != null && !r.hideForNow) {
        if (r.newConfig != null) {
            performConfigurationChangedForActivity(r, r.newConfig);
            if (DEBUG_CONFIGURATION) {
                Slog.v(TAG, "Resuming activity " + r.activityInfo.name + " with newConfig "
                        + r.activity.mCurrentConfig);
            }
            r.newConfig = null;
        }
        if (localLOGV) Slog.v(TAG, "Resuming " + r + " with isForward=" + isForward);
        ViewRootImpl impl = r.window.getDecorView().getViewRootImpl();
        WindowManager.LayoutParams l = impl != null
                ? impl.mWindowAttributes : r.window.getAttributes();
        if ((l.softInputMode
                & WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION)
                != forwardBit) {
            l.softInputMode = (l.softInputMode
                    & (~WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION))
                    | forwardBit;
            if (r.activity.mVisibleFromClient) {
                ViewManager wm = a.getWindowManager();
                View decor = r.window.getDecorView();
                wm.updateViewLayout(decor, l);
            }
        }

        r.activity.mVisibleFromServer = true;
        mNumVisibleActivities++;
        if (r.activity.mVisibleFromClient) {
            r.activity.makeVisible();
        }
    }

    r.nextIdle = mNewActivities;
    mNewActivities = r;
    if (localLOGV) Slog.v(TAG, "Scheduling idle handler for " + r);
    Looper.myQueue().addIdleHandler(new Idler());
}

```
