本篇分析ActivityTaskManagerService(以下简称ATMS)启动Activity的流程

# ATMS#startActivity
```
@Override
public final int startActivity(IApplicationThread caller, String callingPackage,
        String callingFeatureId, Intent intent, String resolvedType, IBinder resultTo,
        String resultWho, int requestCode, int startFlags, ProfilerInfo profilerInfo,
        Bundle bOptions) {
    return startActivityAsUser(caller, callingPackage, callingFeatureId, intent, resolvedType,
            resultTo, resultWho, requestCode, startFlags, profilerInfo, bOptions,
            UserHandle.getCallingUserId()); // 直接调用startActivityAsUser, 多了一个userId参数
}

@Override
public int startActivityAsUser(IApplicationThread caller, String callingPackage,
        String callingFeatureId, Intent intent, String resolvedType, IBinder resultTo,
        String resultWho, int requestCode, int startFlags, ProfilerInfo profilerInfo,
        Bundle bOptions, int userId) {
    return startActivityAsUser(caller, callingPackage, callingFeatureId, intent, resolvedType,
            resultTo, resultWho, requestCode, startFlags, profilerInfo, bOptions, userId,
            true /*validateIncomingUser*/);  // 继续调用重载方法
}
```

# ATMS#startActivityAsUser
```
private int startActivityAsUser(IApplicationThread caller, String callingPackage,
        @Nullable String callingFeatureId, Intent intent, String resolvedType,
        IBinder resultTo, String resultWho, int requestCode, int startFlags,
        ProfilerInfo profilerInfo, Bundle bOptions, int userId, boolean validateIncomingUser) {
    assertPackageMatchesCallingUid(callingPackage); // 检查包名是否与 calling UID 匹配
    enforceNotIsolatedCaller("startActivityAsUser");

    // 对 userId 做一些检查
    userId = getActivityStartController().checkTargetUser(userId, validateIncomingUser,
            Binder.getCallingPid(), Binder.getCallingUid(), "startActivityAsUser");

    // TODO: Switch to user app stacks here.
    // 使用Factory 创建 ActivityStarter 并执行
    return getActivityStartController().obtainStarter(intent, "startActivityAsUser")
            .setCaller(caller)
            .setCallingPackage(callingPackage)
            .setCallingFeatureId(callingFeatureId)
            .setResolvedType(resolvedType)
            .setResultTo(resultTo)
            .setResultWho(resultWho)
            .setRequestCode(requestCode)
            .setStartFlags(startFlags)
            .setProfilerInfo(profilerInfo)
            .setActivityOptions(bOptions)
            .setUserId(userId)
            .execute();

}
```

## getActivityStartController().obtainStarter
getActivityStartController() 返回的是 ActivityStartController 对象, 在ATMS的 initialize方法中创建, 此处创建会创建默认的 DefaultFactory
```
ActivityStartController(ActivityTaskManagerService service) {
    this(service, service.mStackSupervisor,
            new DefaultFactory(service, service.mStackSupervisor,
                new ActivityStartInterceptor(service, service.mStackSupervisor)));
}
```
obtainStarter 方法如下:
```
/**
 * @return A starter to configure and execute starting an activity. It is valid until after
 *         {@link ActivityStarter#execute} is invoked. At that point, the starter should be
 *         considered invalid and no longer modified or used.
 */
ActivityStarter obtainStarter(Intent intent, String reason) {
    return mFactory.obtain().setIntent(intent).setReason(reason); // mFactory即 DefaultFactory
}
```

DefaultFactory的obtain方法尝试从 mStarterPool 池中取出一个元素, 如果没有则创建一个新的元素
```
@Override
public ActivityStarter obtain() {
    ActivityStarter starter = mStarterPool.acquire();

    if (starter == null) {
        starter = new ActivityStarter(mController, mService, mSupervisor, mInterceptor);
    }

    return starter;
}
```
ActivityStarter 创建完成后通过一系列setXXX方法来初始化内部的Request,如:
```
ActivityStarter setResultTo(IBinder resultTo) {
    mRequest.resultTo = resultTo;
    return this;
}
```

# ActivityStarter#execute
从这个函数的名称来看,它将开启启动Activity之旅
```
/**
 * Resolve necessary information according the request parameters provided earlier, and execute
 * the request which begin the journey of starting an activity.
 * @return The starter result.
 */
int execute() {
    try {
        // Refuse possible leaked file descriptors
        if (mRequest.intent != null && mRequest.intent.hasFileDescriptors()) {
            throw new IllegalArgumentException("File descriptors passed in Intent");
        }

        final LaunchingState launchingState;
        synchronized (mService.mGlobalLock) { // 追踪启动状态
            final ActivityRecord caller = ActivityRecord.forTokenLocked(mRequest.resultTo);
            launchingState = mSupervisor.getActivityMetricsLogger().notifyActivityLaunching(
                    mRequest.intent, caller);
        }

        // If the caller hasn't already resolved the activity, we're willing
        // to do so here. If the caller is already holding the WM lock here,
        // and we need to check dynamic Uri permissions, then we're forced
        // to assume those permissions are denied to avoid deadlocking.
        if (mRequest.activityInfo == null) {
            mRequest.resolveActivity(mSupervisor);  // 解析Intent对应的activityInfo
        }

        int res;
        synchronized (mService.mGlobalLock) {
            final boolean globalConfigWillChange = mRequest.globalConfig != null
                    && mService.getGlobalConfiguration().diff(mRequest.globalConfig) != 0;
            final ActivityStack stack = mRootWindowContainer.getTopDisplayFocusedStack();// 获取焦点ActivityStack
            if (stack != null) {
                stack.mConfigWillChange = globalConfigWillChange;
            }
            if (DEBUG_CONFIGURATION) {
                Slog.v(TAG_CONFIGURATION, "Starting activity when config will change = "
                        + globalConfigWillChange);
            }

            final long origId = Binder.clearCallingIdentity(); // 真正执行启动前清除调用者身份

            res = resolveToHeavyWeightSwitcherIfNeeded(); // 处理 heavy-weight process
            if (res != START_SUCCESS) {
                return res;
            }
            // 执行请求
            res = executeRequest(mRequest);

            Binder.restoreCallingIdentity(origId); // 执行完毕后恢复身份

            if (globalConfigWillChange) { // 若有config变化，通知ATMS更新
                // If the caller also wants to switch to a new configuration, do so now.
                // This allows a clean switch, as we are waiting for the current activity
                // to pause (so we will not destroy it), and have not yet started the
                // next activity.
                mService.mAmInternal.enforceCallingPermission(
                        android.Manifest.permission.CHANGE_CONFIGURATION,
                        "updateConfiguration()");
                if (stack != null) {
                    stack.mConfigWillChange = false;
                }
                if (DEBUG_CONFIGURATION) {
                    Slog.v(TAG_CONFIGURATION,
                            "Updating to new configuration after starting activity.");
                }
                mService.updateConfigurationLocked(mRequest.globalConfig, null, false);
            }

            // Notify ActivityMetricsLogger that the activity has launched.
            // ActivityMetricsLogger will then wait for the windows to be drawn and populate
            // WaitResult.
            mSupervisor.getActivityMetricsLogger().notifyActivityLaunched(launchingState, res,
                    mLastStartActivityRecord);
            return getExternalResult(mRequest.waitResult == null ? res
                    : waitForResult(res, mLastStartActivityRecord));
        }
    } finally {
        onExecutionComplete();  // 结束执行,进行回收操作
    }
}
```

## ActivityStarter$Request#resolveActivity
解析ActivityInfo
```
/**
 * Resolve activity from the given intent for this launch.
 */
void resolveActivity(ActivityStackSupervisor supervisor) {
    if (realCallingPid == Request.DEFAULT_REAL_CALLING_PID) {
        realCallingPid = Binder.getCallingPid();
    }
    if (realCallingUid == Request.DEFAULT_REAL_CALLING_UID) {
        realCallingUid = Binder.getCallingUid();
    }

    if (callingUid >= 0) {
        callingPid = -1;
    } else if (caller == null) {
        callingPid = realCallingPid;
        callingUid = realCallingUid;
    } else {
        callingPid = callingUid = -1;
    }

    // To determine the set of needed Uri permission grants, we need the
    // "resolved" calling UID, where we try our best to identify the
    // actual caller that is starting this activity
    int resolvedCallingUid = callingUid;
    if (caller != null) {
        synchronized (supervisor.mService.mGlobalLock) { // 通过app info获取uid
            final WindowProcessController callerApp = supervisor.mService
                    .getProcessController(caller);
            if (callerApp != null) {
                resolvedCallingUid = callerApp.mInfo.uid;
            }
        }
    }

    // Save a copy in case ephemeral needs it
    ephemeralIntent = new Intent(intent);
    // Don't modify the client's object!
    intent = new Intent(intent);
    if (intent.getComponent() != null
            && !(Intent.ACTION_VIEW.equals(intent.getAction()) && intent.getData() == null)
            && !Intent.ACTION_INSTALL_INSTANT_APP_PACKAGE.equals(intent.getAction())
            && !Intent.ACTION_RESOLVE_INSTANT_APP_PACKAGE.equals(intent.getAction())
            && supervisor.mService.getPackageManagerInternalLocked()
                    .isInstantAppInstallerComponent(intent.getComponent())) {
        // Intercept intents targeted directly to the ephemeral installer the ephemeral
        // installer should never be started with a raw Intent; instead adjust the intent
        // so it looks like a "normal" instant app launch.
        intent.setComponent(null /* component */);
    }

    resolveInfo = supervisor.resolveIntent(intent, resolvedType, userId,
            0 /* matchFlags */,
            computeResolveFilterUid(callingUid, realCallingUid, filterCallingUid)); // 解析Intent
    ... // Special case for managed profiles, if attempting to launch non-cryto aware app in a locked managed profile

    // Collect information about the target of the Intent.
    activityInfo = supervisor.resolveActivity(intent, resolveInfo, startFlags,
            profilerInfo); // 收集要启动的activity信息

    // Carefully collect grants without holding lock
    if (activityInfo != null) { // 收集启动目标需要的权限
        intentGrants = supervisor.mService.mUgmInternal.checkGrantUriPermissionFromIntent(
                intent, resolvedCallingUid, activityInfo.applicationInfo.packageName,
                UserHandle.getUserId(activityInfo.applicationInfo.uid));
    }
}
```

## ActivityStarter#executeRequest
进一步执行启动的Request

```
/**
 * Executing activity start request and starts the journey of starting an activity. Here
 * begins with performing several preliminary checks. The normally activity launch flow will
 * go through {@link #startActivityUnchecked} to {@link #startActivityInner}.
 */
private int executeRequest(Request request) {
    if (TextUtils.isEmpty(request.reason)) {
        throw new IllegalArgumentException("Need to specify a reason.");
    }
    mLastStartReason = request.reason;
    mLastStartActivityTimeMs = System.currentTimeMillis();
    mLastStartActivityRecord = null;

    final IApplicationThread caller = request.caller;
    Intent intent = request.intent;
    NeededUriGrants intentGrants = request.intentGrants;
    String resolvedType = request.resolvedType;
    ActivityInfo aInfo = request.activityInfo;
    ResolveInfo rInfo = request.resolveInfo;
    final IVoiceInteractionSession voiceSession = request.voiceSession;
    final IBinder resultTo = request.resultTo;
    String resultWho = request.resultWho;
    int requestCode = request.requestCode;
    int callingPid = request.callingPid;
    int callingUid = request.callingUid;
    String callingPackage = request.callingPackage;
    String callingFeatureId = request.callingFeatureId;
    final int realCallingPid = request.realCallingPid;
    final int realCallingUid = request.realCallingUid;
    final int startFlags = request.startFlags;
    final SafeActivityOptions options = request.activityOptions;
    Task inTask = request.inTask;

    int err = ActivityManager.START_SUCCESS;
    // Pull the optional Ephemeral Installer-only bundle out of the options early.
    final Bundle verificationBundle =
            options != null ? options.popAppVerificationBundle() : null;

    WindowProcessController callerApp = null;
    if (caller != null) { // 获取caller 应用的信息
        callerApp = mService.getProcessController(caller);
        if (callerApp != null) {
            callingPid = callerApp.getPid();
            callingUid = callerApp.mInfo.uid;
        } else {
            Slog.w(TAG, "Unable to find app for caller " + caller + " (pid=" + callingPid
                    + ") when starting: " + intent.toString());
            err = ActivityManager.START_PERMISSION_DENIED;
        }
    }

    final int userId = aInfo != null && aInfo.applicationInfo != null
            ? UserHandle.getUserId(aInfo.applicationInfo.uid) : 0;
    if (err == ActivityManager.START_SUCCESS) {
        Slog.i(TAG, "START u" + userId + " {" + intent.toShortString(true, true, true, false)
                + "} from uid " + callingUid);
    }

    ActivityRecord sourceRecord = null;
    ActivityRecord resultRecord = null;
    if (resultTo != null) { // 解析原Activity,即执行启动操作的Activity
        sourceRecord = mRootWindowContainer.isInAnyStack(resultTo); // 通过token解析ActivityRecord
        if (DEBUG_RESULTS) {
            Slog.v(TAG_RESULTS, "Will send result to " + resultTo + " " + sourceRecord);
        }
        if (sourceRecord != null) {
            if (requestCode >= 0 && !sourceRecord.finishing) {
                resultRecord = sourceRecord;  // 记录返回result到那个Activity
            }
        }
    }

    final int launchFlags = intent.getFlags();
    if ((launchFlags & Intent.FLAG_ACTIVITY_FORWARD_RESULT) != 0 && sourceRecord != null) { // 处理 FLAG_ACTIVITY_FORWARD_RESULT
        // Transfer the result target from the source activity to the new one being started,
        // including any failures.
        if (requestCode >= 0) { // foward flag 不能设置requestCode>=0
            SafeActivityOptions.abort(options);
            return ActivityManager.START_FORWARD_AND_REQUEST_CONFLICT;
        }
        resultRecord = sourceRecord.resultTo; // 将result传递给原Activity的resultTo对应的Activity(即启动原Activity的Activity)
        if (resultRecord != null && !resultRecord.isInStackLocked()) {// 不在stack中则清除目标
            resultRecord = null;
        }
        resultWho = sourceRecord.resultWho;
        requestCode = sourceRecord.requestCode;
        sourceRecord.resultTo = null; // 清除原Activity的resultTo,即清除其返回result的目标Activity信息, 因为在上面已经赋值给了resultRecord
        if (resultRecord != null) { // 移除resultRecord的results中来自sourceRecord的匹配resultWho和requestCode的ResultInfo
            resultRecord.removeResultsLocked(sourceRecord, resultWho, requestCode);
        }
        if (sourceRecord.launchedFromUid == callingUid) { // 启动原Activity的uid与启动新Activity的uid一致
            // The new activity is being launched from the same uid as the previous activity
            // in the flow, and asking to forward its result back to the previous.  In this
            // case the activity is serving as a trampoline between the two, so we also want
            // to update its launchedFromPackage to be the same as the previous activity.
            // Note that this is safe, since we know these two packages come from the same
            // uid; the caller could just as well have supplied that same package name itself
            // . This specifially deals with the case of an intent picker/chooser being
            // launched in the app flow to redirect to an activity picked by the user, where
            // we want the final activity to consider it to have been launched by the
            // previous app activity.
            callingPackage = sourceRecord.launchedFromPackage;   // 保证package一致
            callingFeatureId = sourceRecord.launchedFromFeatureId;
        }
    }

    if (err == ActivityManager.START_SUCCESS && intent.getComponent() == null) {// 无法解析Intent
        // We couldn't find a class that can handle the given Intent.
        // That's the end of that!
        err = ActivityManager.START_INTENT_NOT_RESOLVED;
    }

    if (err == ActivityManager.START_SUCCESS && aInfo == null) { // 类不存在
        // We couldn't find the specific class specified in the Intent.
        // Also the end of the line.
        err = ActivityManager.START_CLASS_NOT_FOUND;
    }

    if (err == ActivityManager.START_SUCCESS && sourceRecord != null
            && sourceRecord.getTask().voiceSession != null) { // 启动成voice session的一部分
        // If this activity is being launched as part of a voice session, we need to ensure
        // that it is safe to do so.  If the upcoming activity will also be part of the voice
        // session, we can only launch it if it has explicitly said it supports the VOICE
        // category, or it is a part of the calling app.
        if ((launchFlags & FLAG_ACTIVITY_NEW_TASK) == 0 // 没有设置 FLAG_ACTIVITY_NEW_TASK , 且 uid不是voice session的
                && sourceRecord.info.applicationInfo.uid != aInfo.applicationInfo.uid) {
            try {
                intent.addCategory(Intent.CATEGORY_VOICE);  // 需要检查是否支持CATEGORY_VOICE
                if (!mService.getPackageManager().activitySupportsIntent(
                        intent.getComponent(), intent, resolvedType)) {
                    Slog.w(TAG, "Activity being started in current voice task does not support "
                            + "voice: " + intent);
                    err = ActivityManager.START_NOT_VOICE_COMPATIBLE;
                }
            } catch (RemoteException e) {
                Slog.w(TAG, "Failure checking voice capabilities", e);
                err = ActivityManager.START_NOT_VOICE_COMPATIBLE;
            }
        }
    }

    if (err == ActivityManager.START_SUCCESS && voiceSession != null) { // startVoiceActivity 才会设置 voiceSession
        // If the caller is starting a new voice session, just make sure the target
        // is actually allowing it to run this way.
        try {
            if (!mService.getPackageManager().activitySupportsIntent(intent.getComponent(),
                    intent, resolvedType)) {
                Slog.w(TAG,
                        "Activity being started in new voice task does not support: " + intent);
                err = ActivityManager.START_NOT_VOICE_COMPATIBLE;
            }
        } catch (RemoteException e) {
            Slog.w(TAG, "Failure checking voice capabilities", e);
            err = ActivityManager.START_NOT_VOICE_COMPATIBLE;
        }
    }

    final ActivityStack resultStack = resultRecord == null
            ? null : resultRecord.getRootTask();  // 获取resultRecord 的根ActivityStack

    if (err != START_SUCCESS) {
        if (resultRecord != null) {
            resultRecord.sendResult(INVALID_UID, resultWho, requestCode, RESULT_CANCELED,
                    null /* data */, null /* dataGrants */);
        }
        SafeActivityOptions.abort(options);
        return err;
    }

    // 检查启动 Activity的权限
    boolean abort = !mSupervisor.checkStartAnyActivityPermission(intent, aInfo, resultWho,
            requestCode, callingPid, callingUid, callingPackage, callingFeatureId,
            request.ignoreTargetSecurity, inTask != null, callerApp, resultRecord, resultStack);
    abort |= !mService.mIntentFirewall.checkStartActivity(intent, callingUid,
            callingPid, resolvedType, aInfo.applicationInfo);
    abort |= !mService.getPermissionPolicyInternal().checkStartActivity(intent, callingUid,
            callingPackage);

    boolean restrictedBgActivity = false;
    if (!abort) {
        try {
            Trace.traceBegin(Trace.TRACE_TAG_WINDOW_MANAGER,
                    "shouldAbortBackgroundActivityStart");
            // 检查是否需要终止后台启动Activity
            restrictedBgActivity = shouldAbortBackgroundActivityStart(callingUid,
                    callingPid, callingPackage, realCallingUid, realCallingPid, callerApp,
                    request.originatingPendingIntent, request.allowBackgroundActivityStart,
                    intent);
        } finally {
            Trace.traceEnd(Trace.TRACE_TAG_WINDOW_MANAGER);
        }
    }

    // Merge the two options bundles, while realCallerOptions takes precedence.
    ActivityOptions checkedOptions = options != null
            ? options.getOptions(intent, aInfo, callerApp, mSupervisor) : null;
    if (request.allowPendingRemoteAnimationRegistryLookup) {
        checkedOptions = mService.getActivityStartController()
                .getPendingRemoteAnimationRegistry()
                .overrideOptionsIfNeeded(callingPackage, checkedOptions);
    }
    if (mService.mController != null) { // ActivityController是否要拦截启动
        try {
            // The Intent we give to the watcher has the extra data stripped off, since it
            // can contain private information.
            Intent watchIntent = intent.cloneFilter();
            abort |= !mService.mController.activityStarting(watchIntent,
                    aInfo.applicationInfo.packageName);
        } catch (RemoteException e) {
            mService.mController = null;
        }
    }

    mInterceptor.setStates(userId, realCallingPid, realCallingUid, startFlags, callingPackage,
            callingFeatureId);
    if (mInterceptor.intercept(intent, rInfo, aInfo, resolvedType, inTask, callingPid,
            callingUid, checkedOptions)) { // ActivityStartInterceptor 是否拦截启动
        // activity start was intercepted, e.g. because the target user is currently in quiet
        // mode (turn off work) or the target application is suspended
        intent = mInterceptor.mIntent;
        rInfo = mInterceptor.mRInfo;
        aInfo = mInterceptor.mAInfo;
        resolvedType = mInterceptor.mResolvedType;
        inTask = mInterceptor.mInTask;
        callingPid = mInterceptor.mCallingPid;
        callingUid = mInterceptor.mCallingUid;
        checkedOptions = mInterceptor.mActivityOptions;

        // The interception target shouldn't get any permission grants
        // intended for the original destination
        intentGrants = null;
    }

    if (abort) {
        if (resultRecord != null) {
            resultRecord.sendResult(INVALID_UID, resultWho, requestCode, RESULT_CANCELED,
                    null /* data */, null /* dataGrants */);
        }
        // We pretend to the caller that it was really started, but they will just get a
        // cancel result.
        ActivityOptions.abort(checkedOptions);
        return START_ABORTED;
    }

    // If permissions need a review before any of the app components can run, we
    // launch the review activity and pass a pending intent to start the activity
    // we are to launching now after the review is completed.
    if (aInfo != null) {  // 检查是否需要review permissions , Permission review applies only to apps not supporting the new permission model
        if (mService.getPackageManagerInternalLocked().isPermissionsReviewRequired( // >=M 的不需要
                aInfo.packageName, userId)) {
            final IIntentSender target = mService.getIntentSenderLocked(
                    ActivityManager.INTENT_SENDER_ACTIVITY, callingPackage, callingFeatureId,
                    callingUid, userId, null, null, 0, new Intent[]{intent},
                    new String[]{resolvedType}, PendingIntent.FLAG_CANCEL_CURRENT
                            | PendingIntent.FLAG_ONE_SHOT, null);

            Intent newIntent = new Intent(Intent.ACTION_REVIEW_PERMISSIONS); // 创建新Intent

            int flags = intent.getFlags();
            flags |= Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS;

            /*
             * Prevent reuse of review activity: Each app needs their own review activity. By
             * default activities launched with NEW_TASK or NEW_DOCUMENT try to reuse activities
             * with the same launch parameters (extras are ignored). Hence to avoid possible
             * reuse force a new activity via the MULTIPLE_TASK flag.
             *
             * Activities that are not launched with NEW_TASK or NEW_DOCUMENT are not re-used,
             * hence no need to add the flag in this case.
             */
            if ((flags & (FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_NEW_DOCUMENT)) != 0) {
                flags |= Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
            }
            newIntent.setFlags(flags);

            newIntent.putExtra(Intent.EXTRA_PACKAGE_NAME, aInfo.packageName);
            newIntent.putExtra(Intent.EXTRA_INTENT, new IntentSender(target));
            if (resultRecord != null) {
                newIntent.putExtra(Intent.EXTRA_RESULT_NEEDED, true);
            }
            intent = newIntent;

            // The permissions review target shouldn't get any permission
            // grants intended for the original destination
            intentGrants = null;

            resolvedType = null;
            callingUid = realCallingUid;
            callingPid = realCallingPid;

            rInfo = mSupervisor.resolveIntent(intent, resolvedType, userId, 0,
                    computeResolveFilterUid(
                            callingUid, realCallingUid, request.filterCallingUid));
            aInfo = mSupervisor.resolveActivity(intent, rInfo, startFlags,
                    null /*profilerInfo*/);

            if (DEBUG_PERMISSIONS_REVIEW) {
                final ActivityStack focusedStack =
                        mRootWindowContainer.getTopDisplayFocusedStack();
                Slog.i(TAG, "START u" + userId + " {" + intent.toShortString(true, true,
                        true, false) + "} from uid " + callingUid + " on display "
                        + (focusedStack == null ? DEFAULT_DISPLAY
                                : focusedStack.getDisplayId()));
            }
        }
    }

    // If we have an ephemeral app, abort the process of launching the resolved intent.
    // Instead, launch the ephemeral installer. Once the installer is finished, it
    // starts either the intent we resolved here [on install error] or the ephemeral
    // app [on install success].
    if (rInfo != null && rInfo.auxiliaryInfo != null) { // 针对instant apps or components 等的特殊情况的处理
        intent = createLaunchIntent(rInfo.auxiliaryInfo, request.ephemeralIntent,
                callingPackage, callingFeatureId, verificationBundle, resolvedType, userId);
        resolvedType = null;
        callingUid = realCallingUid;
        callingPid = realCallingPid;

        // The ephemeral installer shouldn't get any permission grants
        // intended for the original destination
        intentGrants = null;

        aInfo = mSupervisor.resolveActivity(intent, rInfo, startFlags, null /*profilerInfo*/);
    }
    // 创建 ActivityRecord
    final ActivityRecord r = new ActivityRecord(mService, callerApp, callingPid, callingUid,
            callingPackage, callingFeatureId, intent, resolvedType, aInfo,
            mService.getGlobalConfiguration(), resultRecord, resultWho, requestCode,
            request.componentSpecified, voiceSession != null, mSupervisor, checkedOptions,
            sourceRecord);
    mLastStartActivityRecord = r;  // 记录此Record

    if (r.appTimeTracker == null && sourceRecord != null) {
        // If the caller didn't specify an explicit time tracker, we want to continue
        // tracking under any it has.
        r.appTimeTracker = sourceRecord.appTimeTracker;
    }

    final ActivityStack stack = mRootWindowContainer.getTopDisplayFocusedStack();

    // If we are starting an activity that is not from the same uid as the currently resumed
    // one, check whether app switches are allowed.
    if (voiceSession == null && stack != null && (stack.getResumedActivity() == null
            || stack.getResumedActivity().info.applicationInfo.uid != realCallingUid)) {// 非同一应用
        if (!mService.checkAppSwitchAllowedLocked(callingPid, callingUid,
                realCallingPid, realCallingUid, "Activity start")) {   // 检查是否允许切换应用
            if (!(restrictedBgActivity && handleBackgroundActivityAbort(r))) {
                mController.addPendingActivityLaunch(new PendingActivityLaunch(r,
                        sourceRecord, startFlags, stack, callerApp, intentGrants)); // 如果没有abort启动,则添加到pending启动列表
            }
            ActivityOptions.abort(checkedOptions);
            return ActivityManager.START_SWITCHES_CANCELED;
        }
    }

    mService.onStartActivitySetDidAppSwitch();
    mController.doPendingActivityLaunches(false); // 启动pending的Activity, doResume为false
    // 继续启动流程
    mLastStartActivityResult = startActivityUnchecked(r, sourceRecord, voiceSession,
            request.voiceInteractor, startFlags, true /* doResume */, checkedOptions, inTask,
            restrictedBgActivity, intentGrants);

    if (request.outActivity != null) {
        request.outActivity[0] = mLastStartActivityRecord;
    }

    return mLastStartActivityResult;
}
```

# ActivityStarter#startActivityUnchecked
```
/**
 * Start an activity while most of preliminary checks has been done and caller has been
 * confirmed that holds necessary permissions to do so.
 * Here also ensures that the starting activity is removed if the start wasn't successful.
 */
private int startActivityUnchecked(final ActivityRecord r, ActivityRecord sourceRecord,
            IVoiceInteractionSession voiceSession, IVoiceInteractor voiceInteractor,
            int startFlags, boolean doResume, ActivityOptions options, Task inTask,
            boolean restrictedBgActivity, NeededUriGrants intentGrants) {
    int result = START_CANCELED;
    final ActivityStack startedActivityStack;
    try {
        mService.deferWindowLayout();// 通知wms推迟布局
        Trace.traceBegin(Trace.TRACE_TAG_WINDOW_MANAGER, "startActivityInner");
        result = startActivityInner(r, sourceRecord, voiceSession, voiceInteractor,
                startFlags, doResume, options, inTask, restrictedBgActivity, intentGrants); // 调用startActivityInner
    } finally {
        Trace.traceEnd(Trace.TRACE_TAG_WINDOW_MANAGER);
        startedActivityStack = handleStartResult(r, result); // 处理启动结果
        mService.continueWindowLayout(); // 通知wms继续布局
    }

    postStartActivityProcessing(r, result, startedActivityStack);

    return result;
}
```

## ActivityStarter#startActivityInner
处理Activity Task 的管理
```
/**
 * Start an activity and determine if the activity should be adding to the top of an existing
 * task or delivered new intent to an existing activity. Also manipulating the activity task
 * onto requested or valid stack/display.
 *
 * Note: This method should only be called from {@link #startActivityUnchecked}.
 */
// TODO(b/152429287): Make it easier to exercise code paths through startActivityInner
@VisibleForTesting
int startActivityInner(final ActivityRecord r, ActivityRecord sourceRecord,
        IVoiceInteractionSession voiceSession, IVoiceInteractor voiceInteractor,
        int startFlags, boolean doResume, ActivityOptions options, Task inTask,
        boolean restrictedBgActivity, NeededUriGrants intentGrants) {
    setInitialState(r, options, inTask, doResume, startFlags, sourceRecord, voiceSession,
            voiceInteractor, restrictedBgActivity); // 设置一些状态

    computeLaunchingTaskFlags(); // 计算启动Task的flag

    computeSourceStack(); // 计算原Activity对应的 ActivityStack

    mIntent.setFlags(mLaunchFlags);

    final Task reusedTask = getReusableTask(); // 获取可重用的Task

    // If requested, freeze the task list
    if (mOptions != null && mOptions.freezeRecentTasksReordering()
            && mSupervisor.mRecentTasks.isCallerRecents(r.launchedFromUid)
            && !mSupervisor.mRecentTasks.isFreezeTaskListReorderingSet()) { // 判断是否要冻结task list order
        mFrozeTaskList = true;
        mSupervisor.mRecentTasks.setFreezeTaskListReordering();
    }

    // Compute if there is an existing task that should be used for.
    final Task targetTask = reusedTask != null ? reusedTask : computeTargetTask(); // 计算目标的Task
    final boolean newTask = targetTask == null;  // 如果目标Task为null则需要创建新Task
    mTargetTask = targetTask;

    computeLaunchParams(r, sourceRecord, targetTask); // 计算启动参数

    // Check if starting activity on given task or on a new task is allowed.
    int startResult = isAllowedToStart(r, newTask, targetTask); // 判断是否能启动此Activity
    if (startResult != START_SUCCESS) {
        return startResult;
    }

    final ActivityRecord targetTaskTop = newTask
            ? null : targetTask.getTopNonFinishingActivity();
    if (targetTaskTop != null) {
        // Recycle the target task for this launch.
        startResult = recycleTask(targetTask, targetTaskTop, reusedTask, intentGrants);//Prepare the target task to be reused for this launch
        if (startResult != START_SUCCESS) {
            return startResult;
        }
    } else {
        mAddingToTask = true;
    }

    // If the activity being launched is the same as the one currently at the top, then
    // we need to check if it should only be launched once.
    final ActivityStack topStack = mRootWindowContainer.getTopDisplayFocusedStack();
    if (topStack != null) { // 判断要启动的Activity是否与栈顶的一致,一致则不需要启动,会回调其 onNewIntent
        startResult = deliverToCurrentTopIfNeeded(topStack, intentGrants);
        if (startResult != START_SUCCESS) {
            return startResult;
        }
    }

    if (mTargetStack == null) { // 获取目标ActivityStack
        mTargetStack = getLaunchStack(mStartActivity, mLaunchFlags, targetTask, mOptions);
    }
    if (newTask) { // 创建新Task
        final Task taskToAffiliate = (mLaunchTaskBehind && mSourceRecord != null)
                ? mSourceRecord.getTask() : null;
        String packageName= mService.mContext.getPackageName();
        if (mPerf != null) {
            mStartActivity.perfActivityBoostHandler =
                mPerf.perfHint(BoostFramework.VENDOR_HINT_FIRST_LAUNCH_BOOST,
                                    packageName, -1, BoostFramework.Launch.BOOST_V1);
        }
        setNewTask(taskToAffiliate); // 创建或重用Task
        if (mService.getLockTaskController().isLockTaskModeViolation(
                mStartActivity.getTask())) {
            Slog.e(TAG, "Attempted Lock Task Mode violation mStartActivity=" + mStartActivity);
            return START_RETURN_LOCK_TASK_MODE_VIOLATION;
        }
    } else if (mAddingToTask) { // 添加到Task或reparent Task
        addOrReparentStartingActivity(targetTask, "adding to task");
    }

    if (!mAvoidMoveToFront && mDoResume) { // 如果要resume, 将targetTask移动到目标ActivityStack最前面
        mTargetStack.getStack().moveToFront("reuseOrNewTask", targetTask);
        if (mOptions != null) {
            if (mOptions.getTaskAlwaysOnTop()) {
                mTargetStack.setAlwaysOnTop(true);
            }
        }
        if (!mTargetStack.isTopStackInDisplayArea() && mService.mInternal.isDreaming()) { // 如果此时处于 dream 状态,则设置launch-behind
            // Launching underneath dream activity (fullscreen, always-on-top). Run the launch-
            // -behind transition so the Activity gets created and starts in visible state.
            mLaunchTaskBehind = true;
            r.mLaunchTaskBehind = true;
        }
    }

    mService.mUgmInternal.grantUriPermissionUncheckedFromIntent(intentGrants,
            mStartActivity.getUriPermissionsLocked());
    if (mStartActivity.resultTo != null && mStartActivity.resultTo.info != null) {
        // we need to resolve resultTo to a uid as grantImplicitAccess deals explicitly in UIDs
        final PackageManagerInternal pmInternal =
                mService.getPackageManagerInternalLocked();
        final int resultToUid = pmInternal.getPackageUidInternal(
                        mStartActivity.resultTo.info.packageName, 0, mStartActivity.mUserId);
        pmInternal.grantImplicitAccess(mStartActivity.mUserId, mIntent,
                UserHandle.getAppId(mStartActivity.info.applicationInfo.uid) /*recipient*/,
                resultToUid /*visible*/, true /*direct*/); // implicit access granted: " + recipientUid + " -> " + visibleUid
    }
    if (newTask) { // 此处打印 event log：wm_create_task
        EventLogTags.writeWmCreateTask(mStartActivity.mUserId,
                mStartActivity.getTask().mTaskId);
    }
    mStartActivity.logStartActivity(
            EventLogTags.WM_CREATE_ACTIVITY, mStartActivity.getTask());

    mTargetStack.mLastPausedActivity = null;

    mRootWindowContainer.sendPowerHintForLaunchStartIfNeeded(
            false /* forceSend */, mStartActivity);
     // 调用目标ActivityStack的startActivityLocked 添加r到栈顶,启动动画,startingWindow等处理        
    mTargetStack.startActivityLocked(mStartActivity, topStack.getTopNonFinishingActivity(),
            newTask, mKeepCurTransition, mOptions);
    if (mDoResume) { // 需要resume
        final ActivityRecord topTaskActivity =
                mStartActivity.getTask().topRunningActivityLocked();
        if (!mTargetStack.isTopActivityFocusable()    // top Activity不是focusable
                || (topTaskActivity != null && topTaskActivity.isTaskOverlay() // Overlay情况,如PIP
                && mStartActivity != topTaskActivity)) {
            // If the activity is not focusable, we can't resume it, but still would like to
            // make sure it becomes visible as it starts (this will also trigger entry
            // animation). An example of this are PIP activities.
            // Also, we don't want to resume activities in a task that currently has an overlay
            // as the starting activity just needs to be in the visible paused state until the
            // over is removed.
            // Passing {@code null} as the start parameter ensures all activities are made
            // visible.
            mTargetStack.ensureActivitiesVisible(null /* starting */,
                    0 /* configChanges */, !PRESERVE_WINDOWS);  // 确保可见性
            // Go ahead and tell window manager to execute app transition for this activity
            // since the app transition will not be triggered through the resume channel.
            mTargetStack.getDisplay().mDisplayContent.executeAppTransition();
        } else {
            // If the target stack was not previously focusable (previous top running activity
            // on that stack was not visible) then any prior calls to move the stack to the
            // will not update the focused stack.  If starting the new activity now allows the
            // task stack to be focusable, then ensure that we now update the focused stack
            // accordingly.
            if (mTargetStack.isTopActivityFocusable()
                    && !mRootWindowContainer.isTopDisplayFocusedStack(mTargetStack)) {// 不是最上面的focused ActivityStack
                mTargetStack.moveToFront("startActivityInner"); 将此stack推到最上面
            }
            mRootWindowContainer.resumeFocusedStacksTopActivities(
                    mTargetStack, mStartActivity, mOptions); // 执行resume Activity
        }
    }
    mRootWindowContainer.updateUserStack(mStartActivity.mUserId, mTargetStack);

    // Update the recent tasks list immediately when the activity starts
    mSupervisor.mRecentTasks.add(mStartActivity.getTask());
    mSupervisor.handleNonResizableTaskIfNeeded(mStartActivity.getTask(),
            mPreferredWindowingMode, mPreferredTaskDisplayArea, mTargetStack);

    return START_SUCCESS;
}
```

### ActivityStarter#setInitialState
初始化ActivityStarter中的相关变量
```
private void setInitialState(ActivityRecord r, ActivityOptions options, Task inTask,
        boolean doResume, int startFlags, ActivityRecord sourceRecord,
        IVoiceInteractionSession voiceSession, IVoiceInteractor voiceInteractor,
        boolean restrictedBgActivity) {
    reset(false /* clearRequest */);

    mStartActivity = r;
    mIntent = r.intent;
    mOptions = options;
    mCallingUid = r.launchedFromUid;
    mSourceRecord = sourceRecord;
    mVoiceSession = voiceSession;
    mVoiceInteractor = voiceInteractor;
    mRestrictedBgActivity = restrictedBgActivity;

    mLaunchParams.reset();

    // Preferred display id is the only state we need for now and it could be updated again
    // after we located a reusable task (which might be resided in another display).
    mSupervisor.getLaunchParamsController().calculate(inTask, r.info.windowLayout, r,
            sourceRecord, options, PHASE_DISPLAY, mLaunchParams);
    mPreferredTaskDisplayArea = mLaunchParams.hasPreferredTaskDisplayArea()
            ? mLaunchParams.mPreferredTaskDisplayArea
            : mRootWindowContainer.getDefaultTaskDisplayArea();
    mPreferredWindowingMode = mLaunchParams.mWindowingMode;

    mLaunchMode = r.launchMode; // 启动模式

    mLaunchFlags = adjustLaunchFlagsToDocumentMode(
            r, LAUNCH_SINGLE_INSTANCE == mLaunchMode,
            LAUNCH_SINGLE_TASK == mLaunchMode, mIntent.getFlags());
    mLaunchTaskBehind = r.mLaunchTaskBehind
            && !isLaunchModeOneOf(LAUNCH_SINGLE_TASK, LAUNCH_SINGLE_INSTANCE)
            && (mLaunchFlags & FLAG_ACTIVITY_NEW_DOCUMENT) != 0;

    sendNewTaskResultRequestIfNeeded();

    if ((mLaunchFlags & FLAG_ACTIVITY_NEW_DOCUMENT) != 0 && r.resultTo == null) {
        mLaunchFlags |= FLAG_ACTIVITY_NEW_TASK;
    }

    // If we are actually going to launch in to a new task, there are some cases where
    // we further want to do multiple task.
    if ((mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) != 0) {  // 处理multiple Task情况
        if (mLaunchTaskBehind
                || r.info.documentLaunchMode == DOCUMENT_LAUNCH_ALWAYS) {
            mLaunchFlags |= FLAG_ACTIVITY_MULTIPLE_TASK;
        }
    }

    // We'll invoke onUserLeaving before onPause only if the launching
    // activity did not explicitly state that this is an automated launch.
    // 没有FLAG_ACTIVITY_NO_USER_ACTION,说明时user操作,后续会调用onUserLeaving
    mSupervisor.mUserLeaving = (mLaunchFlags & FLAG_ACTIVITY_NO_USER_ACTION) == 0;
    if (DEBUG_USER_LEAVING) Slog.v(TAG_USER_LEAVING,
            "startActivity() => mUserLeaving=" + mSupervisor.mUserLeaving);

    // If the caller has asked not to resume at this point, we make note
    // of this in the record so that we can skip it when trying to find
    // the top running activity.
    mDoResume = doResume;
    if (!doResume || !r.okToShowLocked() || mLaunchTaskBehind) { // 处理是否resume
        r.delayedResume = true;
        mDoResume = false;
    }

    if (mOptions != null) { // 处理options
        if (mOptions.getLaunchTaskId() != INVALID_TASK_ID && mOptions.getTaskOverlay()) { // 处理overlay
            r.setTaskOverlay(true);
            if (!mOptions.canTaskOverlayResume()) {
                final Task task = mRootWindowContainer.anyTaskForId(
                        mOptions.getLaunchTaskId());
                final ActivityRecord top = task != null
                        ? task.getTopNonFinishingActivity() : null;
                if (top != null && !top.isState(RESUMED)) {
                    // The caller specifies that we'd like to be avoided to be moved to the
                    // front, so be it!
                    mDoResume = false;
                    mAvoidMoveToFront = true;
                }
            }
        } else if (mOptions.getAvoidMoveToFront()) { // 处理mAvoidMoveToFront
            mDoResume = false;
            mAvoidMoveToFront = true;
        }
    }

    mNotTop = (mLaunchFlags & FLAG_ACTIVITY_PREVIOUS_IS_TOP) != 0 ? sourceRecord : null;

    mInTask = inTask; // 设置Task
    // In some flows in to this function, we retrieve the task record and hold on to it
    // without a lock before calling back in to here...  so the task at this point may
    // not actually be in recents.  Check for that, and if it isn't in recents just
    // consider it invalid.
    if (inTask != null && !inTask.inRecents) {
        Slog.w(TAG, "Starting activity in task not in recents: " + inTask);
        mInTask = null;
    }

    mStartFlags = startFlags;
    ...// 处理START_FLAG_ONLY_IF_NEEDED

    mNoAnimation = (mLaunchFlags & FLAG_ACTIVITY_NO_ANIMATION) != 0; // 是否无动画

    if (mRestrictedBgActivity && !mService.isBackgroundActivityStartsEnabled()) { // 禁止后台启动activity处理
        mAvoidMoveToFront = true;
        mDoResume = false;
    }
}

```

### ActivityStarter#computeLaunchingTaskFlags
计算启动Task的flag
```
private void computeLaunchingTaskFlags() {
    // If the caller is not coming from another activity, but has given us an explicit task into
    // which they would like us to launch the new activity, then let's see about doing that.
    // 从非activity上下文启动activity，但是指定了要启动的task
    if (mSourceRecord == null && mInTask != null && mInTask.getStack() != null) { // 指定了Task的情况
        final Intent baseIntent = mInTask.getBaseIntent();
        final ActivityRecord root = mInTask.getRootActivity();
        if (baseIntent == null) {
            ActivityOptions.abort(mOptions);
            throw new IllegalArgumentException("Launching into task without base intent: "
                    + mInTask);
        }

        // If this task is empty, then we are adding the first activity -- it
        // determines the root, and must be launching as a NEW_TASK.
        if (isLaunchModeOneOf(LAUNCH_SINGLE_INSTANCE, LAUNCH_SINGLE_TASK)) { // 处理目标activity是 single Task 模式或 single instance 模式
            if (!baseIntent.getComponent().equals(mStartActivity.intent.getComponent())) { // 非同一Task
                ActivityOptions.abort(mOptions);
                throw new IllegalArgumentException("Trying to launch singleInstance/Task "
                        + mStartActivity + " into different task " + mInTask);
            }
            if (root != null) { // 需要task是空
                ActivityOptions.abort(mOptions);
                throw new IllegalArgumentException("Caller with mInTask " + mInTask
                        + " has root " + root + " but target is singleInstance/Task");
            }
        }

        // If task is empty, then adopt the interesting intent launch flags in to the
        // activity being started.
        if (root == null) { // 处理Task是空的情况
            final int flagsOfInterest = FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK
                    | FLAG_ACTIVITY_NEW_DOCUMENT | FLAG_ACTIVITY_RETAIN_IN_RECENTS;
            mLaunchFlags = (mLaunchFlags & ~flagsOfInterest)
                    | (baseIntent.getFlags() & flagsOfInterest);
            mIntent.setFlags(mLaunchFlags);
            mInTask.setIntent(mStartActivity);
            mAddingToTask = true; // 设置添加到Task flag为true

            // If the task is not empty and the caller is asking to start it as the root of
            // a new task, then we don't actually want to start this on the task. We will
            // bring the task to the front, and possibly give it a new intent.
        } else if ((mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) != 0) {// 非空且设置new Task flag
            mAddingToTask = false;  // 置false
        } else {
            mAddingToTask = true;
        }

        mReuseTask = mInTask;
    } else { // 没有指定Task的情况
        mInTask = null;
        // Launch ResolverActivity in the source task, so that it stays in the task bounds
        // when in freeform workspace.
        // Also put noDisplay activities in the source task. These by itself can be placed
        // in any task/stack, however it could launch other activities like ResolverActivity,
        // and we want those to stay in the original task.
        if ((mStartActivity.isResolverOrDelegateActivity() || mStartActivity.noDisplay)
                && mSourceRecord != null && mSourceRecord.inFreeformWindowingMode()) { // freeform 模式下的情况
            mAddingToTask = true;
        }
    }

    if (mInTask == null) {
        if (mSourceRecord == null) { // 原Activity为null,通常是非Activity上下文启动
            // This activity is not being started from another...  in this
            // case we -always- start a new task.
            if ((mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) == 0 && mInTask == null) { // 自动添加FLAG_ACTIVITY_NEW_TASK
                Slog.w(TAG, "startActivity called from non-Activity context; forcing " +
                        "Intent.FLAG_ACTIVITY_NEW_TASK for: " + mIntent);
                mLaunchFlags |= FLAG_ACTIVITY_NEW_TASK; // 添加 new task flag
            }
        } else if (mSourceRecord.launchMode == LAUNCH_SINGLE_INSTANCE) { // 启动新activity的activity处于 single instance 模式
            // The original activity who is starting us is running as a single
            // instance...  this new activity it is starting must go on its
            // own task.
            mLaunchFlags |= FLAG_ACTIVITY_NEW_TASK; // 添加 new task flag
        } else if (isLaunchModeOneOf(LAUNCH_SINGLE_INSTANCE, LAUNCH_SINGLE_TASK)) { // 要启动的的activity是 single instance/Task 模式
            // The activity being started is a single instance...  it always
            // gets launched into its own task.
            mLaunchFlags |= FLAG_ACTIVITY_NEW_TASK; // 添加 new task flag
        }
    }
}
```

### ActivityStarter#computeSourceStack
计算原ActivityStack
```
private void computeSourceStack() {
    if (mSourceRecord == null) { // 原Activity为null,则为null
        mSourceStack = null;
        return;
    }
    if (!mSourceRecord.finishing) { // 原Activity处于finishing, 则返回其所在的Stack
        mSourceStack = mSourceRecord.getRootTask();
        return;
    }
    // 下面处理原Activity处于finishing
    // If the source is finishing, we can't further count it as our source. This is because the
    // task it is associated with may now be empty and on its way out, so we don't want to
    // blindly throw it in to that task.  Instead we will take the NEW_TASK flow and try to find
    // a task for it. But save the task information so it can be used when creating the new task.
    if ((mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) == 0) { // 处理没有设置FLAG_ACTIVITY_NEW_TASK
        Slog.w(TAG, "startActivity called from finishing " + mSourceRecord
                + "; forcing " + "Intent.FLAG_ACTIVITY_NEW_TASK for: " + mIntent);
        mLaunchFlags |= FLAG_ACTIVITY_NEW_TASK;
        mNewTaskInfo = mSourceRecord.info;

        // It is not guaranteed that the source record will have a task associated with it. For,
        // example, if this method is being called for processing a pending activity launch, it
        // is possible that the activity has been removed from the task after the launch was
        // enqueued.
        final Task sourceTask = mSourceRecord.getTask();
        mNewTaskIntent = sourceTask != null ? sourceTask.intent : null;
    }
    mSourceRecord = null; // 将相关信息置为null
    mSourceStack = null;
}
```

### ActivityStarter#getReusableTask
尝试获取一个可重用的Task
```
/**
 * Decide whether the new activity should be inserted into an existing task. Returns null
 * if not or an ActivityRecord with the task into which the new activity should be added.
 */
private Task getReusableTask() {
    // If a target task is specified, try to reuse that one
    if (mOptions != null && mOptions.getLaunchTaskId() != INVALID_TASK_ID) {// 如果指定了Task ID
        Task launchTask = mRootWindowContainer.anyTaskForId(mOptions.getLaunchTaskId());
        if (launchTask != null) { // 获取指定id的Task
            return launchTask;
        }
        return null;
    }

    // We may want to try to place the new activity in to an existing task.  We always
    // do this if the target activity is singleTask or singleInstance; we will also do
    // this if NEW_TASK has been requested, and there is not an additional qualifier telling
    // us to still place it in a new task: multi task, always doc mode, or being asked to
    // launch this as a new task behind the current one.
    添加到已经存在的条件: 指定FLAG_ACTIVITY_NEW_TASK但没有指定FLAG_ACTIVITY_MULTIPLE_TASK 或
                      是single Task或single instance 模式
    boolean putIntoExistingTask = ((mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) != 0 &&
            (mLaunchFlags & FLAG_ACTIVITY_MULTIPLE_TASK) == 0)
            || isLaunchModeOneOf(LAUNCH_SINGLE_INSTANCE, LAUNCH_SINGLE_TASK);
    // If bring to front is requested, and no result is requested and we have not been given
    // an explicit task to launch in to, and we can find a task that was started with this
    // same component, then instead of launching bring that one to the front.
    putIntoExistingTask &= mInTask == null && mStartActivity.resultTo == null;// 没有指定Task且不需要返回值
    ActivityRecord intentActivity = null;
    if (putIntoExistingTask) {
        if (LAUNCH_SINGLE_INSTANCE == mLaunchMode) {// 处理 single instance 模式
            // There can be one and only one instance of single instance activity in the
            // history, and it is always in its own unique task, so we do a special search.
            intentActivity = mRootWindowContainer.findActivity(mIntent, mStartActivity.info,
                   mStartActivity.isActivityTypeHome()); // 寻找满足的ActivityRecord
        } else if ((mLaunchFlags & FLAG_ACTIVITY_LAUNCH_ADJACENT) != 0) {// 用于split-screen multi-window mode
            // For the launch adjacent case we only want to put the activity in an existing
            // task if the activity already exists in the history.
            intentActivity = mRootWindowContainer.findActivity(mIntent, mStartActivity.info,
                    !(LAUNCH_SINGLE_TASK == mLaunchMode));
        } else {
            // Otherwise find the best task to put the activity in.
            intentActivity =  // 优先从PreferredDisplayArea寻找,若寻找不到再到其他DisplayArea寻找
                    mRootWindowContainer.findTask(mStartActivity, mPreferredTaskDisplayArea);
        }
    }

    if (intentActivity != null
            && (mStartActivity.isActivityTypeHome() || intentActivity.isActivityTypeHome())
            && intentActivity.getDisplayArea() != mPreferredTaskDisplayArea) {
        // Do not reuse home activity on other display areas.
        intentActivity = null;
    }
    // 若intentActivity存在,则复用其Task
    return intentActivity != null ? intentActivity.getTask() : null;
}
```

### ActivityStarter#computeTargetTask
如果没有reusedTask,则会调用computeTargetTask来计算目标的Task
```
private Task computeTargetTask() {
    if (mStartActivity.resultTo == null // 启动此Activity的原Activity为null
      && mInTask == null && !mAddingToTask  // 没有指定Task, 且mAddingToTask为false
            && (mLaunchFlags & FLAG_ACTIVITY_NEW_TASK) != 0 /* 指定了new task的flag */) {
        // A new task should be created instead of using existing one.
        return null;
    } else if (mSourceRecord != null) { // 复用原Activity所在的Task
        return mSourceRecord.getTask();
    } else if (mInTask != null) { // 使用指定的Task
        return mInTask;
    } else {
        final ActivityStack stack = getLaunchStack(mStartActivity, mLaunchFlags,
                null /* task */, mOptions); // 获取启动的ActivityStack
        final ActivityRecord top = stack.getTopNonFinishingActivity();
        if (top != null) {
            return top.getTask();
        } else {
            // Remove the stack if no activity in the stack.
            stack.removeIfPossible();
        }
    }
    return null;
}
```
getLaunchStack
```
private ActivityStack getLaunchStack(ActivityRecord r, int launchFlags, Task task,
        ActivityOptions aOptions) {
    // We are reusing a task, keep the stack!
    if (mReuseTask != null) { // 是否有可复用的Task
        return mReuseTask.getStack(); // 获取其stack
    }

    final boolean onTop = // 可显示到top
            (aOptions == null || !aOptions.getAvoidMoveToFront()) && !mLaunchTaskBehind;
    return mRootWindowContainer.getLaunchStack(r, aOptions, task, onTop, mLaunchParams,
            mRequest.realCallingPid, mRequest.realCallingUid);
}
```

### RootWindowContainer#getLaunchStack
```
/// @frameworks/base/services/core/java/com/android/server/wm/RootWindowContainer.java
/**
 * Returns the right stack to use for launching factoring in all the input parameters.
 *
 * @param r The activity we are trying to launch. Can be null.
 * @param options The activity options used to the launch. Can be null.
 * @param candidateTask The possible task the activity might be launched in. Can be null.
 * @param launchParams The resolved launch params to use.
 * @param realCallingPid The pid from {@link ActivityStarter#setRealCallingPid}
 * @param realCallingUid The uid from {@link ActivityStarter#setRealCallingUid}
 *
 * @return The stack to use for the launch or INVALID_STACK_ID.
 */
ActivityStack getLaunchStack(@Nullable ActivityRecord r,
        @Nullable ActivityOptions options, @Nullable Task candidateTask, boolean onTop,
        @Nullable LaunchParamsController.LaunchParams launchParams, int realCallingPid,
        int realCallingUid) {
    int taskId = INVALID_TASK_ID;
    int displayId = INVALID_DISPLAY;
    TaskDisplayArea taskDisplayArea = null;

    // We give preference to the launch preference in activity options.
    if (options != null) { // 若有options,获取其携带的信息
        taskId = options.getLaunchTaskId();
        displayId = options.getLaunchDisplayId();
        final WindowContainerToken daToken = options.getLaunchTaskDisplayArea();
        taskDisplayArea = daToken != null
                ? (TaskDisplayArea) WindowContainer.fromBinder(daToken.asBinder()) : null;
    }

    // First preference for stack goes to the task Id set in the activity options. Use the stack
    // associated with that if possible.
    if (taskId != INVALID_TASK_ID) { // 此处是options里获取的taskId
        // Temporarily set the task id to invalid in case in re-entry.
        options.setLaunchTaskId(INVALID_TASK_ID);
        final Task task = anyTaskForId(taskId,
                MATCH_TASK_IN_STACKS_OR_RECENT_TASKS_AND_RESTORE, options, onTop);
        options.setLaunchTaskId(taskId);
        if (task != null) {
            return task.getStack();
        }
    }
    // 获取Activity的type, 如ACTIVITY_TYPE_STANDARD,ACTIVITY_TYPE_HOME
    final int activityType = resolveActivityType(r, options, candidateTask);
    ActivityStack stack = null;

    // Next preference for stack goes to the taskDisplayArea candidate.
    if (launchParams != null && launchParams.mPreferredTaskDisplayArea != null) {
        taskDisplayArea = launchParams.mPreferredTaskDisplayArea; // 使用参数的 DIsplayArea
    }

    if (taskDisplayArea == null && displayId != INVALID_DISPLAY) {
        final DisplayContent displayContent = getDisplayContent(displayId);
        if (displayContent != null) {  // 获取DisplayContent中的DIsplayArea
            taskDisplayArea = displayContent.getDefaultTaskDisplayArea();
        }
    }

    if (taskDisplayArea != null) {
        final int tdaDisplayId = taskDisplayArea.getDisplayId();
        final boolean canLaunchOnDisplayFromStartRequest =
                realCallingPid != 0 && realCallingUid > 0 && r != null
                        && mStackSupervisor.canPlaceEntityOnDisplay(tdaDisplayId,
                        realCallingPid, realCallingUid, r.info);
        // 判断是否可在指定的Display 上启动Activity                
        if (canLaunchOnDisplayFromStartRequest || canLaunchOnDisplay(r, tdaDisplayId)) {
            if (r != null) {
                // 从指定 DisplayArea 中查找
                final ActivityStack result = getValidLaunchStackInTaskDisplayArea(
                        taskDisplayArea, r, candidateTask, options, launchParams);
                if (result != null) {
                    return result;
                }
            }
            // Falling back to default task container // 从默认的 taskDisplayArea 找
            taskDisplayArea = taskDisplayArea.mDisplayContent.getDefaultTaskDisplayArea();
            stack = taskDisplayArea.getOrCreateStack(r, options, candidateTask, activityType,
                    onTop);
            if (stack != null) {
                return stack;
            }
        }
    }

    // Give preference to the stack and display of the input task and activity if they match the
    // mode we want to launch into.
    TaskDisplayArea container = null;
    if (candidateTask != null) {
        stack = candidateTask.getStack();
    }
    if (stack == null && r != null) {
        stack = r.getRootTask();
    }
    // 根据 windowingMode 获取或创建 ActivityStack
    int windowingMode = launchParams != null ? launchParams.mWindowingMode
            : WindowConfiguration.WINDOWING_MODE_UNDEFINED;
    if (stack != null) {
        container = stack.getDisplayArea();
        if (container != null && canLaunchOnDisplay(r, container.mDisplayContent.mDisplayId)) {
            if (windowingMode == WindowConfiguration.WINDOWING_MODE_UNDEFINED) {
                windowingMode = container.resolveWindowingMode(r, options, candidateTask,
                        activityType);
            }
            // Always allow organized tasks that created by organizer since the activity type
            // of an organized task is decided by the activity type of its top child, which
            // could be incompatible with the given windowing mode and activity type.
            if (stack.isCompatible(windowingMode, activityType) || stack.mCreatedByOrganizer) {
                return stack;
            }
            if (windowingMode == WINDOWING_MODE_FULLSCREEN_OR_SPLIT_SCREEN_SECONDARY
                    && container.getRootSplitScreenPrimaryTask() == stack
                    && candidateTask == stack.getTopMostTask()) {
                // This is a special case when we try to launch an activity that is currently on
                // top of split-screen primary stack, but is targeting split-screen secondary.
                // In this case we don't want to move it to another stack.
                // TODO(b/78788972): Remove after differentiating between preferred and required
                // launch options.
                return stack;
            }
        }
    }

    if (container == null
            || !canLaunchOnDisplay(r, container.mDisplayContent.mDisplayId)) {
        container = getDefaultTaskDisplayArea();
        if (windowingMode == WindowConfiguration.WINDOWING_MODE_UNDEFINED) {
            windowingMode = container.resolveWindowingMode(r, options, candidateTask,
                    activityType);
        }
    }
    // 根据参数获取或创建ActivityStack
    return container.getOrCreateStack(r, options, candidateTask, activityType, onTop);
}
```

### ActivityStarter#computeLaunchParams
```
private void computeLaunchParams(ActivityRecord r, ActivityRecord sourceRecord,
        Task targetTask) {
    final ActivityStack sourceStack = mSourceStack != null ? mSourceStack
            : mRootWindowContainer.getTopDisplayFocusedStack();
    // 若处于分屏模式
    if (sourceStack != null && sourceStack.inSplitScreenWindowingMode()
            && (mOptions == null
                    || mOptions.getLaunchWindowingMode() == WINDOWING_MODE_UNDEFINED)) {
        int windowingMode =
                targetTask != null ? targetTask.getWindowingMode() : WINDOWING_MODE_UNDEFINED;
        if ((mLaunchFlags & FLAG_ACTIVITY_LAUNCH_ADJACENT) != 0) { // 处理分屏
            if (sourceStack.inSplitScreenPrimaryWindowingMode()) {
                windowingMode = WINDOWING_MODE_SPLIT_SCREEN_SECONDARY;
            } else if (sourceStack.inSplitScreenSecondaryWindowingMode()) {
                windowingMode = WINDOWING_MODE_SPLIT_SCREEN_PRIMARY;
            }
        }

        if (mOptions == null) { // 创建ActivityOptions
            mOptions = ActivityOptions.makeBasic();
        }
        mOptions.setLaunchWindowingMode(windowingMode); // 设置windowingMode
    }

    mSupervisor.getLaunchParamsController().calculate(targetTask, r.info.windowLayout, r,
            sourceRecord, mOptions, PHASE_BOUNDS, mLaunchParams); // 计算LaunchParams
    mPreferredTaskDisplayArea = mLaunchParams.hasPreferredTaskDisplayArea()
            ? mLaunchParams.mPreferredTaskDisplayArea
            : mRootWindowContainer.getDefaultTaskDisplayArea();
    mPreferredWindowingMode = mLaunchParams.mWindowingMode;
}
```

### ActivityStarter#deliverToCurrentTopIfNeeded
当启动的Activity是处于最上面运行的Activity时
```
// If the activity being launched is the same as the one currently at the top, then
// we need to check if it should only be launched once.
final ActivityStack topStack = mRootWindowContainer.getTopDisplayFocusedStack();
if (topStack != null) { // 判断要启动的Activity是否与栈顶的一致,一致则不需要启动,会回调其 onNewIntent
    startResult = deliverToCurrentTopIfNeeded(topStack, intentGrants);
    if (startResult != START_SUCCESS) {
        return startResult;
    }
}
```
deliverToCurrentTopIfNeeded
```
/**
 * Check if the activity being launched is the same as the one currently at the top and it
 * should only be launched once.
 */
private int deliverToCurrentTopIfNeeded(ActivityStack topStack, NeededUriGrants intentGrants) {
    final ActivityRecord top = topStack.topRunningNonDelayedActivityLocked(mNotTop);
    final boolean dontStart = top != null && mStartActivity.resultTo == null
            && top.mActivityComponent.equals(mStartActivity.mActivityComponent) // 要启动的与最上面的组件相同
            && top.mUserId == mStartActivity.mUserId
            && top.attachedToProcess()
            && ((mLaunchFlags & FLAG_ACTIVITY_SINGLE_TOP) != 0  // 是 singleTop或singleTask 模式
            || isLaunchModeOneOf(LAUNCH_SINGLE_TOP, LAUNCH_SINGLE_TASK))
            // This allows home activity to automatically launch on secondary task display area
            // when it was added, if home was the top activity on default task display area,
            // instead of sending new intent to the home activity on default display area.
            && (!top.isActivityTypeHome() || top.getDisplayArea() == mPreferredTaskDisplayArea);
    if (!dontStart) {
        return START_SUCCESS;
    }

    // For paranoia, make sure we have correctly resumed the top activity.
    topStack.mLastPausedActivity = null;
    if (mDoResume) { // resume top Activity
        mRootWindowContainer.resumeFocusedStacksTopActivities();
    }
    ActivityOptions.abort(mOptions);
    if ((mStartFlags & START_FLAG_ONLY_IF_NEEDED) != 0) {//有flag START_FLAG_ONLY_IF_NEEDED,不必继续
        // We don't need to start a new activity, and the client said not to do anything if
        // that is the case, so this is it!
        return START_RETURN_INTENT_TO_CALLER;
    }

    deliverNewIntent(top, intentGrants); // 回调 top Activity 的 onNewIntent

    // Don't use mStartActivity.task to show the toast. We're not starting a new activity but
    // reusing 'top'. Fields in mStartActivity may not be fully initialized.
    mSupervisor.handleNonResizableTaskIfNeeded(top.getTask(),
            mLaunchParams.mWindowingMode, mPreferredTaskDisplayArea, topStack);

    return START_DELIVERED_TO_TOP;
}
```

### 创建Task
```
if (newTask) {
    final Task taskToAffiliate = (mLaunchTaskBehind && mSourceRecord != null)
            ? mSourceRecord.getTask() : null;
    String packageName= mService.mContext.getPackageName();
    if (mPerf != null) {
        mStartActivity.perfActivityBoostHandler =
            mPerf.perfHint(BoostFramework.VENDOR_HINT_FIRST_LAUNCH_BOOST,
                                packageName, -1, BoostFramework.Launch.BOOST_V1);
    }
    setNewTask(taskToAffiliate); // 设置新Task
    if (mService.getLockTaskController().isLockTaskModeViolation(
            mStartActivity.getTask())) {
        Slog.e(TAG, "Attempted Lock Task Mode violation mStartActivity=" + mStartActivity);
        return START_RETURN_LOCK_TASK_MODE_VIOLATION;
    }
} else if (mAddingToTask) { // 添加或reparent到Task
    addOrReparentStartingActivity(targetTask, "adding to task");
}
```

#### ActivityStarter#setNewTask
```
private void setNewTask(Task taskToAffiliate) {
    final boolean toTop = !mLaunchTaskBehind && !mAvoidMoveToFront; // 可显示到top
    final Task task = mTargetStack.reuseOrCreateTask( // 复用或创建新Task
            mNewTaskInfo != null ? mNewTaskInfo : mStartActivity.info,
            mNewTaskIntent != null ? mNewTaskIntent : mIntent, mVoiceSession,
            mVoiceInteractor, toTop, mStartActivity, mSourceRecord, mOptions);
    // 添加或reparent到Task        
    addOrReparentStartingActivity(task, "setTaskFromReuseOrCreateNewTask - mReuseTask");

    if (DEBUG_TASKS) {
        Slog.v(TAG_TASKS, "Starting new activity " + mStartActivity
                + " in new task " + mStartActivity.getTask());
    }

    if (taskToAffiliate != null) {
        mStartActivity.setTaskToAffiliateWith(taskToAffiliate);
    }
}
```

##### ActivityStack#reuseOrCreateTask
```
// TODO: Can be removed once we change callpoints creating stacks to be creating tasks.
/** Either returns this current task to be re-used or creates a new child task. */
Task reuseOrCreateTask(ActivityInfo info, Intent intent, IVoiceInteractionSession voiceSession,
        IVoiceInteractor voiceInteractor, boolean toTop, ActivityRecord activity,
        ActivityRecord source, ActivityOptions options) {

    Task task;
    if (DisplayContent.alwaysCreateStack(getWindowingMode(), getActivityType())) { // stack只包含单个Task, 将自身复用为Task
        // This stack will only contain one task, so just return itself since all stacks ara now
        // tasks and all tasks are now stacks.
        task = reuseAsLeafTask(voiceSession, voiceInteractor, intent, info, activity);
    } else { // 可以包含多个Task
        // Create child task since this stack can contain multiple tasks.
        final int taskId = activity != null
                ? mStackSupervisor.getNextTaskIdForUser(activity.mUserId)
                : mStackSupervisor.getNextTaskIdForUser();
        task = new ActivityStack(mAtmService, taskId, info, intent, voiceSession,
                voiceInteractor, null /* taskDescription */, this);

        // add the task to stack first, mTaskPositioner might need the stack association
        addChild(task, toTop, (info.flags & FLAG_SHOW_FOR_ALL_USERS) != 0); // 将Task添加到此Stack
    }

    int displayId = getDisplayId();
    if (displayId == INVALID_DISPLAY) displayId = DEFAULT_DISPLAY;
    final boolean isLockscreenShown = mAtmService.mStackSupervisor.getKeyguardController()
            .isKeyguardOrAodShowing(displayId); // 正显示锁屏或Aod
    if (!mStackSupervisor.getLaunchParamsController()
            .layoutTask(task, info.windowLayout, activity, source, options)
            && !matchParentBounds() && task.isResizeable() && !isLockscreenShown) {
        task.setBounds(getRequestedOverrideBounds());
    }

    return task;
}
```


#### ActivityStarter#addOrReparentStartingActivity
```
private void addOrReparentStartingActivity(Task parent, String reason) {
    String packageName= mService.mContext.getPackageName();
    if (mPerf != null) {
        mStartActivity.perfActivityBoostHandler =
            mPerf.perfHint(BoostFramework.VENDOR_HINT_FIRST_LAUNCH_BOOST,
                                packageName, -1, BoostFramework.Launch.BOOST_V1);
    }
    if (mStartActivity.getTask() == null || mStartActivity.getTask() == parent) {
        parent.addChild(mStartActivity); // 添加ActivityRecord到Task
    } else { // reparent到新Task
        mStartActivity.reparent(parent, parent.getChildCount() /* top */, reason);
    }
}
```

### 创建 ActivityRecord
此版本 ActivityRecord 是继承自 WindowToken

```
ActivityRecord(ActivityTaskManagerService _service, WindowProcessController _caller,
        int _launchedFromPid, int _launchedFromUid, String _launchedFromPackage,
        @Nullable String _launchedFromFeature, Intent _intent, String _resolvedType,
        ActivityInfo aInfo, Configuration _configuration, ActivityRecord _resultTo,
        String _resultWho, int _reqCode, boolean _componentSpecified,
        boolean _rootVoiceInteraction, ActivityStackSupervisor supervisor,
        ActivityOptions options, ActivityRecord sourceRecord) {
          // 创建Token作为WindowToken内部的token， 此处windowType是 TYPE_APPLICATION
    super(_service.mWindowManager, new Token(_intent).asBinder(), TYPE_APPLICATION, true,
            null /* displayContent */, false /* ownerCanManageAppTokens */);

    mAtmService = _service;
    appToken = (Token) token; // 上面new Token所得，实际类型是 IApplicationToken.Stub
    info = aInfo;
    mUserId = UserHandle.getUserId(info.applicationInfo.uid);
    packageName = info.applicationInfo.packageName;
    mInputApplicationHandle = new InputApplicationHandle(appToken);
    intent = _intent;

    // If the class name in the intent doesn't match that of the target, this is probably an
    // alias. We have to create a new ComponentName object to keep track of the real activity
    // name, so that FLAG_ACTIVITY_CLEAR_TOP is handled properly.
    if (info.targetActivity == null
            || (info.targetActivity.equals(intent.getComponent().getClassName())
            && (info.launchMode == LAUNCH_MULTIPLE
            || info.launchMode == LAUNCH_SINGLE_TOP))) {
        mActivityComponent = intent.getComponent();
    } else {
        mActivityComponent =
                new ComponentName(info.packageName, info.targetActivity);
    }
    // 初始化内部状态
    mTargetSdk = info.applicationInfo.targetSdkVersion;
    mShowForAllUsers = (info.flags & FLAG_SHOW_FOR_ALL_USERS) != 0;
    setOrientation(info.screenOrientation); // 设置orientation
    mRotationAnimationHint = info.rotationAnimation;

    mShowWhenLocked = (aInfo.flags & ActivityInfo.FLAG_SHOW_WHEN_LOCKED) != 0;
    mInheritShownWhenLocked = (aInfo.privateFlags & FLAG_INHERIT_SHOW_WHEN_LOCKED) != 0;
    mTurnScreenOn = (aInfo.flags & FLAG_TURN_SCREEN_ON) != 0;

    int realTheme = info.getThemeResource();
    if (realTheme == Resources.ID_NULL) {
        realTheme = aInfo.applicationInfo.targetSdkVersion < HONEYCOMB
                ? android.R.style.Theme : android.R.style.Theme_Holo;
    }

    final AttributeCache.Entry ent = AttributeCache.instance().get(packageName,
            realTheme, com.android.internal.R.styleable.Window, mUserId);

    if (ent != null) {
        mOccludesParent = !ActivityInfo.isTranslucentOrFloating(ent.array);
        hasWallpaper = ent.array.getBoolean(R.styleable.Window_windowShowWallpaper, false);
        noDisplay = ent.array.getBoolean(R.styleable.Window_windowNoDisplay, false);
    } else {
        hasWallpaper = false;
        noDisplay = false;
    }

    if (options != null) {
        mLaunchTaskBehind = options.getLaunchTaskBehind();

        final int rotationAnimation = options.getRotationAnimationHint();
        // Only override manifest supplied option if set.
        if (rotationAnimation >= 0) {
            mRotationAnimationHint = rotationAnimation;
        }
    }

    // Application tokens start out hidden.
    setVisible(false); // 设置默认为隐藏
    mVisibleRequested = false;

    ColorDisplayService.ColorDisplayServiceInternal cds = LocalServices.getService(
            ColorDisplayService.ColorDisplayServiceInternal.class);
    cds.attachColorTransformController(packageName, mUserId,
            new WeakReference<>(mColorTransformController));

    appToken.attach(this); // 保存 ActivityRecord 的弱引用，后续可以使用tokenToActivityRecordLocked获取ActivityRecord

    ...
    // 设置ActivityRecord状态为 INITIALIZING
    setState(INITIALIZING, "ActivityRecord ctor");
    callServiceTrackeronActivityStatechange(INITIALIZING, true);
    ...

    info.taskAffinity = getTaskAffinityWithUid(info.taskAffinity, info.applicationInfo.uid);
    taskAffinity = info.taskAffinity;
    ...

    if ((aInfo.flags & FLAG_EXCLUDE_FROM_RECENTS) != 0) { // 是否包含在recents
        intent.addFlags(FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
    }

    launchMode = aInfo.launchMode;
    // 设置activity的类型，比如 ACTIVITY_TYPE_HOME
    setActivityType(_componentSpecified, _launchedFromUid, _intent, options, sourceRecord);

    immersive = (aInfo.flags & FLAG_IMMERSIVE) != 0;

    requestedVrComponent = (aInfo.requestedVrComponent == null) ?
            null : ComponentName.unflattenFromString(aInfo.requestedVrComponent);

    lockTaskLaunchMode = getLockTaskLaunchMode(aInfo, options);

    if (options != null) {
        pendingOptions = options;
        final PendingIntent usageReport = pendingOptions.getUsageTimeReport();
        if (usageReport != null) {
            appTimeTracker = new AppTimeTracker(usageReport);
        }
        // Gets launch task display area and display id from options. Returns
        // null/INVALID_DISPLAY if not set.
        final WindowContainerToken daToken = options.getLaunchTaskDisplayArea();
        mHandoverTaskDisplayArea = daToken != null
                ? (TaskDisplayArea) WindowContainer.fromBinder(daToken.asBinder()) : null;
        mHandoverLaunchDisplayId = options.getLaunchDisplayId();
    }

    if (mPerf == null)
        mPerf = new BoostFramework();
}

```

### ActivityStack#startActivityLocked
这本分是真正启动activity前的准备工作
1,将ActivityRecord放到task最前面
2,准备过渡动画以及显示闪屏页
```
/// @frameworks/base/services/core/java/com/android/server/wm/ActivityStack.java
void startActivityLocked(ActivityRecord r, ActivityRecord focusedTopActivity,
        boolean newTask, boolean keepCurTransition, ActivityOptions options) {
    Task rTask = r.getTask(); // ActivityRecord所在的Task
    final boolean allowMoveToFront = options == null || !options.getAvoidMoveToFront();
    final boolean isOrhasTask = rTask == this || hasChild(rTask);// 等于此stack或是它的child
    // mLaunchTaskBehind tasks get placed at the back of the task stack.
    // 可以显示到前台,并且是 newTask或不包含此rTask
    if (!r.mLaunchTaskBehind && allowMoveToFront && (!isOrhasTask || newTask)) {
        // Last activity in task had been removed or ActivityManagerService is reusing task.
        // Insert or replace.
        // Might not even be in.
        positionChildAtTop(rTask); // 将rTask调整到top位置
    }
    Task task = null;
    if (!newTask && isOrhasTask) { // 非newTask,且在Stack中
        // Starting activity cannot be occluding activity, otherwise starting window could be
        // remove immediately without transferring to starting activity.
        final ActivityRecord occludingActivity = getOccludingActivityAbove(r);
        if (occludingActivity != null) { // 如被其他Task遮挡,则只是仅仅添加而不启动
            // Here it is!  Now, if this is not yet visible (occluded by another task) to the
            // user, then just add it without starting; it will get started when the user
            // navigates back to it.
            if (DEBUG_ADD_REMOVE) Slog.i(TAG, "Adding activity " + r + " to task " + task,
                    new RuntimeException("here").fillInStackTrace());
            rTask.positionChildAtTop(r);
            ActivityOptions.abort(options);
            return;
        }
    }

    // Place a new activity at top of stack, so it is next to interact with the user.

    // If we are not placing the new activity frontmost, we do not want to deliver the
    // onUserLeaving callback to the actual frontmost activity
    final Task activityTask = r.getTask();
    if (task == activityTask && mChildren.indexOf(task) != (getChildCount() - 1)) {// 此处应该是历史遗留问题,此处的task一直为null
        mStackSupervisor.mUserLeaving = false;
        if (DEBUG_USER_LEAVING) Slog.v(TAG_USER_LEAVING,
                "startActivity() behind front, mUserLeaving=false");
    }

    task = activityTask;

    // Slot the activity into the history stack and proceed
    if (DEBUG_ADD_REMOVE) Slog.i(TAG, "Adding activity " + r + " to stack to task " + task,
            new RuntimeException("here").fillInStackTrace());
    task.positionChildAtTop(r); // 将此ActivityRecord放置在Task的顶部

    if (mActivityPluginDelegate != null) {
        mActivityPluginDelegate.activityInvokeNotification
            (r.info.applicationInfo.packageName, r.occludesParent());
    }

    // The transition animation and starting window are not needed if {@code allowMoveToFront}
    // is false, because the activity won't be visible.
    // 不是home或recents类型 或 包含ActivityRecord 并且 能显示到前台
    if ((!isHomeOrRecentsStack() || hasActivity()) && allowMoveToFront) {
        final DisplayContent dc = getDisplay().mDisplayContent;
        if (DEBUG_TRANSITION) Slog.v(TAG_TRANSITION,
                "Prepare open transition: starting " + r);
        if ((r.intent.getFlags() & Intent.FLAG_ACTIVITY_NO_ANIMATION) != 0) { // 指定不需要动画
            dc.prepareAppTransition(TRANSIT_NONE, keepCurTransition);
            mStackSupervisor.mNoAnimActivities.add(r);
        } else {  // 下面指定需要动画的情况
            int transit = TRANSIT_ACTIVITY_OPEN;
            if (newTask) { // 如果是新任务
                if (r.mLaunchTaskBehind) {
                    transit = TRANSIT_TASK_OPEN_BEHIND;
                } else if (getDisplay().isSingleTaskInstance()) {
                    // If a new task is being launched in a single task display, we don't need
                    // to play normal animation, but need to trigger a callback when an app
                    // transition is actually handled. So ignore already prepared activity, and
                    // override it.
                    transit = TRANSIT_SHOW_SINGLE_TASK_DISPLAY;
                    keepCurTransition = false;
                } else {
                    // If a new task is being launched, then mark the existing top activity as
                    // supporting picture-in-picture while pausing only if the starting activity
                    // would not be considered an overlay on top of the current activity
                    // (eg. not fullscreen, or the assistant)
                    if (canEnterPipOnTaskSwitch(focusedTopActivity,
                            null /* toFrontTask */, r, options)) { // 是否支持 PIP
                        focusedTopActivity.supportsEnterPipOnTaskSwitch = true;// 设置支持PIP
                    }
                    transit = TRANSIT_TASK_OPEN;
                }
            }
            dc.prepareAppTransition(transit, keepCurTransition); // 准备过渡动画
            mStackSupervisor.mNoAnimActivities.remove(r);
        }
        boolean doShow = true;
        if (newTask) {
            // Even though this activity is starting fresh, we still need
            // to reset it to make sure we apply affinities to move any
            // existing activities from other tasks in to it.
            // If the caller has requested that the target task be
            // reset, then do so.
            if ((r.intent.getFlags() & Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED) != 0) {// 处理FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                resetTaskIfNeeded(r, r);
                doShow = topRunningNonDelayedActivityLocked(null) == r;
            }
        } else if (options != null && options.getAnimationType()
                == ActivityOptions.ANIM_SCENE_TRANSITION) {
            doShow = false;
        }
        if (r.mLaunchTaskBehind) {// 处理lunch behind
            // Don't do a starting window for mLaunchTaskBehind. More importantly make sure we
            // tell WindowManager that r is visible even though it is at the back of the stack.
            r.setVisibility(true);
            ensureActivitiesVisible(null, 0, !PRESERVE_WINDOWS);
            // Go ahead to execute app transition for this activity since the app transition
            // will not be triggered through the resume channel.
            getDisplay().mDisplayContent.executeAppTransition();
        } else if (SHOW_APP_STARTING_PREVIEW && doShow) { // 处理显示 starting window
            // Figure out if we are transitioning from another activity that is
            // "has the same starting icon" as the next one.  This allows the
            // window manager to keep the previous window it had previously
            // created, if it still had one.
            Task prevTask = r.getTask();
            ActivityRecord prev = prevTask.topActivityWithStartingWindow();
            if (prev != null) { // 处理是否重用之前的starting window
                // We don't want to reuse the previous starting preview if:
                // (1) The current activity is in a different task.
                if (prev.getTask() != prevTask) {
                    prev = null;
                }
                // (2) The current activity is already displayed.
                else if (prev.nowVisible) {
                    prev = null;
                }
            }
            // 最终通过PhoneWindowManager的addSplashScreen添加
            r.showStartingWindow(prev, newTask, isTaskSwitch(r, focusedTopActivity)); // 添加 starting window
        }
    } else {
        // If this is the first activity, don't do any fancy animations,
        // because there is nothing for it to animate on top of.
        ActivityOptions.abort(options);
    }
}
```

### RootWindowContainer#resumeFocusedStacksTopActivities
1,启动目标activity
2,收尾工作，确保有activity显示在display上
```
/// @frameworks/base/services/core/java/com/android/server/wm/RootWindowContainer.java
boolean resumeFocusedStacksTopActivities(
        ActivityStack targetStack, ActivityRecord target, ActivityOptions targetOptions) {

    if (!mStackSupervisor.readyToResume()) { // mDeferResumeCount为0
        return false;
    }

    boolean result = false;
    if (targetStack != null && (targetStack.isTopStackInDisplayArea()
            || getTopDisplayFocusedStack() == targetStack)) {
        result = targetStack.resumeTopActivityUncheckedLocked(target, targetOptions); // resume stack的Activity
    }

    // 处理resume activity后的收尾工作，确保有activity显示在display上
    for (int displayNdx = getChildCount() - 1; displayNdx >= 0; --displayNdx) {
        boolean resumedOnDisplay = false;
        final DisplayContent display = getChildAt(displayNdx);
        for (int tdaNdx = display.getTaskDisplayAreaCount() - 1; tdaNdx >= 0; --tdaNdx) {
            final TaskDisplayArea taskDisplayArea = display.getTaskDisplayAreaAt(tdaNdx);
            for (int sNdx = taskDisplayArea.getStackCount() - 1; sNdx >= 0; --sNdx) {
                final ActivityStack stack = taskDisplayArea.getStackAt(sNdx);
                final ActivityRecord topRunningActivity = stack.topRunningActivity();
                if (!stack.isFocusableAndVisible() || topRunningActivity == null) {
                    continue;
                }
                if (stack == targetStack) {
                    // Simply update the result for targetStack because the targetStack had
                    // already resumed in above. We don't want to resume it again, especially in
                    // some cases, it would cause a second launch failure if app process was
                    // dead.
                    resumedOnDisplay |= result;
                    continue;
                }
                if (taskDisplayArea.isTopStack(stack) && topRunningActivity.isState(RESUMED)) {
                    // Kick off any lingering app transitions form the MoveTaskToFront
                    // operation, but only consider the top task and stack on that display.
                    stack.executeAppTransition(targetOptions);
                } else {
                    resumedOnDisplay |= topRunningActivity.makeActiveIfNeeded(target);
                }
            }
        }
        if (!resumedOnDisplay) { // 无resumed activity显示
            // In cases when there are no valid activities (e.g. device just booted or launcher
            // crashed) it's possible that nothing was resumed on a display. Requesting resume
            // of top activity in focused stack explicitly will make sure that at least home
            // activity is started and resumed, and no recursion occurs.
            final ActivityStack focusedStack = display.getFocusedStack();
            if (focusedStack != null) { // 尝试resume 焦点stack
                result |= focusedStack.resumeTopActivityUncheckedLocked(target, targetOptions);
            } else if (targetStack == null) { // 尝试resume 桌面
                result |= resumeHomeActivity(null /* prev */, "no-focusable-task",
                        display.getDefaultTaskDisplayArea());
            }
        }
    }

    return result;
}

```
