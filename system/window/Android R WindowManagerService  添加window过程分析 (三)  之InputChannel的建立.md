
# InputChannel介绍
从 InputChannel 的注释来看, 它是指代了一个用于进程间传递输入事件的文件描述符. 实际上, 它是对socket pair的一端fd的的封装. 它是基于Parcelable的, 因而可以跨进程传递给另一端, 进而完成两端通信管道的建立.

```
    /**
     * An input channel specifies the file descriptors used to send input events
     * to a window in another process.  It is Parcelable so that it can be sent
     * to the process that is to receive events.  Only one thread should be reading
     * from an InputChannel at a time.
     * @hide
     */
```

在WMS#addWindow的流程中, 会通过WindowState#openInputChannel创建一对InputChannel,而他们本质上就是封装了native的socket pair的两端.之后将一端注册到IMS的InputDispatcher, 另一端返回给client用于创建WindowInputEventReceiver,接收相关事件.之后在两端之间就可以完成事件的分发以及事件处理结果的反馈. 接下来详细分析建立InputChannel的过程.

#  创建 InputChannel对
在WMS的addWindow方法方法会根据客户端传递过来的参数判断是否创建input channel, 要创建需满足以下条件:
 - outInputChannel 不为空 , 用于返回给客户端
 - INPUT_FEATURE_NO_INPUT_CHANNEL 没有设置

```
    // @WMS#addWindow
    final boolean openInputChannels = (outInputChannel != null
             && (attrs.inputFeatures & INPUT_FEATURE_NO_INPUT_CHANNEL) == 0);
     if  (openInputChannels) {
         win.openInputChannel(outInputChannel);
     }
```

#  WindowState#openInputChannel

```
    // @frameworks/base/services/core/java/com/android/server/wm/WindowState.java
    void openInputChannel(InputChannel outInputChannel) {
        if (mInputChannel != null) { // 不能为此WindowState重复创建input channel
            throw new IllegalStateException("Window already has an input channel.");
        }
        String name = getName();  // 获取InputChannel channel名
        // 创建一个input channel 对. native层是创建了socket pair, java只是对底层的封装
        InputChannel[] inputChannels = InputChannel.openInputChannelPair(name);  
        mInputChannel = inputChannels[0];
        mClientChannel = inputChannels[1];
        mWmService.mInputManager.registerInputChannel(mInputChannel); // 将0端的InputChannel注册到IMS
        mInputWindowHandle.token = mInputChannel.getToken(); // native创建的Binder token对象
        if (outInputChannel != null) {
          // 将 nativeInputChannel 的地址转换到 outInputChannel 的 mPtr
            mClientChannel.transferTo(outInputChannel);
            mClientChannel.dispose();  // 释放此InputChannel上的引用,以便回收
            mClientChannel = null;
        } else {
          // 如果client端window已死,创建DeadWindowEventReceiver, 接收到事件后,
          // 直接调用 finishInputEvent(event, true); 发送处理完毕反馈
            // If the window died visible, we setup a dummy input channel, so that taps
            // can still detected by input monitor channel, and we can relaunch the app.
            // Create dummy event receiver that simply reports all events as handled.
            mDeadWindowEventReceiver = new DeadWindowEventReceiver(mClientChannel);
        }
        mWmService.mInputToWindowMap.put(mInputWindowHandle.token, this); // 将token关联此WindowState
    },
```

这个方法首先通过openInputChannelPair 创建了一个socket pair , 然后通过registerInputChannel 将socketfd[0]注册到了IMS, 将socketfd[1]端通过transferTo转换到了outInputChannel, 之后这个outInputChannel返回给客户端创建另一端, 这样就使得IMS与客户端建立了事件分发与反馈的通道.
在客户端的ViewRootImpl通过此channel创建一个WindowInputEventReceiver, 通过此receiver可以接收到来自IMS端分发的事件, 也可以向其反馈事件处理完毕.

看一下另外一个有意思的地方 : InputChannel的名字是怎么确认的. 它是调用 getName 方法获取 :
WindowState的identityHashCode 取十六进制的值 + getWindowTag()

```
@Override
String getName() {
    return Integer.toHexString(System.identityHashCode(this))
            + " " + getWindowTag();
}

CharSequence getWindowTag() {
    CharSequence tag = mAttrs.getTitle();
    if (tag == null || tag.length() <= 0) { // 没有title使用packageName
        tag = mAttrs.packageName;
    }
    return tag;
}

```

这个名字还不是最终的, 在 InputChannel::openInputChannelPair 中会区分client 和 server
```
// InputChannel::openInputChannelPair
std::string serverChannelName = name + " (server)";
std::string clientChannelName = name + " (client)";
```
一些例子
```
282: channelName='f5a3811 com.android.systemui (server)', windowName='f5a3811 com.android.systemui (server)', status=NORMAL, monitor=false, responsive=true
  OutboundQueue: <empty>
  WaitQueue: <empty>
281: channelName='27d4286 StatusBar (server)', windowName='27d4286 StatusBar (server)', status=NORMAL, monitor=false, responsive=true
  OutboundQueue: <empty>
  WaitQueue: <empty>
302: channelName='recents_animation_input_consumer (server)', windowName='recents_animation_input_consumer (server)', status=NORMAL, monitor=false, responsive=true
  OutboundQueue: <empty>
  WaitQueue: <empty>
280: channelName='1d511e5 NotificationShade (server)', windowName='1d511e5 NotificationShade (server)', status=NORMAL, monitor=false, responsive=true

```

##  InputChannel#openInputChannelPair
openInputChannelPair 方法主要调用 nativeOpenInputChannelPair

```
    // @frameworks/base/core/java/android/view/InputChannel.java
    /**
     * Creates a new input channel pair.  One channel should be provided to the input
     * dispatcher and the other to the application's input queue.
     * @param name The descriptive (non-unique) name of the channel pair.
     * @return A pair of input channels.  The first channel is designated as the
     * server channel and should be used to publish input events.  The second channel
     * is designated as the client channel and should be used to consume input events.
     */
    public static InputChannel[] openInputChannelPair(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }

        if (DEBUG) {
            Slog.d(TAG, "Opening input channel pair '" + name + "'");
        }
        return nativeOpenInputChannelPair(name); // 调用native方法处理     
    }
```

### InputChannel相关的 JNI函数注册表
路径是frameworks/base/core/jni/android_view_InputChannel.cpp

```
static const JNINativeMethod gInputChannelMethods[] = {
    /* name, signature, funcPtr */
    { "nativeOpenInputChannelPair", "(Ljava/lang/String;)[Landroid/view/InputChannel;",
            (void*)android_view_InputChannel_nativeOpenInputChannelPair },
    { "nativeDispose", "(Z)V",
            (void*)android_view_InputChannel_nativeDispose },
    { "nativeRelease", "()V",
            (void*)android_view_InputChannel_nativeRelease },
    { "nativeTransferTo", "(Landroid/view/InputChannel;)V",
            (void*)android_view_InputChannel_nativeTransferTo },
    { "nativeReadFromParcel", "(Landroid/os/Parcel;)V",
            (void*)android_view_InputChannel_nativeReadFromParcel },
    { "nativeWriteToParcel", "(Landroid/os/Parcel;)V",
            (void*)android_view_InputChannel_nativeWriteToParcel },
    { "nativeGetName", "()Ljava/lang/String;",
            (void*)android_view_InputChannel_nativeGetName },
    { "nativeDup", "(Landroid/view/InputChannel;)V",
            (void*)android_view_InputChannel_nativeDup },
    { "nativeGetToken", "()Landroid/os/IBinder;",
            (void*)android_view_InputChannel_nativeGetToken },
};
```


因此,可知nativeOpenInputChannelPair方法对应的JNI函数是android_view_InputChannel_nativeOpenInputChannelPair.

## android_view_InputChannel_nativeOpenInputChannelPair
由上面的JNI函数表可知, nativeOpenInputChannelPair方法对应的JNI函数是android_view_InputChannel_nativeOpenInputChannelPair , 实现在android_view_InputChannel.cpp中. 这个函数的逻辑如下:
- InputChannel::openInputChannelPair 创建socket pair, 并将其封装为C++层的InputChannel
- 创建长度为2的object array ,即 channelPair数组
- 分别创建server/client 对应的java层InputChannel
- 保存java层InputChannelObj 到channelPair数组, 并返回


```
// @frameworks/base/core/jni/android_view_InputChannel.cpp
static jobjectArray android_view_InputChannel_nativeOpenInputChannelPair(JNIEnv* env,
        jclass clazz, jstring nameObj) {
    ScopedUtfChars nameChars(env, nameObj);
    std::string name = nameChars.c_str();

    sp<InputChannel> serverChannel;
    sp<InputChannel> clientChannel;
    status_t result = InputChannel::openInputChannelPair(name, serverChannel, clientChannel); // 创建InputChannel pair

    if (result) {
        std::string message = android::base::StringPrintf(
                "Could not open input channel pair : %s", strerror(-result));
        jniThrowRuntimeException(env, message.c_str());
        return nullptr;
    }
    // 创建一个大小为2的数组,存储创建的InputChannel
    jobjectArray channelPair = env->NewObjectArray(2, gInputChannelClassInfo.clazz, nullptr);
    if (env->ExceptionCheck()) {
        return nullptr;
    }
    // 创建server的java层InputChannel
    jobject serverChannelObj = android_view_InputChannel_createInputChannel(env, serverChannel);
    if (env->ExceptionCheck()) {
        return nullptr;
    }
    // 创建client的java 层InputChannel
    jobject clientChannelObj = android_view_InputChannel_createInputChannel(env, clientChannel);
    if (env->ExceptionCheck()) {
        return nullptr;
    }
    // 保存到数组
    env->SetObjectArrayElement(channelPair, 0, serverChannelObj);
    env->SetObjectArrayElement(channelPair, 1, clientChannelObj);
    return channelPair;
}
```

### InputChannel::openInputChannelPair
 InputChannel::openInputChannelPair 方法的实现在InputTransport.cpp中
此方法用于创建一个socket pair, socketpair它是一个全双工的通信实现. 可以往socketfd[0]中写，从socketfd[1]中读；也可以往socketfd[1]中写，从socketfd[0]中读； 

```
// @frameworks/native/libs/input/InputTransport.cpp
status_t InputChannel::openInputChannelPair(const std::string& name,
        sp<InputChannel>& outServerChannel, sp<InputChannel>& outClientChannel) {
    int sockets[2];
    // 创建socket pair
    if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, sockets)) {
        status_t result = -errno;
        ALOGE("channel '%s' ~ Could not create socket pair.  errno=%d",
                name.c_str(), errno);
        outServerChannel.clear();
        outClientChannel.clear();
        return result;
    }
    // 设置socket 的buffer大小
    int bufferSize = SOCKET_BUFFER_SIZE;
    setsockopt(sockets[0], SOL_SOCKET, SO_SNDBUF, &bufferSize, sizeof(bufferSize));
    setsockopt(sockets[0], SOL_SOCKET, SO_RCVBUF, &bufferSize, sizeof(bufferSize));
    setsockopt(sockets[1], SOL_SOCKET, SO_SNDBUF, &bufferSize, sizeof(bufferSize));
    setsockopt(sockets[1], SOL_SOCKET, SO_RCVBUF, &bufferSize, sizeof(bufferSize));

    sp<IBinder> token = new BBinder();  // 创建了一个BBinder对象的token, 保存在java层的mInputWindowHandle.token中
    // 根据socket fd 创建对应的InputChannel
    std::string serverChannelName = name + " (server)";
    android::base::unique_fd serverFd(sockets[0]);
    outServerChannel = InputChannel::create(serverChannelName, std::move(serverFd), token); // 创建C++ server channel

    std::string clientChannelName = name + " (client)";
    android::base::unique_fd clientFd(sockets[1]);
    outClientChannel = InputChannel::create(clientChannelName, std::move(clientFd), token); // 创建C++ client channel
    return OK;
}
```

### InputChannel::create
此方法用来创建InputChannel

```
// @frameworks/native/libs/input/InputTransport.cpp
sp<InputChannel> InputChannel::create(const std::string& name, android::base::unique_fd fd,
                                      sp<IBinder> token) {
    const int result = fcntl(fd, F_SETFL, O_NONBLOCK);  // 设置非阻塞
    if (result != 0) {
        LOG_ALWAYS_FATAL("channel '%s' ~ Could not make socket non-blocking: %s", name.c_str(),
                         strerror(errno));
        return nullptr;
    }
    return new InputChannel(name, std::move(fd), token); // 持有 token 对象
}
```

// 构造InputChannel
```
InputChannel::InputChannel(const std::string& name, android::base::unique_fd fd, sp<IBinder> token)
      : mName(name), mFd(std::move(fd)), mToken(token) {
    if (DEBUG_CHANNEL_LIFECYCLE) {
        ALOGD("Input channel constructed: name='%s', fd=%d", mName.c_str(), mFd.get());
    }
}
```

### android_view_InputChannel_createInputChannel
再次回到android_view_InputChannel_nativeOpenInputChannelPair函数, 当通过 InputChannel::openInputChannelPair 创建InputChannel对后, 再通过此native的android_view_InputChannel_createInputChannel函数创建java层的inputChannel对象
```
// @frameworks/native/libs/input/InputTransport.cpp
static jobject android_view_InputChannel_createInputChannel(JNIEnv* env,
        sp<InputChannel> inputChannel) {
    std::unique_ptr<NativeInputChannel> nativeInputChannel =
            std::make_unique<NativeInputChannel>(inputChannel);  // 通过inputChannel 创建 NativeInputChannel
    // 创建java InputChannel
    jobject inputChannelObj = env->NewObject(gInputChannelClassInfo.clazz,
            gInputChannelClassInfo.ctor);
    if (inputChannelObj) {
      // 将nativeInputChannel指针保存到java InputChannel的mPtr
        android_view_InputChannel_setNativeInputChannel(env, inputChannelObj,
                 nativeInputChannel.release());
    }
    return inputChannelObj;
}
```

### android_view_InputChannel_setNativeInputChannel
这个jni函数的作用是给java层的InputChannel的mPtr赋值, 保存native层的NativeInputChannel对象地址, 建立两者的联系
```
// 给 java层 InputChannel 的 mPtr 赋值为 NativeInputChannel对象地址
static void android_view_InputChannel_setNativeInputChannel(JNIEnv* env, jobject inputChannelObj,
        NativeInputChannel* nativeInputChannel) {
    env->SetLongField(inputChannelObj, gInputChannelClassInfo.mPtr,
             reinterpret_cast<jlong>(nativeInputChannel));
}
```

再次回到  WindowState#openInputChannel 方法, 通过InputChannel.openInputChannelPair返回的即是上面创建的InputChannel对.  接下来执行的是InputManager.registerInputChannel和mClientChannel.transferTo(outInputChannel) 操作, 最后存储 mInputWindowHandle.token.

首先来看简单一点的transferTo实现:

## InputChannel#transferTo
通过transferTo 方法将client InputChannel 的信息转换到outInputChannel返回给客户端
mClientChannel.transferTo(outInputChannel);
transferTo 使用的是nativeTransferTo方法进行转换
```
/**
 * Transfers ownership of the internal state of the input channel to another
 * instance and invalidates this instance.  This is used to pass an input channel
 * as an out parameter in a binder call.
 * @param other The other input channel instance.
 */
public void transferTo(InputChannel outParameter) {
    if (outParameter == null) {
        throw new IllegalArgumentException("outParameter must not be null");
    }

    nativeTransferTo(outParameter);
}
```

### android_view_InputChannel_nativeTransferTo
这个native方法对应的jni函数同样在InputTransport.cpp
结合上面, 此处的 obj 代表的是 mClientChannel ,  而 otherObj  代表的是outInputChannel
```
// @frameworks/native/libs/input/InputTransport.cpp
static void android_view_InputChannel_nativeTransferTo(JNIEnv* env, jobject obj,
        jobject otherObj) {
    if (android_view_InputChannel_getNativeInputChannel(env, otherObj) != nullptr) {
        jniThrowException(env, "java/lang/IllegalStateException",
                "Other object already has a native input channel.");
        return;
    }

    NativeInputChannel* nativeInputChannel =
            android_view_InputChannel_getNativeInputChannel(env, obj); // 首先通过java层的mPtr获取native层的NativeInputChannel
    android_view_InputChannel_setNativeInputChannel(env, otherObj, nativeInputChannel); // 将此nativeInputChannel 地址设置到 outInputChannel.mPtr   
    android_view_InputChannel_setNativeInputChannel(env, obj, nullptr);  // 将 mClientChannel.mPtr 设置成nullptr.
}
```

经过以上操作后, outInputChannel 就持有了NativeInputChannel 地址, 而mClientChannel 中的地址被清除, 成了空壳子.

##  InputManagerService#registerInputChannel
下面将InputChannel注册到IMS的naive实现, 使此InputChannel成为一个input事件的目标.
```
//@frameworks/base/services/core/java/com/android/server/input/InputManagerService.java
/**
 * Registers an input channel so that it can be used as an input event target. The channel is
 * registered with a generated token.
 *
 * @param inputChannel The input channel to register.
 */
public void registerInputChannel(InputChannel inputChannel) {
    if (inputChannel == null) {
        throw new IllegalArgumentException("inputChannel must not be null.");
    }

    nativeRegisterInputChannel(mPtr, inputChannel);
}
```

###  nativeRegisterInputChannel
native方法nativeRegisterInputChannel对应的jni实现是在com_android_server_input_InputManagerService.cpp中的同名函数
```
//@frameworks/base/services/core/jni/com_android_server_input_InputManagerService.cpp
static void nativeRegisterInputChannel(JNIEnv* env, jclass /* clazz */,
        jlong ptr, jobject inputChannelObj) {
    // 获取jni层的代表 NativeInputManager
    NativeInputManager* im = reinterpret_cast<NativeInputManager*>(ptr);
    // 获取native的InputChannel
    sp<InputChannel> inputChannel = android_view_InputChannel_getInputChannel(env,
            inputChannelObj);
    if (inputChannel == nullptr) {
        throwInputChannelNotInitialized(env);
        return;
    }
    // 向NativeInputManager 注册InputChannel
    status_t status = im->registerInputChannel(env, inputChannel);

    if (status) {
        std::string message;
        message += StringPrintf("Failed to register input channel.  status=%d", status);
        jniThrowRuntimeException(env, message.c_str());
        return;
    }
 	// 设置InputChannel disposed 时的回调,  此回调会在InputChannel对象执行dispose方法时被用到
    android_view_InputChannel_setDisposeCallback(env, inputChannelObj,
            handleInputChannelDisposed, im);
}
```


#### NativeInputManager#registerInputChannel
mInputManager即native层的InputManager, 通过它获取InputDispatcher对象, 然后通过InputDispatcher执行注册
```
status_t NativeInputManager::registerInputChannel(JNIEnv* /* env */,
        const sp<InputChannel>& inputChannel) {
    ATRACE_CALL();

    return mInputManager->getDispatcher()->registerInputChannel(inputChannel);
}
```

#### InputDispatcher#registerInputChannel
这个函数的工作如下:
- 创建Connection对象
- 以InputChannel的fd为key存储connection , 以此InputChannel的token存储InputChannel
- 注册 InputChannel的fd到Looper的监听列表, 当收到ALOOPER_EVENT_INPUT , 回调handleReceiveCallback

```
status_t InputDispatcher::registerInputChannel(const sp<InputChannel>& inputChannel) {
#if DEBUG_REGISTRATION
    ALOGD("channel '%s' ~ registerInputChannel", inputChannel->getName().c_str());
#endif

    { // acquire lock
        std::scoped_lock _l(mLock);
        // 获取是否已经存在了 Connection
        sp<Connection> existingConnection = getConnectionLocked(inputChannel->getConnectionToken());
        if (existingConnection != nullptr) { // 存在则不重复添加
            ALOGW("Attempted to register already registered input channel '%s'",
                  inputChannel->getName().c_str());
            return BAD_VALUE;
        }
       // 根据InputChannel创建一个Connection
        sp<Connection> connection = new Connection(inputChannel, false /*monitor*/, mIdGenerator);

        int fd = inputChannel->getFd();
        mConnectionsByFd[fd] = connection; // 以InputChannel的fd为key存储connection
        mInputChannelsByToken[inputChannel->getConnectionToken()] = inputChannel; // 以此InputChannel的token存储InputChannel

        mLooper->addFd(fd, 0, ALOOPER_EVENT_INPUT, handleReceiveCallback, this); // 将此fd添加到监听列表
    } // release lock

    // Wake the looper because some connections have changed.
    mLooper->wake();
    return OK;
}
```

Connection 类是对一个InputChannel 上的input事件分发状态的管理者, 定义如下,可以发现它是在命名空间android::inputdispatcher中定义的:
```
// @frameworks/native/services/inputflinger/dispatcher/Connection.h
namespace android::inputdispatcher {

struct DispatchEntry;

/* Manages the dispatch state associated with a single input channel. */
class Connection : public RefBase {
protected:
    virtual ~Connection();

public:
    enum Status {  // 状态值
        // Everything is peachy.
        STATUS_NORMAL,
        // An unrecoverable communication error has occurred.
        STATUS_BROKEN,
        // The input channel has been unregistered.
        STATUS_ZOMBIE
    };

    Status status;
    sp<InputChannel> inputChannel; // never null
    bool monitor;
    InputPublisher inputPublisher;
    InputState inputState;

    // True if this connection is responsive.
    // If this connection is not responsive, avoid publishing more events to it until the
    // application consumes some of the input.
    bool responsive = true;

    // Queue of events that need to be published to the connection.
    std::deque<DispatchEntry*> outboundQueue; // 需要发布的事件队列

    // Queue of events that have been published to the connection but that have not
    // yet received a "finished" response from the application.
    std::deque<DispatchEntry*> waitQueue; // 等待客户端回复的事件队列

    Connection(const sp<InputChannel>& inputChannel, bool monitor, const IdGenerator& idGenerator);

    inline const std::string getInputChannelName() const { return inputChannel->getName(); }

    const std::string getWindowName() const;
    const char* getStatusLabel() const;

    std::deque<DispatchEntry*>::iterator findWaitQueueEntry(uint32_t seq);
};
} // namespace android::inputdispatcher
```

当收到ALOOPER_EVENT_INPUT , 回调handleReceiveCallback. 通常是客户端发送处理完事件的反馈.
####  InputDispatcher::handleReceiveCallback
这个分发主要是处理客户端发送的反馈 ,完成消息循环的处理.
```
int InputDispatcher::handleReceiveCallback(int fd, int events, void* data) {
    InputDispatcher* d = static_cast<InputDispatcher*>(data);

    { // acquire lock
        std::scoped_lock _l(d->mLock);
        // 查找fd对应的Connection
        if (d->mConnectionsByFd.find(fd) == d->mConnectionsByFd.end()) {
            ALOGE("Received spurious receive callback for unknown input channel.  "
                  "fd=%d, events=0x%x",
                  fd, events);
            return 0; // remove the callback
        }

        bool notify;
        sp<Connection> connection = d->mConnectionsByFd[fd];
        if (!(events & (ALOOPER_EVENT_ERROR | ALOOPER_EVENT_HANGUP))) { // 正常收到事件
            if (!(events & ALOOPER_EVENT_INPUT)) { // 事件类型不对,则直接返回
                ALOGW("channel '%s' ~ Received spurious callback for unhandled poll event.  "
                      "events=0x%x",
                      connection->getInputChannelName().c_str(), events);
                return 1;
            }

            nsecs_t currentTime = now();
            bool gotOne = false;
            status_t status;
            for (;;) {
                uint32_t seq;
                bool handled;
                // 接收client 的finish 消息
                status = connection->inputPublisher.receiveFinishedSignal(&seq, &handled);  // 事件完成处理的善后
                if (status) {
                    break;
                }
                // 结束派发反馈的cycle, 将要执行的操作封装成一个CommandEntry,添加到mCommandQueue
                d->finishDispatchCycleLocked(currentTime, connection, seq, handled);
                gotOne = true;
            }
            // gotOne为true , 则必然成功收到了client的finish消息, 执行mCommandQueue中的commands
            if (gotOne) {
                d->runCommandsLockedInterruptible();
                if (status == WOULD_BLOCK) {
                    return 1;
                }
            }

            notify = status != DEAD_OBJECT || !connection->monitor;
            if (notify) {
                ALOGE("channel '%s' ~ Failed to receive finished signal.  status=%d",
                      connection->getInputChannelName().c_str(), status);
            }
        } else {
            // Monitor channels are never explicitly unregistered.
            // We do it automatically when the remote endpoint is closed so don't warn
            // about them.
            const bool stillHaveWindowHandle =
                    d->getWindowHandleLocked(connection->inputChannel->getConnectionToken()) !=
                    nullptr;
            notify = !connection->monitor && stillHaveWindowHandle;
            if (notify) {
                ALOGW("channel '%s' ~ Consumer closed input channel or an error occurred.  "
                      "events=0x%x",
                      connection->getInputChannelName().c_str(), events);
            }
        }

        // Unregister the channel.
        d->unregisterInputChannelLocked(connection->inputChannel, notify);
        return 0; // remove the callback
    }             // release lock
}
```

关于client发送反馈信号参考另外一篇[Android R InputChannel之发送事件处理反馈](https://gitee.com/pecuyu/android-notes/blob/master/system/input/Android%20R%20InputChannel%E4%B9%8B%E5%8F%91%E9%80%81%E4%BA%8B%E4%BB%B6%E5%A4%84%E7%90%86%E5%8F%8D%E9%A6%88.md)

# 应用端对InputChannel的处理
ViewRootImpl的setView方法中, 在addToDisplay后返回WMS创建的InputChannel, 之后判断此InputChannel是否为null 来创建 WindowInputEventReceiver

```
if (inputChannel != null) {
    if (mInputQueueCallback != null) {
        mInputQueue = new InputQueue();
        mInputQueueCallback.onInputQueueCreated(mInputQueue);
    }
    mInputEventReceiver = new WindowInputEventReceiver(inputChannel,  // 创建receiver接收input事件
            Looper.myLooper());
}
```


## InputEventReceiver 构造方法
重点关注WindowInputEventReceiver 的构造方法, 此方法直接调用父类 InputEventReceiver的构造
```
/**
 * Creates an input event receiver bound to the specified input channel.
 *
 * @param inputChannel The input channel.
 * @param looper The looper to use when invoking callbacks.
 */
public InputEventReceiver(InputChannel inputChannel, Looper looper) {
    if (inputChannel == null) {
        throw new IllegalArgumentException("inputChannel must not be null");
    }
    if (looper == null) {
        throw new IllegalArgumentException("looper must not be null");
    }

    mInputChannel = inputChannel;
    mMessageQueue = looper.getQueue();
    // 执行native方法, 创建native的对象.
    mReceiverPtr = nativeInit(new WeakReference<InputEventReceiver>(this),
            inputChannel, mMessageQueue);

    mCloseGuard.open("dispose");
}
```

### InputEventReceiver#nativeInit
此native方法对应的jni函数在android_view_InputEventReceiver,  与native方法同名
```
// @frameworks/base/core/jni/android_view_InputEventReceiver.cpp
static jlong nativeInit(JNIEnv* env, jclass clazz, jobject receiverWeak,
        jobject inputChannelObj, jobject messageQueueObj) {
    // 获取native层的InputChannel代表        
    sp<InputChannel> inputChannel = android_view_InputChannel_getInputChannel(env,
            inputChannelObj);
    if (inputChannel == nullptr) {
        jniThrowRuntimeException(env, "InputChannel is not initialized.");
        return 0;
    }

    sp<MessageQueue> messageQueue = android_os_MessageQueue_getMessageQueue(env, messageQueueObj);
    if (messageQueue == nullptr) {
        jniThrowRuntimeException(env, "MessageQueue is not initialized.");
        return 0;
    }
    // 创建一个 NativeInputEventReceiver, 它实现了 LooperCallback
    sp<NativeInputEventReceiver> receiver = new NativeInputEventReceiver(env,
            receiverWeak, inputChannel, messageQueue);
    status_t status = receiver->initialize(); // 初始化NativeInputEventReceiver
    if (status) {
        String8 message;
        message.appendFormat("Failed to initialize input event receiver.  status=%d", status);
        jniThrowRuntimeException(env, message.string());
        return 0;
    }

    receiver->incStrong(gInputEventReceiverClassInfo.clazz); // retain a reference for the object
    return reinterpret_cast<jlong>(receiver.get());  // 返回地址给java层
}
```

NativeInputEventReceiver的构造函数
```
NativeInputEventReceiver::NativeInputEventReceiver(JNIEnv* env,
        jobject receiverWeak, const sp<InputChannel>& inputChannel,
        const sp<MessageQueue>& messageQueue) :
        mReceiverWeakGlobal(env->NewGlobalRef(receiverWeak)), // 使用全局引用保存 java层InputEventReceiver的弱引用
        // 构建InputConsumer,用于接收输入事件与发送事件反馈. 而在InputDispatcher,使用InputPublisher发送输入事件与接收事件反馈
        mInputConsumer(inputChannel), mMessageQueue(messageQueue),
        mBatchedInputEventPending(false), mFdEvents(0) {
    if (kDebugDispatchCycle) {
        ALOGD("channel '%s' ~ Initializing input event receiver.", getInputChannelName().c_str());
    }
}
```

### NativeInputEventReceiver::initialize
接下来NativeInputEventReceiver::initialize 函数
```
status_t NativeInputEventReceiver::initialize() {
    setFdEvents(ALOOPER_EVENT_INPUT); // 监听 ALOOPER_EVENT_INPUT事件  
    return OK;
}
```

setFdEvents 函数主要是对InputChannel的fd添加/移除事件监听
```
void NativeInputEventReceiver::setFdEvents(int events) {
    if (mFdEvents != events) {
        mFdEvents = events;
        int fd = mInputConsumer.getChannel()->getFd();
        if (events) {
            // 此处事件触发的callback 是this ,表示NativeInputEventReceiver 自身.
            mMessageQueue->getLooper()->addFd(fd, 0, events, this, nullptr);
        } else {
            mMessageQueue->getLooper()->removeFd(fd);
        }
    }
}
```

## NativeInputEventReceiver::handleEvent
当收到IMS分发的input事件时, 会触发监听fd设置的事件,并回调callback函数 handleEvent
```
int NativeInputEventReceiver::handleEvent(int receiveFd, int events, void* data) {
    if (events & (ALOOPER_EVENT_ERROR | ALOOPER_EVENT_HANGUP)) {
        // This error typically occurs when the publisher has closed the input channel
        // as part of removing a window or finishing an IME session, in which case
        // the consumer will soon be disposed as well.
        if (kDebugDispatchCycle) {
            ALOGD("channel '%s' ~ Publisher closed input channel or an error occurred.  "
                    "events=0x%x", getInputChannelName().c_str(), events);
        }
        return 0; // remove the callback
    }

    if (events & ALOOPER_EVENT_INPUT) { // 处理input事件
        JNIEnv* env = AndroidRuntime::getJNIEnv();
        status_t status = consumeEvents(env, false /*consumeBatches*/, -1, nullptr);  // 调用 consumeEvents 去处理和分发事件
        mMessageQueue->raiseAndClearException(env, "handleReceiveCallback");
        return status == OK || status == NO_MEMORY ? 1 : 0;
    }

    if (events & ALOOPER_EVENT_OUTPUT) { // client端fd可写时收到EVENT_OUTPUT事件,继续处理pending的事件反馈..
        for (size_t i = 0; i < mFinishQueue.size(); i++) { // 遍历pending事件反馈队列
            const Finish& finish = mFinishQueue.itemAt(i);
            status_t status = mInputConsumer.sendFinishedSignal(finish.seq, finish.handled); // 发送事件反馈
        ...}
        mFinishQueue.clear();  // 所有发送完成后清除队列
        setFdEvents(ALOOPER_EVENT_INPUT); // 只设置ALOOPER_EVENT_INPUT, 移除了ALOOPER_EVENT_OUTPUT监听,也就是不需要监听可写事件了.
        return 1;
    }
    return 1;
}
```

## NativeInputEventReceiver::consumeEvents
消费 ALOOPER_EVENT_INPUT 事件

```
status_t NativeInputEventReceiver::consumeEvents(JNIEnv* env,
        bool consumeBatches, nsecs_t frameTime, bool* outConsumedBatch) {
  ...
    ScopedLocalRef<jobject> receiverObj(env, nullptr);
    bool skipCallbacks = false;
    for (;;) {
        uint32_t seq;
        int motionEventType = -1;
        int touchMoveNum = -1;
        bool flag = false;

        InputEvent* inputEvent;
       //  这个地方是通过 mChannel->receiveMessage 获取服务端发送来的数据, 进行加工
        status_t status = mInputConsumer.consume(&mInputEventFactory,
                consumeBatches, frameTime, &seq, &inputEvent,
                &motionEventType, &touchMoveNum, &flag);
    	...

        if (!skipCallbacks) {
      ... // 下面根据type获取对应的inputEvent对象
            jobject inputEventObj;
            switch (inputEvent->getType()) {
            case AINPUT_EVENT_TYPE_KEY:
                if (kDebugDispatchCycle) {
                    ALOGD("channel '%s' ~ Received key event.", getInputChannelName().c_str());
                }
                inputEventObj = android_view_KeyEvent_fromNative(env,
                        static_cast<KeyEvent*>(inputEvent));
                break;

            case AINPUT_EVENT_TYPE_MOTION: {..}
            case AINPUT_EVENT_TYPE_FOCUS: {...}
            default:
                assert(false); // InputConsumer should prevent this from ever happening
                inputEventObj = nullptr;
            }
    	       // 获取到了inputEvent , 进行分发
            if (inputEventObj) {
                if (kDebugDispatchCycle) {
                    ALOGD("channel '%s' ~ Dispatching input event.", getInputChannelName().c_str());
                }
                env->CallVoidMethod(receiverObj.get(),
                        gInputEventReceiverClassInfo.dispatchInputEvent, seq, inputEventObj); // 调用 InputEventReceiver 的 dispatchInputEvent  
                if (env->ExceptionCheck()) {
                    ALOGE("Exception dispatching input event.");
                    skipCallbacks = true;
                }
                env->DeleteLocalRef(inputEventObj);
            } else {
                ALOGW("channel '%s' ~ Failed to obtain event object.",
                        getInputChannelName().c_str());
                skipCallbacks = true;
            }
        }

        if (skipCallbacks) { // 没有获取到inputEvent等情况, 直接发送事件处理完的反馈给server
            mInputConsumer.sendFinishedSignal(seq, false);
        }
    }
}
```

## InputEventReceiver#dispatchInputEvent
java 层 InputEventReceiver 对事件的分发

```
// Called from native code.
@SuppressWarnings("unused")
@UnsupportedAppUsage
private void dispatchInputEvent(int seq, InputEvent event) {
    mSeqMap.put(event.getSequenceNumber(), seq);
    onInputEvent(event);
}
```

## WindowInputEventReceiver#onInputEvent
在ViewRootImpl中创建了WindowInputEventReceiver ,它是InputEventReceiver 的子类, 因此它的 onInputEvent 会被调用. 到此,client端收到了事件.
```
@Override
public void onInputEvent(InputEvent event) {
    Trace.traceBegin(Trace.TRACE_TAG_VIEW, "processInputEventForCompatibility");
    List<InputEvent> processedEvents;
    try {
        processedEvents =
            mInputCompatProcessor.processInputEventForCompatibility(event);
    } finally {
        Trace.traceEnd(Trace.TRACE_TAG_VIEW);
    }
    if (processedEvents != null) {
        if (processedEvents.isEmpty()) { // 没有event, 直接结束
            // InputEvent consumed by mInputCompatProcessor
            finishInputEvent(event, true);
        } else { // 否则加入队列等待分发
            for (int i = 0; i < processedEvents.size(); i++) {
                enqueueInputEvent(
                        processedEvents.get(i), this,
                        QueuedInputEvent.FLAG_MODIFIED_FOR_COMPATIBILITY, true);
            }
        }
    } else {
        enqueueInputEvent(event, this, 0, true);
    }
}
```
