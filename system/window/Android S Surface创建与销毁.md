
# 创建 surface
继承自WindowContainer的组件，如 Task、ActivityRecord / WindowToken、WindowState，当创建并添加到 Parent 时，会回调其onParentChanged方法，在该方法中会创建其对应的 surface
```java
/// frameworks/base/services/core/java/com/android/server/wm/WindowContainer.java
void onParentChanged(ConfigurationContainer newParent, ConfigurationContainer oldParent,
        PreAssignChildLayersCallback callback) {
    super.onParentChanged(newParent, oldParent);
    if (mParent == null) {
        return;
    }

    if (mSurfaceControl == null) {
        // If we don't yet have a surface, but we now have a parent, we should
        // build a surface.
        createSurfaceControl(false /*force*/); // 创建 surface
    } else {
        // If we have a surface but a new parent, we just need to perform a reparent. Go through
        // surface animator such that hierarchy is preserved when animating, i.e.
        // mSurfaceControl stays attached to the leash and we just reparent the leash to the
        // new parent.
        reparentSurfaceControl(getSyncTransaction(), mParent.mSurfaceControl); // reparent  操作
    }

    if (callback != null) {
        callback.onPreAssignChildLayers();
    }

    // Either way we need to ask the parent to assign us a Z-order.
    mParent.assignChildLayers(); // 处理 layer 层级
    scheduleAnimation();
}
```

## 创建 Task surface
如下是启动Activity流程，将 Task 添加到 TaskDisplayArea， 会创建一个Surface，名字如Task=33，对应sf创建的是 EffectLayer，
```java
at android.view.SurfaceControl$Builder.build (SurfaceControl.java:1266)
at com.android.server.wm.Windowcontainer.setInitialSurfaceControlProperties (windowContainer.java:446)
at com.android.server.wm.Task.setInitialSurfaceControlProperties (Task.java:3225)
at com.android. server.wm.WindowContainer.createSurfacecontrol (WindowContainer.java:442)
at com.android. server.wm.WindowContainer.onParentchanged (WindowContainer.java:423)
at com.android. server.wm.WindowContainer.onParentchanged (windowcontainer.java:410)
at com.android.server.wm.Task.onParentchanged (Task.java:1237)
at com.android.server.wm.windowcontainer.setParent (windowcontainer.java:400)
at com.android.server.wm.WindowContainer.addChild (WindowContainer.java:574)
at com.android. server.wm.TaskDisplayArea.addChildTask (TaskDisplayArea.java:390)
at com.android.server.wm.TaskDisplayArea.addChild (TaskDisplayArea.java:376)
at com.android.server.wm.Task$Builder.build (Task.java:7112)
at com.android.server.wm.TaskDisplayArea.getOrCreateRootTask (TaskDisplayArea.java:1163)
at com.android. server.wm.TaskDisplayArea.getOrCreateRootTask (TaskDisplayArea.java:1194)
at com.android. server.wm.RootWindowContainer.getLaunchRootTask (RootwindowContainer.java:3411)
at com.android.server.wm.ActivityStarter.getLaunchRootTask (Activitystarter.java:3514)
at com.android.server.wm.ActivityStarter.startActivityInner (Activitystarter.java:2239)
at com.android. server.wm.ActivityStarter.startActivityUnchecked (Activitystarter.java:2007)
at com.android.server.wm.ActivityStarter,executeRequest (Activitystarter.java:1601)
at com.android.server.wm.ActivityStarter.execute (activitystarter.java:888)
at com.android server .wm.ActivityraskManagerservice.startActivityAsuser (ActivityraskManagerservice.java:1373)
at com.android.server.wm.ActivityTaskManagerservice.startActivityAsuser (ActivityTaskManagerservice.java:1298)
at com,android.server.wm.ActivityTaskManagerservice.startActivity (ActivityTaskManagerservice.java:1273)
at android.app.IActivityTaskManager$Stub.onTransact (IActivityTaskManager.java:1001)
at com.android.server.wm.ActivityTaskManagerservice.onTransact (ActivityTaskManagerservice.java:5609)
at android.os.Binder.execTransactInternal (Binder.java:1203)
at android.os.Binder.execTransact (Binder.java:1157)
```


Task 重写 setInitialSurfaceControlProperties 方法， task会创建一个 effect layer
```java
/// @frameworks/base/services/core/java/com/android/server/wm/Task.java
@Override
void setInitialSurfaceControlProperties(SurfaceControl.Builder b) {
    b.setEffectLayer().setMetadata(METADATA_TASK_ID, mTaskId);
    super.setInitialSurfaceControlProperties(b);
}

// SurfaceControl$Builder
public Builder setEffectLayer() {
    mFlags |= NO_COLOR_FILL;
    unsetBufferSize();
    // 注意指定的 FX_SURFACE_EFFECT
    return setFlags(FX_SURFACE_EFFECT, FX_SURFACE_MASK);
}
```

## 创建 ActivityRecord surface
如下是启动Activity流程，将 ActivityRecord 添加到 Task， 会创建一个Surface，名字如 ActivityRecord{bf3fd2d u0 com.google.android.dialer/.extensions.GoogleDialtactsActivity，对应sf创建的是 ContainerLayer
```java
at android.view.SurfaceControlsBuilder.build (surfaceControl.java:1266)
at com.android. server.wm.WindowContainer.setInitialSurfaceControlProperties (Windowontainer.java:446)
at com.android.server.wm.WindowContainer.createSurfaceControl (windowcontainer.java:442)
at com.android. server.wm.WindowToken.createSurfaceControl (windowToken.java:372)
at com.android. server.wm.WindowContainer.onParentchanged (windowContainer.java:423)
at com.android.server.wm.WindowContainer.onParentChanged (WindowContainer.java:410)
at com.android. server.wm.ActivityRecord.onParentChanged (ActivityRecord.java:1511)
at com.android. server.wm.WindowContainer.setParent (WindowContainer.java:400)
at com.android. server.wm.WindowContainer.addChild (Windowcontainer.java:574)
at com.android.server.wm.TaskFragment.addchild (TaskFragment.java:1999)
at com.android.server.wm.Task.addchild(Task.java:1500)
at com.android. server.wm.ActivityStarter.addOrReparentsStartingActivity (Activitystarter.java:3445)
at com.android. server.wm.ActivityStarter.setNewTask (Activitystarter.java:3365)
at com.android. server.wm.ActivityStarter.startActivityInner (Activitystarter.java:2267)
at com.android.server.wm.ActivityStarter.startActivityUnchecked (Activitystarter.java:2007)
at com.android. server.wm.ActivityStarter.executeRequest (Activitystarter.java:1601)
at com.android. server.wm.Activitystarter.execute (Activitystarter.java:888)
at com.android.server.wm.ActivityTaskManagerService.startActivityAsuser (ActivityTaskManagerservice.java:1298)
at com.android. server.wm.ActivityTaskManagerService.startActivityAsUser (ActivityTaskManagerservice.java:1373)
at com.android. server.wm.ActivityTaskManagerService.startActivity (ActivityTaskManagerservice.java:1273)
at android.app.IActivityTaskManager$Stub.onTransact (IActivityTaskManager.java:1001)
at com.android. server.wm.ActivityTaskManagerService.onTransact (ActivityTaskManagerservice.java:5609)
at android.os.Binder.execTransactInternal (Binder.java:1203)
at android.os.Binder.execTransact (Binder.java:1157)
```

## 创建 WindowState surface
此流程是 WindowManagerservice#addWindow , 将 WindowState 添加到ActivityRecord/WindowToken，将创建一个Surface，名字如 79d6a24 com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity，对应sf创建的是 ContainerLayer
```java
at android.view.SurfaceControl$Builder.build (surfaceControl.java:1266)
at com.android. server.wm.WindowContainer.setInitialSurfaceControlProperties (WindowContainer.java:446)
at com.android.server.wm.Windowcontainer.createSurfaceControl (WindowContainer.java:442)
at com.android.server.wm.Windowcontainer.onParentchanged (windowcontainer.java:423)
at com.android.server.wm.WindowContainer.onParentchanged (Windowcontainer.java:410)
at com.android.server.wm.Windowstate.onParentchanged (Windowstate.java:1278)
at com.android.server.wm.windowcontainer.setParent (windowcontainer.java:400)
at com.android.server.wm.WindowContainer.addChild (WindowContainer.java:546)
at com.android.server.wm.WindowToken.addWindow (WindowToken.java:321)
at com.android.server.wm.ActivityRecord.addWindow (ActivityRecord.java:4186)
at com.android. server.wm.WindowManagerservice.addWindow (WindowManagerservice.java:1935)
at com.android.server.wm.Session.addToDisplayAsUser (session.java:204)
at android.view.IWindowSession$Stub.onTransact (IWindowsession.java:646)
at com.android.server.wm.Session.onTransact (session.java:170)
```

## 创建 WindowStateAnimator surface
此流程是 WindowManagerService#relayoutWindow 流程，将创建一个 BufferStateLayer
```java
at android.view.SurfaceControl$Builder.build (surfaceControl.java:1266)
at com.android.server.wm.WindowSurfaceController.<init> (Windowsurfacecontroller.java:109)
at com.android.server.wm.WindowStateAnimator.createSurfaceLocked (windowstatenimator.java:356)
at com.android.server.wm.WindowManagerservice.createSsurfaceControl (windowManagerservice.java:2925)
at com.android. server.wm.WindowManagerService.relayoutWindow (WindowManagerservice.java:2624)
at com.android.server.wm.Session.relayout (session.java:235)
at android.view.IWindowSession$Stub.onTransact (IWindowsession.java:745)
at com.android.server.wm.Session.onTransact (session.java:170)
at android.os.Binder.execTransactInternal (Binder.java:1198)
```

因此上面创建layer层级如下：
- Task  
  - 会创建一个 EffectLayer
- ActivityRecord / WindowToken
  - 会创建一个 ContainerLayer
- WindowState
  - 会创建一个 ContainerLayer

WindowStateAnimator 会创建应用需要的Surface，目前创建的一般是BufferStateLayer (BLAST layer)

从 dumpsys SurfaceFlinger 看创建的 layer 信息
```c
// 为 task 创建的 EffectLayer
+ EffectLayer (Task=33#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=    30002, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,   0,   0], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=0, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(-1.000,-1.000,-1.000,1.000), flags=0x00000000, tr=[0.00, 0.00][0.00, 0.00]
      parent=DefaultTaskDisplayArea#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={taskId:33}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,
// 为 ActivityRecord 创建的 ContainerLayer      
+ ContainerLayer (ActivityRecord{bf3fd2d u0 com.google.android.dialer/.extensions.GoogleDialtactsActivity#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=        0, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,  -1,  -1], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=1, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(0.000,0.000,0.000,1.000), flags=0x00000000, tr=[0.00, 0.00][0.00, 0.00]
      parent=Task=33#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,
// 为 WindowState 创建的 ContainerLayer
+ ContainerLayer (79d6a24 com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=        0, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,  -1,  -1], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=1, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(0.000,0.000,0.000,1.000), flags=0x00000000, tr=[0.00, 0.00][0.00, 0.00]
      parent=ActivityRecord{bf3fd2d u0 com.google.android.dialer/.extensions.GoogleDialtactsActivity#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,
// WindowStateAnimator 创建的 BufferStateLayer， 提供应用使用做绘制
+ BufferStateLayer (com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity#0) uid=10099
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=1)
    [  0,   0, 1080, 1920]
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=        0, pos=(0,0), size=(1080,1920), crop=[  0,   0,   0,   0], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=1, invalidate=0, dataspace=Default, defaultPixelFormat=RGBA_8888, backgroundBlurRadius=0, color=(0.000,0.000,0.000,1.000), flags=0x00000102, tr=[0.00, 0.00][0.00, 0.00]
      parent=79d6a24 com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity#0
      zOrderRelativeOf=none
      activeBuffer=[1080x1920:1080,RGBA_8888], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={dequeueTime:266112269947, windowType:1, ownerPID:3284, ownerUID:10099}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,}
```

# 销毁 Surface
此流程是Activity进入stop状态销毁Surface流程
```java
at android.view.SurfaceControl.release (surfacecontrol.java:1740)
at android.view.SurfaceControl$Transaction.remove (surfacecontrol.java:3783)
at com.android.server.wm.Windowsurfacecontroller.destroy (Windowsurfacecontroller.java:139)
at com.android.server.wm.WindowstateAnimator.destroySurface (WindowstateAnimator.java:886)
at com.android.server.wm.Windowtate.destroySurfaceUnchecked (windowstate.java:3777)
at com.android.server.wm.WindowstateAnimator.destroySurfaceLocked (windowstateanimator.java:426)
at com.android.server.wm.Windowstate.destroySurface (windowstate.java:3751)
at com.android.server.wm.ActivityRecord.destroySurfaces (ActivityRecord.java:5600)
at com.android.server.wm.ActivityRecord.destroySurfaces (ActivityRecord.java:5581)
at com.android.server.wm.ctivityRecord.notifyAppStopped (activityRecord.java:5646)
at com.android.server.wm.ActivityRecord.activityStopped (ActivityRecord java:6235)
at com.android.server.wm.ActivityClientController.activityStopped (Activityclientcontroller.java:214)
at android.app.IActivitycLientcontroller$Stub.onTransact (IActivityClientcontroller.java:587)
at com.android. server.wm.ActivityClientController.onTransact (Activityclientcontroller.java:128)
at android.os.Binder.execTransactInternal (Binder.java:1203)
```

关键之处在 SurfaceControl$Transaction.remove 方法
```java
/**
 * Equivalent to reparent with a null parent, in that it removes
 * the SurfaceControl from the scene, but it also releases
 * the local resources (by calling {@link SurfaceControl#release})
 * after this method returns, {@link SurfaceControl#isValid} will return
 * false for the argument.
 *
 * @param sc The surface to remove and release.
 * @return This transaction
 * @hide
 */
@NonNull
public Transaction remove(@NonNull SurfaceControl sc) {
    reparent(sc, null); // 将此Surface parent 置空，将移除 layer
    sc.release(); // 释放 SurfaceControl 资源
    return this;
}
```

此时再查看sf layer信息，此时 BufferStateLayer (com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity#0) 已经不存在
```c
+ EffectLayer (Task=33#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=    30002, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,   0,   0], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=0, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(-1.000,-1.000,-1.000,1.000), flags=0x00000000, tr=[0.00, 0.00][0.00, 0.00]
      parent=DefaultTaskDisplayArea#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={taskId:33}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,
+ ContainerLayer (ActivityRecord{bf3fd2d u0 com.google.android.dialer/.extensions.GoogleDialtactsActivity#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=        0, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,  -1,  -1], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=1, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(0.000,0.000,0.000,1.000), flags=0x00000001, tr=[0.00, 0.00][0.00, 0.00]
      parent=Task=33#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000,
+ ContainerLayer (79d6a24 com.google.android.dialer/com.google.android.dialer.extensions.GoogleDialtactsActivity#0) uid=1000
  Region TransparentRegion (this=0 count=0)
  Region VisibleRegion (this=0 count=0)
  Region SurfaceDamageRegion (this=0 count=0)
      layerStack=   0, z=        0, pos=(0,0), size=(  -1,  -1), crop=[  0,   0,  -1,  -1], cornerRadius=0.000000, isProtected=0, isTrustedOverlay=0, isOpaque=0, invalidate=1, dataspace=Default, defaultPixelFormat=Unknown/None, backgroundBlurRadius=0, color=(0.000,0.000,0.000,1.000), flags=0x00000000, tr=[0.00, 0.00][0.00, 0.00]
      parent=ActivityRecord{bf3fd2d u0 com.google.android.dialer/.extensions.GoogleDialtactsActivity#0
      zOrderRelativeOf=none
      activeBuffer=[   0x   0:   0,Unknown/None], tr=[0.00, 0.00][0.00, 0.00] queued-frames=0, mRefreshPending=0, metadata={}, cornerRadiusCrop=[0.00, 0.00, 0.00, 0.00],  shadowRadius=0.000, }
```

# Surface创建flag

## ISurfaceComposerClient flags
ISurfaceComposerClient.h 中定义的创建Surface的flag如下，它与SurfaceControl.java中的定义对应
```c
/// @frameworks/native/libs/gui/include/gui/ISurfaceComposerClient.h
// flags for createSurface()
enum { // (keep in sync with SurfaceControl.java)
    eHidden = 0x00000004,
    eDestroyBackbuffer = 0x00000020,
    eSkipScreenshot = 0x00000040,
    eSecure = 0x00000080,
    eNonPremultiplied = 0x00000100,
    eOpaque = 0x00000400,
    eProtectedByApp = 0x00000800,
    eProtectedByDRM = 0x00001000,
    eCursorWindow = 0x00002000,
    eNoColorFill = 0x00004000,

    eFXSurfaceBufferQueue = 0x00000000,
    eFXSurfaceEffect = 0x00020000,
    eFXSurfaceBufferState = 0x00040000,
    eFXSurfaceContainer = 0x00080000,
    eFXSurfaceMask = 0x000F0000,
};
```

## SurfaceControl中定义的创建Surface的flags
```java
/// @frameworks/base/core/java/android/view/SurfaceControl.java
/* flags used in constructor (keep in sync with ISurfaceComposerClient.h) */

/**
 * Surface creation flag: Surface is created hidden
 * @hide
 */
@UnsupportedAppUsage(maxTargetSdk = Build.VERSION_CODES.R, trackingBug = 170729553)
public static final int HIDDEN = 0x00000004;

/**
 * Surface creation flag: Skip this layer and its children when taking a screenshot. This
 * also includes mirroring and screen recording, so the layers with flag SKIP_SCREENSHOT
 * will not be included on non primary displays.
 * @hide
 */
public static final int SKIP_SCREENSHOT = 0x00000040;

/**
 * Surface creation flag: Special measures will be taken to disallow the surface's content to
 * be copied. In particular, screenshots and secondary, non-secure displays will render black
 * content instead of the surface content.
 *
 * @see #createDisplay(String, boolean)
 * @hide
 */
public static final int SECURE = 0x00000080;


/**
 * Queue up BufferStateLayer buffers instead of dropping the oldest buffer when this flag is
 * set. This blocks the client until all the buffers have been presented. If the buffers
 * have presentation timestamps, then we may drop buffers.
 * @hide
 */
public static final int ENABLE_BACKPRESSURE = 0x00000100;

/**
 * Surface creation flag: Creates a surface where color components are interpreted
 * as "non pre-multiplied" by their alpha channel. Of course this flag is
 * meaningless for surfaces without an alpha channel. By default
 * surfaces are pre-multiplied, which means that each color component is
 * already multiplied by its alpha value. In this case the blending
 * equation used is:
 * <p>
 *    <code>DEST = SRC + DEST * (1-SRC_ALPHA)</code>
 * <p>
 * By contrast, non pre-multiplied surfaces use the following equation:
 * <p>
 *    <code>DEST = SRC * SRC_ALPHA * DEST * (1-SRC_ALPHA)</code>
 * <p>
 * pre-multiplied surfaces must always be used if transparent pixels are
 * composited on top of each-other into the surface. A pre-multiplied
 * surface can never lower the value of the alpha component of a given
 * pixel.
 * <p>
 * In some rare situations, a non pre-multiplied surface is preferable.
 * @hide
 */
public static final int NON_PREMULTIPLIED = 0x00000100;

/**
 * Surface creation flag: Indicates that the surface must be considered opaque,
 * even if its pixel format contains an alpha channel. This can be useful if an
 * application needs full RGBA 8888 support for instance but will
 * still draw every pixel opaque.
 * <p>
 * This flag is ignored if setAlpha() is used to make the surface non-opaque.
 * Combined effects are (assuming a buffer format with an alpha channel):
 * <ul>
 * <li>OPAQUE + alpha(1.0) == opaque composition
 * <li>OPAQUE + alpha(0.x) == blended composition
 * <li>!OPAQUE + alpha(1.0) == blended composition
 * <li>!OPAQUE + alpha(0.x) == blended composition
 * </ul>
 * If the underlying buffer lacks an alpha channel, the OPAQUE flag is effectively
 * set automatically.
 * @hide
 */
public static final int OPAQUE = 0x00000400;

/**
 * Surface creation flag: Application requires a hardware-protected path to an
 * external display sink. If a hardware-protected path is not available,
 * then this surface will not be displayed on the external sink.
 *
 * @hide
 */
public static final int PROTECTED_APP = 0x00000800;

// 0x1000 is reserved for an independent DRM protected flag in framework

/**
 * Surface creation flag: Window represents a cursor glyph.
 * @hide
 */
public static final int CURSOR_WINDOW = 0x00002000;

/**
 * Surface creation flag: Indicates the effect layer will not have a color fill on
 * creation.
 *
 * @hide
 */
public static final int NO_COLOR_FILL = 0x00004000;

/**
 * Surface creation flag: Creates a normal surface.
 * This is the default.
 *
 * @hide
 */
public static final int FX_SURFACE_NORMAL   = 0x00000000;

/**
 * Surface creation flag: Creates a effect surface which
 * represents a solid color and or shadows.
 *
 * @hide
 */
public static final int FX_SURFACE_EFFECT = 0x00020000;

/**
 * Surface creation flag: Creates a container surface.
 * This surface will have no buffers and will only be used
 * as a container for other surfaces, or for its InputInfo.
 * @hide
 */
public static final int FX_SURFACE_CONTAINER = 0x00080000;

/**
 * @hide
 */
public static final int FX_SURFACE_BLAST = 0x00040000;

/**
 * Mask used for FX values above.
 *
 * @hide
 */
public static final int FX_SURFACE_MASK = 0x000F0000;
```

## 创建layer的flag
- FX_SURFACE_NORMAL、FX_SURFACE_BLAST
  - 对应sf 的 eFXSurfaceBufferQueue、eFXSurfaceBufferState，目前都是创建 BufferStateLayer
- FX_SURFACE_EFFECT
  - 对应sf 的 eFXSurfaceEffect，创建 EffectLayer
- FX_SURFACE_CONTAINER
  - 对应sf 的 eFXSurfaceContainer，创建 ContainerLayer
- SKIP_SCREENSHOT
  - 截图时不截取当前layer及其子layer
- SECURE
  - 截图，副屏及不安全的显示屏不显示该layer内容
