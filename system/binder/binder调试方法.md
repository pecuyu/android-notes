# 通过mask控制binger debug log输出
## Binder debug mask 条目:
```c
/// kernel_common/drivers/android/binder.c
enum {
	BINDER_DEBUG_USER_ERROR             = 1U << 0, // 1
	BINDER_DEBUG_FAILED_TRANSACTION     = 1U << 1, // 2
	BINDER_DEBUG_DEAD_TRANSACTION       = 1U << 2, // 4
	BINDER_DEBUG_OPEN_CLOSE             = 1U << 3, // 8
	BINDER_DEBUG_DEAD_BINDER            = 1U << 4, // 16
	BINDER_DEBUG_DEATH_NOTIFICATION     = 1U << 5, // 32
	BINDER_DEBUG_READ_WRITE             = 1U << 6, // 64
	BINDER_DEBUG_USER_REFS              = 1U << 7, // 128
	BINDER_DEBUG_THREADS                = 1U << 8, // 216
	BINDER_DEBUG_TRANSACTION            = 1U << 9, // 512
	BINDER_DEBUG_TRANSACTION_COMPLETE   = 1U << 10,// 1024
	BINDER_DEBUG_FREE_BUFFER            = 1U << 11,// 2048
	BINDER_DEBUG_INTERNAL_REFS          = 1U << 12,// 4096
	BINDER_DEBUG_PRIORITY_CAP           = 1U << 13,// 8192
	BINDER_DEBUG_SPINLOCKS              = 1U << 14,// 16384
};

// 默认debug mask 7:  1+2+4
static uint32_t binder_debug_mask = BINDER_DEBUG_USER_ERROR | BINDER_DEBUG_FAILED_TRANSACTION | BINDER_DEBUG_DEAD_TRANSACTION;


// 通过给的mask和binder_debug_mask进行&操作，来判断是否输出
#define binder_debug(mask, x...) \
	do { \
		if (binder_debug_mask & mask) \
			pr_info_ratelimited(x); \
	} while (0)

#define binder_user_error(x...) \
	do { \
		if (binder_debug_mask & BINDER_DEBUG_USER_ERROR) \
			pr_info_ratelimited(x); \
		if (binder_stop_on_user_error) \
			binder_stop_on_user_error = 2; \
	} while (0)
```

## 修改 debug mask 值
```c
Binder debug mask 文件路径:
/sys/module/binder/parameters/debug_mask
~
通过echo写入新mask：
# echo 31 >  /sys/module/binder/parameters/debug_mask
```

## 输出效果
```c
2022-10-28 20:58:49.326 0-0/? I/binder: binder_open: 1225:3314
2022-10-28 20:58:49.326 0-0/? I/binder: binder_mmap: 1225 bd7e2000-bd8e0000 (1016 K) vma 200031 pagep 8000000000000025
2022-10-28 20:59:56.172 0-0/? I/binder: 2970 close vm area be2f5000-be3f3000 (1016 K) vma 10220011 pagep 8000000000000025
2022-10-28 20:59:56.175 0-0/? I/binder: binder_flush: 2970 woke 0 threads
2022-10-28 20:59:56.176 0-0/? I/binder: binder_flush: 2970 woke 0 threads
2022-10-28 20:59:56.177 0-0/? I/binder: node 199497 now dead, refs 1, death 0
2022-10-28 20:59:56.183 0-0/? I/binder: 2970 delete ref 112486 desc 3 has death notification
2022-10-28 20:59:56.183 0-0/? I/binder: binder_deferred_release: 2970 threads 7, nodes 11 (ref 11), refs 16, active transactions 0
2022-10-28 20:59:56.184 0-0/? I/binder: 2970 delete ref 111961 desc 24 has death notification
2022-10-28 20:59:56.184 0-0/? I/binder: 520:1494 BC_DEAD_BINDER_DONE 00000000edbba690 found 0000000000000000
2022-10-28 20:59:56.186 0-0/? I/binder: 2970 delete ref 112060 desc 29 has death notification
2022-10-28 20:59:56.195 0-0/? I/binder: undelivered transaction 204166, process died.
2022-10-28 20:59:56.195 0-0/? I/binder: binder_deferred_release: 2970 threads 19, nodes 40 (ref 47), refs 57, active transactions 0
```


# 通过mask控制binger alloc debug log输出
## Binder alloc debug mask 条目
```c
/// @kernel_common/drivers/android/binder_alloc.c
enum {
	BINDER_DEBUG_USER_ERROR             = 1U << 0, // 1
	BINDER_DEBUG_OPEN_CLOSE             = 1U << 1, // 2
	BINDER_DEBUG_BUFFER_ALLOC           = 1U << 2, // 4
	BINDER_DEBUG_BUFFER_ALLOC_ASYNC     = 1U << 3, // 8
};

// 默认值1
static uint32_t binder_alloc_debug_mask = BINDER_DEBUG_USER_ERROR;

// 通过给的mask和binder_alloc_debug_mask进行&操作，来判断是否输出
#define binder_alloc_debug(mask, x...) \
	do { \
		if (binder_alloc_debug_mask & mask) \
			pr_info_ratelimited(x); \
	} while (0)
```

## 修改 alloc debug mask值
```c
Binder alloc debug mask 文件路径:
/sys/module/binder/parameters/debug_mask
~
通过echo写入新mask：
# echo 7 >  /sys/module/binder_alloc/parameters/debug_mask
```

## 输出效果
```c
2022-10-28 20:54:33.869 0-0/? I/binder_alloc: 520: binder_alloc_buf size 112 got buffer 0000000000000000 size 4608
2022-10-28 20:54:33.869 0-0/? I/binder_alloc: 520: binder_alloc_buf size 112 got 0000000000000000
2022-10-28 20:54:33.870 0-0/? I/binder_alloc: 936: binder_alloc_buf size 16 got buffer 0000000000000000 size 1040376
2022-10-28 20:54:33.870 0-0/? I/binder_alloc: 936: binder_alloc_buf size 16 got 0000000000000000
2022-10-28 20:54:33.871 0-0/? I/binder_alloc: 520: binder_free_buf 0000000000000000 size 112 buffer_size 112
2022-10-28 20:54:33.871 0-0/? I/binder_alloc: 936: binder_free_buf 0000000000000000 size 16 buffer_size 16
2022-10-28 20:54:33.871 0-0/? I/binder_alloc: 520: merge free, buffer 0000000000000000 share page with 0000000000000000
2022-10-28 20:54:33.872 0-0/? I/binder_alloc: 936: merge free, buffer 0000000000000000 share page with 0000000000000000
```
