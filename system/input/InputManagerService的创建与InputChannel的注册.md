## Java层InputManagerService的创建
在SystemServer启动的时候，创建了InputManagerService，同时建立了与WindowManagerService的联系，为以后的input事件分发做准备：

frameworks/base/services/java/com/android/server/SystemServer.java
SystemServer.java --> startOtherServices()

    /**
     * Starts a miscellaneous grab bag of stuff that has yet to be refactored
     * and organized.
     */
    private void startOtherServices() {
     ...
            inputManager = new InputManagerService(context); // init
            // 通过参数，与WMS建立联系
            wm = WindowManagerService.main(context, inputManager,
                    mFactoryTestMode != FactoryTest.FACTORY_TEST_LOW_LEVEL,
                    !mFirstBoot, mOnlyCore);
            ServiceManager.addService(Context.WINDOW_SERVICE, wm);
            ServiceManager.addService(Context.INPUT_SERVICE, inputManager);
            
          ...

            mActivityManagerService.setWindowManager(wm);
			// 设置callback回调，用来处理事件的派发策略等，最终交由PhoneWindowManager处理
            inputManager.setWindowManagerCallbacks(wm.getInputMonitor());
            inputManager.start();  // start
     ...
    }
    
frameworks/base/services/core/java/com/android/server/input/InputManagerService.java
InputManagerService构造方法如下：

    public InputManagerService(Context context) {
        this.mContext = context;
		// 创建InputManagerHandler
        this.mHandler = new InputManagerHandler(DisplayThread.get().getLooper());

        mUseDevInputEventForAudioJack =
                context.getResources().getBoolean(R.bool.config_useDevInputEventForAudioJack);

        // 调用底层的init函数nativeInit，传递了一个消息队列，保存底层NativeInputManager的地址到mPtr
        mPtr = nativeInit(this, mContext, mHandler.getLooper().getQueue());

        String doubleTouchGestureEnablePath = context.getResources().getString(
                R.string.config_doubleTouchGestureEnableFile);
        mDoubleTouchGestureEnableFile = TextUtils.isEmpty(doubleTouchGestureEnablePath) ? null :
            new File(doubleTouchGestureEnablePath);
        // 添加local service
        LocalServices.addService(InputManagerInternal.class, new LocalService());
    }

## NativeInputManager

nativeInit方法的真正实现在com_android_server_input_InputManagerService类，对应着同名方法。相关JNI函数的注册信息如下，在onload.cpp中完成注册 ：

    static const JNINativeMethod gInputManagerMethods[] = {
    /* name, signature, funcPtr */
    { "nativeInit",
        "(Lcom/android/server/input/InputManagerService;Landroid/content/Context;Landroid/os/MessageQueue;)J",  (void*) nativeInit },
    { "nativeStart", "(J)V",  (void*) nativeStart },
    ...
    }


nativeInit方法创建了JNI层的NativeInputManager：
frameworks/base/services/core/jni/com_android_server_input_InputManagerService.cpp

com_android_server_input_InputManagerService.cpp --> nativeInit(...)

    static jlong nativeInit(JNIEnv* env, jclass /* clazz */,
        jobject serviceObj, jobject contextObj, jobject messageQueueObj) {
        sp<MessageQueue> messageQueue = android_os_MessageQueue_getMessageQueue(env, messageQueueObj);
        if (messageQueue == NULL) {
            jniThrowRuntimeException(env, "MessageQueue is not initialized.");
            return 0;
        }
    
        // 创建NativeInputManager ，serviceObj 指向Java层的IMS，contextObj 是指向IMS持有的Context
        NativeInputManager* im = new NativeInputManager(contextObj, serviceObj,
                messageQueue->getLooper());
        im->incStrong(0);
        return reinterpret_cast<jlong>(im);  // 返回NativeInputManager地址
    }


创建 NativeInputManager , 它是在com_android_server_input_InputManagerService中定义的：

    NativeInputManager::NativeInputManager(jobject contextObj,
        jobject serviceObj, const sp<Looper>& looper) :
        mLooper(looper), mInteractive(true) {
        JNIEnv* env = jniEnv();
    	// 全局引用
        mContextObj = env->NewGlobalRef(contextObj); // 持有Context引用
        mServiceObj = env->NewGlobalRef(serviceObj); // 持有IMS的引用
    
        {
            AutoMutex _l(mLock);
            mLocked.systemUiVisibility = ASYSTEM_UI_VISIBILITY_STATUS_BAR_VISIBLE;
            mLocked.pointerSpeed = 0;
            mLocked.pointerGesturesEnabled = true;
            mLocked.showTouches = false;
        }
        mInteractive = true;
    
        sp<EventHub> eventHub = new EventHub(); // 创建EventHub，用于获取与处理原始事件
         // 创建InputManager ， this参数分别作为readerPolicy 和 dispatcherPolicy
        mInputManager = new InputManager(eventHub, this, this);
    }

## 创建native层的InputManager
创建 InputManager时，需要传递EventHub为参数，readerPolicy和dispatcherPolicy都是NativeInputManager对象。在该构造方法中，会创建InputDispatcher和InputReader，在构造InputReader时建立两者的联系，policy参数都是NativeInputManager对象：
frameworks/native/services/inputflinger/InputManager.cpp
InputManager构造方法：

    InputManager::InputManager(
        const sp<EventHubInterface>& eventHub,
        const sp<InputReaderPolicyInterface>& readerPolicy,
        const sp<InputDispatcherPolicyInterface>& dispatcherPolicy) {
        // 创建InputDispatcher
        mDispatcher = new InputDispatcher(dispatcherPolicy);  // 处理Event分发
        // 创建InputReader，关联eventHub，InputDispatcher
        mReader = new InputReader(eventHub, readerPolicy, mDispatcher);  // 处理读取input事件
        initialize();
    }


调用 InputManager::initialize（），该方法会创建读写线程，分别与mReader和mDispatcher进行关联

    void InputManager::initialize() {
        // 创建InputReaderThread ，Event读线程
            mReaderThread = new InputReaderThread(mReader);
        // 创建InputDispatcherThread ， Event分发线程
            mDispatcherThread = new InputDispatcherThread(mDispatcher);
    }

## IMS#start

回到 SystemServer.java --> startOtherServices()
最后会调用inputManager.start(); 也就是调用InputManagerService.java -->  start() ，这个方法会首先调用nativeStart方法让底层启动运行，接着会注册相关的监听：

     public void start() {
        Slog.i(TAG, "Starting input manager");
        nativeStart(mPtr); // native启动

        // Add ourself to the Watchdog monitors.
        Watchdog.getInstance().addMonitor(this);

        registerPointerSpeedSettingObserver();
        registerShowTouchesSettingObserver();
        registerAccessibilityLargePointerSettingObserver();

        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updatePointerSpeedFromSettings();
                updateShowTouchesFromSettings();
                updateAccessibilityLargePointerFromSettings();
            }
        }, new IntentFilter(Intent.ACTION_USER_SWITCHED), null, mHandler);

        updatePointerSpeedFromSettings();
        updateShowTouchesFromSettings();
        updateAccessibilityLargePointerFromSettings();
    }


调用nativeStart方法：

    static void nativeStart(JNIEnv* env, jclass /* clazz */, jlong ptr) {
        NativeInputManager* im = reinterpret_cast<NativeInputManager*>(ptr);
    	//im->getInputManager()返回 mInputManager
        status_t result = im->getInputManager()->start();
        if (result) {
            jniThrowRuntimeException(env, "Input manager could not be started.");
        }
    }

接着会调用InputManager的start方法，开启底层线程

    status_t InputManager::start() {
		// 启动mDispatcherThread 线程
        status_t result = mDispatcherThread->run("InputDispatcher", PRIORITY_URGENT_DISPLAY);
        if (result) {
            ALOGE("Could not start InputDispatcher thread due to error %d.", result);
            return result;
        }
    	// 启动mReaderThread 线程
        result = mReaderThread->run("InputReader", PRIORITY_URGENT_DISPLAY);
        if (result) {
            ALOGE("Could not start InputReader thread due to error %d.", result);
    
            mDispatcherThread->requestExit();
            return result;
        }
    
        return OK;
    }


read线程执行：

    bool InputReaderThread::threadLoop() {
        mReader->loopOnce();
        return true;  // 返回true，会不停调用threadLoop方法
    }

## InputReader::loopOnce

在线程循环中调用InputReader::loopOnce() ，这个方法首先通过EventHub的getEvents获取事件，之后对事件进行加工处理，最后会将事件传递给InputDispatcher来分发：

    void InputReader::loopOnce() {
    ...

        // 从EventHub获取事件
        size_t count = mEventHub->getEvents(timeoutMillis, mEventBuffer, EVENT_BUFFER_SIZE);

        { // acquire lock
            AutoMutex _l(mLock);
            mReaderIsAliveCondition.broadcast();
    
            if (count) {
                processEventsLocked(mEventBuffer, count); // 对事件进行加工处理
            }
    
            if (mNextTimeout != LLONG_MAX) {
                nsecs_t now = systemTime(SYSTEM_TIME_MONOTONIC);
                if (now >= mNextTimeout) {
        
                    mNextTimeout = LLONG_MAX;
                    timeoutExpiredLocked(now);
                }
            }
    
            if (oldGeneration != mGeneration) {
                inputDevicesChanged = true;
                getInputDevicesLocked(inputDevices);
            }
        } // release lock

        // Send out a message that the describes the changed input devices.
        if (inputDevicesChanged) {
            mPolicy->notifyInputDevicesChanged(inputDevices);
        }
    
        // Flush queued events out to the listener.
        // This must happen outside of the lock because the listener could potentially call
        // back into the InputReader's methods, such as getScanCodeState, or become blocked
        // on another thread similarly waiting to acquire the InputReader lock thereby
        // resulting in a deadlock.  This situation is actually quite plausible because the
        // listener is actually the input dispatcher, which calls into the window manager,
        // which occasionally calls into the input reader.
        // 将事件传递给Dispatcher ，mQueuedListener为对InputDispatcher引用的包装
        mQueuedListener->flush();  
    
    }

processEventsLocked 处理事件：

	void InputReader::processEventsLocked(const RawEvent* rawEvents, size_t count) {
	    for (const RawEvent* rawEvent = rawEvents; count;) {
	        int32_t type = rawEvent->type;
	        size_t batchSize = 1;
	        if (type < EventHubInterface::FIRST_SYNTHETIC_EVENT) {
	            int32_t deviceId = rawEvent->deviceId;
	            while (batchSize < count) {
	                if (rawEvent[batchSize].type >= EventHubInterface::FIRST_SYNTHETIC_EVENT
	                        || rawEvent[batchSize].deviceId != deviceId) {
	                    break;
	                }
	                batchSize += 1;
	            processEventsForDeviceLocked(deviceId, rawEvent, batchSize); // 处理某个设备事件
	        } else {
	            switch (rawEvent->type) {
	            case EventHubInterface::DEVICE_ADDED: // 添加设备
	                addDeviceLocked(rawEvent->when, rawEvent->deviceId);
	                break;
	            case EventHubInterface::DEVICE_REMOVED:  // 移除设备
	                removeDeviceLocked(rawEvent->when, rawEvent->deviceId);
	                break;
	            case EventHubInterface::FINISHED_DEVICE_SCAN:  // 处理配置改变
	                handleConfigurationChangedLocked(rawEvent->when);
	                break;
	            default:
	                ALOG_ASSERT(false); // can't happen
	                break;
	            }
	        }
	        count -= batchSize;
	        rawEvent += batchSize;
	    }
	}


接下来调用QueuedInputListener的flush方法
    
    void QueuedInputListener::flush() {
        size_t count = mArgsQueue.size();
        for (size_t i = 0; i < count; i++) {
            NotifyArgs* args = mArgsQueue[i];
            args->notify(mInnerListener); // mInnerListener即InputDispatcher引用
            delete args;
        }
        mArgsQueue.clear();
    }

对motion事件，会调用NotifyMotionArgs::notify：

    void NotifyMotionArgs::notify(const sp<InputListenerInterface>& listener) const {
        listener->notifyMotion(this);
    }

## InputDispatcher::notifyMotion

listener即InputDispatcher引用，会调用InputDispatcher的notifyMotion方法，将事件交给InputDispatcher处理：

    void InputDispatcher::notifyMotion(const NotifyMotionArgs* args) {
    #if DEBUG_INBOUND_EVENT_DETAILS
   		...  // debug code
    #endif
        if (!validateMotionEvent(args->action, args->actionButton,
                    args->pointerCount, args->pointerProperties)) { // 无效事件
            return;
        }
    
        uint32_t policyFlags = args->policyFlags;
        policyFlags |= POLICY_FLAG_TRUSTED;
		// 策略对事件的拦截，这个策略由NativeInputManager实现
        mPolicy->interceptMotionBeforeQueueing(args->eventTime, /*byref*/ policyFlags); 
    
        bool needWake;
        { // acquire lock
            mLock.lock();
            // 过滤事件
            if (shouldSendMotionToInputFilterLocked(args)) {
                mLock.unlock();
    
                MotionEvent event;
                event.initialize(args->deviceId, args->source, args->action, args->actionButton,
                        args->flags, args->edgeFlags, args->metaState, args->buttonState,
                        0, 0, args->xPrecision, args->yPrecision,
                        args->downTime, args->eventTime,
                        args->pointerCount, args->pointerProperties, args->pointerCoords);
    
                policyFlags |= POLICY_FLAG_FILTERED;
                if (!mPolicy->filterInputEvent(&event, policyFlags)) {
                    return; // event was consumed by the filter
                }
    
                mLock.lock();
            }
    
            // Just enqueue a new motion event.
            MotionEntry* newEntry = new MotionEntry(args->eventTime,
                    args->deviceId, args->source, policyFlags,
                    args->action, args->actionButton, args->flags,
                    args->metaState, args->buttonState,
                    args->edgeFlags, args->xPrecision, args->yPrecision, args->downTime,
                    args->displayId,
                    args->pointerCount, args->pointerProperties, args->pointerCoords, 0, 0);
    		// 消息入队
            needWake = enqueueInboundEventLocked(newEntry);
            mLock.unlock();
        } // release lock
    
        if (needWake) { // 当有消息入队时需要唤醒
            mLooper->wake(); // 唤醒线程分发
        }
    }


enqueueInboundEventLocked事件入队：

    bool InputDispatcher::enqueueInboundEventLocked(EventEntry* entry) {
        bool needWake = mInboundQueue.isEmpty();
        mInboundQueue.enqueueAtTail(entry);
    
        switch (entry->type) {
        case EventEntry::TYPE_KEY: {
            // Optimize app switch latency.
            // If the application takes too long to catch up then we drop all events preceding
            // the app switch key.
            KeyEntry* keyEntry = static_cast<KeyEntry*>(entry);
            if (isAppSwitchKeyEventLocked(keyEntry)) {
                if (keyEntry->action == AKEY_EVENT_ACTION_DOWN) {
                    mAppSwitchSawKeyDown = true;
                } else if (keyEntry->action == AKEY_EVENT_ACTION_UP) {
                    if (mAppSwitchSawKeyDown) {
                        mAppSwitchDueTime = keyEntry->eventTime + APP_SWITCH_TIMEOUT;
                        mAppSwitchSawKeyDown = false;
                        needWake = true;
                    }
                }
            }
            break;
        }
    
        case EventEntry::TYPE_MOTION: {
            // Optimize case where the current application is unresponsive and the user
            // decides to touch a window in a different application.
            // If the application takes too long to catch up then we drop all events preceding
            // the touch into the other window.
            MotionEntry* motionEntry = static_cast<MotionEntry*>(entry);
            if (motionEntry->action == AMOTION_EVENT_ACTION_DOWN
                    && (motionEntry->source & AINPUT_SOURCE_CLASS_POINTER)
                    && mInputTargetWaitCause == INPUT_TARGET_WAIT_CAUSE_APPLICATION_NOT_READY
                    && mInputTargetWaitApplicationHandle != NULL) {
                int32_t displayId = motionEntry->displayId;
                int32_t x = int32_t(motionEntry->pointerCoords[0].
                        getAxisValue(AMOTION_EVENT_AXIS_X));
                int32_t y = int32_t(motionEntry->pointerCoords[0].
                        getAxisValue(AMOTION_EVENT_AXIS_Y));
				// 查找合适的窗口
                sp<InputWindowHandle> touchedWindowHandle = findTouchedWindowAtLocked(displayId, x, y);
                if (touchedWindowHandle != NULL
                        && touchedWindowHandle->inputApplicationHandle
                                != mInputTargetWaitApplicationHandle) {
                    // User touched a different application than the one we are waiting on.
                    // Flag the event, and start pruning the input queue.
                    mNextUnblockedEvent = motionEntry;
                    needWake = true;
                }
            }
            break;
        }
        }
    
        return needWake;
    }


## InputDispatcher::dispatchOnce

当dispatch线程唤醒后执行threadLoop方法：

    bool InputDispatcherThread::threadLoop() {
        mDispatcher->dispatchOnce();
        return true;
    }

接着调用dispatchOnce

    void InputDispatcher::dispatchOnce() {
        nsecs_t nextWakeupTime = LONG_LONG_MAX;
        { // acquire lock
            AutoMutex _l(mLock);
            mDispatcherIsAliveCondition.broadcast();
    
            // Run a dispatch loop if there are no pending commands.
            // The dispatch loop might enqueue commands to run afterwards.
            if (!haveCommandsLocked()) {
                dispatchOnceInnerLocked(&nextWakeupTime);
            }
    
            // Run all pending commands if there are any.
            // If any commands were run then force the next poll to wake up immediately.
            if (runCommandsLockedInterruptible()) {
                nextWakeupTime = LONG_LONG_MIN;
            }
        } // release lock
    
        // Wait for callback or timeout or wake.  (make sure we round up, not down)
        nsecs_t currentTime = now();
        int timeoutMillis = toMillisecondTimeoutDelay(currentTime, nextWakeupTime);
        mLooper->pollOnce(timeoutMillis);
    }


调用dispatchOnceInnerLocked

	void InputDispatcher::dispatchOnceInnerLocked(nsecs_t* nextWakeupTime) {
    nsecs_t currentTime = now();
		...	
	
	    // Ready to start a new event.
	    // If we don't already have a pending event, go grab one.
	    if (! mPendingEvent) {
	        if (mInboundQueue.isEmpty()) {
	            if (isAppSwitchDue) {
	                // The inbound queue is empty so the app switch key we were waiting
	                // for will never arrive.  Stop waiting for it.
	                resetPendingAppSwitchLocked(false);
	                isAppSwitchDue = false;
	            }
	
	            // Synthesize a key repeat if appropriate.
	            if (mKeyRepeatState.lastKeyEntry) {
	                if (currentTime >= mKeyRepeatState.nextRepeatTime) {
	                    mPendingEvent = synthesizeKeyRepeatLocked(currentTime);
	                } else {
	                    if (mKeyRepeatState.nextRepeatTime < *nextWakeupTime) {
	                        *nextWakeupTime = mKeyRepeatState.nextRepeatTime;
	                    }
	                }
	            }
	
	            // Nothing to do if there is no pending event.
	            if (!mPendingEvent) {
	                return;
	            }
	        } else {
	            // Inbound queue has at least one entry.
	            mPendingEvent = mInboundQueue.dequeueAtHead();
	        }
	
	        // Poke user activity for this event.
	        if (mPendingEvent->policyFlags & POLICY_FLAG_PASS_TO_USER) {
	            pokeUserActivityLocked(mPendingEvent);
	        }
	
	        // Get ready to dispatch the event.
	        resetANRTimeoutsLocked();
	    }
	
	    // Now we have an event to dispatch.
	    // All events are eventually dequeued and processed this way, even if we intend to drop them.
	    ALOG_ASSERT(mPendingEvent != NULL);
	    bool done = false;
	    ...
	
	    if (mNextUnblockedEvent == mPendingEvent) {
	        mNextUnblockedEvent = NULL;
	    }
	
	    switch (mPendingEvent->type) {
		    case EventEntry::TYPE_CONFIGURATION_CHANGED: {...} break;
		    case EventEntry::TYPE_KEY: {...} break;
		
		    case EventEntry::TYPE_MOTION: {  // 对于MOTION事件
		        MotionEntry* typedEntry = static_cast<MotionEntry*>(mPendingEvent);
		        if (dropReason == DROP_REASON_NOT_DROPPED && isAppSwitchDue) {
		            dropReason = DROP_REASON_APP_SWITCH;
		        }
		        if (dropReason == DROP_REASON_NOT_DROPPED && isStaleEventLocked(currentTime, typedEntry)) {
		            dropReason = DROP_REASON_STALE;
		        }
		        if (dropReason == DROP_REASON_NOT_DROPPED && mNextUnblockedEvent) {
		            dropReason = DROP_REASON_BLOCKED;
		        }
		        done = dispatchMotionLocked(currentTime, typedEntry,
		                &dropReason, nextWakeupTime);
		        break;
		    }
		
		    default:...
	    }
	
	    if (done) {
	        if (dropReason != DROP_REASON_NOT_DROPPED) {
	            dropInboundEventLocked(mPendingEvent, dropReason);
	        }
	        mLastDropReason = dropReason;
	
	        releasePendingEventLocked();
	        *nextWakeupTime = LONG_LONG_MIN;  // force next poll to wake up immediately
	    }
	}
	

## dispatch event 

对于MOTION事件，接下来调用dispatchMotionLocked来处理：
	
	bool InputDispatcher::dispatchMotionLocked(
	        nsecs_t currentTime, MotionEntry* entry, DropReason* dropReason, nsecs_t* nextWakeupTime) {
	    // Preprocessing...
	
	    // Clean up if dropping the event...
	   
	    bool isPointerEvent = entry->source & AINPUT_SOURCE_CLASS_POINTER;
	
	    // Identify targets.
	    Vector<InputTarget> inputTargets;
	
	    bool conflictingPointerActions = false;
	    int32_t injectionResult;
	    if (isPointerEvent) {  // pointer event
	        // Pointer event.  (eg. touchscreen)
	        injectionResult = findTouchedWindowTargetsLocked(currentTime,
	                entry, inputTargets, nextWakeupTime, &conflictingPointerActions); // 找到合适目标窗口
	    } else {
	        // Non touch event.  (eg. trackball)
	        injectionResult = findFocusedWindowTargetsLocked(currentTime,
	                entry, inputTargets, nextWakeupTime);
	    }
	    if (injectionResult == INPUT_EVENT_INJECTION_PENDING) {
	        return false;
	    }
	
	    setInjectionResultLocked(entry, injectionResult);
	    ...
	
	    addMonitoringTargetsLocked(inputTargets); // 添加监听
	
	    // Dispatch the motion.
	    if (conflictingPointerActions) {
	        CancelationOptions options(CancelationOptions::CANCEL_POINTER_EVENTS,
	                "conflicting pointer actions");
	        synthesizeCancelationEventsForAllConnectionsLocked(options);
	    }
	    dispatchEventLocked(currentTime, entry, inputTargets);
	    return true;
	}

首先找到目标窗口，之后调用dispatchEventLocked来分发事件：

	void InputDispatcher::dispatchEventLocked(nsecs_t currentTime,
	        EventEntry* eventEntry, const Vector<InputTarget>& inputTargets) {
	    ALOG_ASSERT(eventEntry->dispatchInProgress); // should already have been set to true
	
	    pokeUserActivityLocked(eventEntry);
	
	    for (size_t i = 0; i < inputTargets.size(); i++) {
	        const InputTarget& inputTarget = inputTargets.itemAt(i);
	
	        ssize_t connectionIndex = getConnectionIndexLocked(inputTarget.inputChannel);
	        if (connectionIndex >= 0) { // 得到connection
	            sp<Connection> connection = mConnectionsByFd.valueAt(connectionIndex);
	            prepareDispatchCycleLocked(currentTime, connection, eventEntry, &inputTarget);
	        } else { ....
	        }
	    }
	}

### Publish event with collection

之后找到目标窗口的connection，然后调用prepareDispatchCycleLocked来进入分发：

    void InputDispatcher::prepareDispatchCycleLocked(nsecs_t currentTime,
            const sp<Connection>& connection, EventEntry* eventEntry, const InputTarget* inputTarget) {
        // Skip this event if the connection status is not normal.
        // We don't want to enqueue additional outbound events if the connection is broken.
        if (connection->status != Connection::STATUS_NORMAL) {
            return;
        }
    
        // Split a motion event if needed.
        if (inputTarget->flags & InputTarget::FLAG_SPLIT) {
            ALOG_ASSERT(eventEntry->type == EventEntry::TYPE_MOTION);
    
            MotionEntry* originalMotionEntry = static_cast<MotionEntry*>(eventEntry);
            if (inputTarget->pointerIds.count() != originalMotionEntry->pointerCount) {
                MotionEntry* splitMotionEntry = splitMotionEvent(
                        originalMotionEntry, inputTarget->pointerIds);
                if (!splitMotionEntry) {
                    return; // split event was dropped
                }
    
                enqueueDispatchEntriesLocked(currentTime, connection,
                        splitMotionEntry, inputTarget);
                splitMotionEntry->release();
                return;
            }
        }
    
        // Not splitting.  Enqueue dispatch entries for the event as is.
        enqueueDispatchEntriesLocked(currentTime, connection, eventEntry, inputTarget);
    }

处理事件拆分，并调用enqueueDispatchEntriesLocked将要分发的消息入队：

	void InputDispatcher::enqueueDispatchEntriesLocked(nsecs_t currentTime,
	        const sp<Connection>& connection, EventEntry* eventEntry, const InputTarget* inputTarget) {
	    bool wasEmpty = connection->outboundQueue.isEmpty();
	
	    // Enqueue dispatch entries for the requested modes.
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_HOVER_EXIT);
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_OUTSIDE);
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_HOVER_ENTER);
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_IS);
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_SLIPPERY_EXIT);
	    enqueueDispatchEntryLocked(connection, eventEntry, inputTarget,
	            InputTarget::FLAG_DISPATCH_AS_SLIPPERY_ENTER);
	
	    // If the outbound queue was previously empty, start the dispatch cycle going.
	    if (wasEmpty && !connection->outboundQueue.isEmpty()) {
	        startDispatchCycleLocked(currentTime, connection);
	    }
	}


最后调用startDispatchCycleLocked方法来分发事件


	void InputDispatcher::startDispatchCycleLocked(nsecs_t currentTime,
	        const sp<Connection>& connection) {

	    while (connection->status == Connection::STATUS_NORMAL
	            && !connection->outboundQueue.isEmpty()) {
	        DispatchEntry* dispatchEntry = connection->outboundQueue.head;
	        dispatchEntry->deliveryTime = currentTime;
	
	        // Publish the event.
	        status_t status;
	        EventEntry* eventEntry = dispatchEntry->eventEntry;
	        switch (eventEntry->type) {
	        case EventEntry::TYPE_KEY: { break;}
	        case EventEntry::TYPE_MOTION: {
	            ...
	
	            // Publish the motion event.  通过connection发布事件
	            status = connection->inputPublisher.publishMotionEvent(dispatchEntry->seq,
	                    motionEntry->deviceId, motionEntry->source, motionEntry->displayId,
	                    dispatchEntry->resolvedAction, motionEntry->actionButton,
	                    dispatchEntry->resolvedFlags, motionEntry->edgeFlags,
	                    motionEntry->metaState, motionEntry->buttonState,
	                    xOffset, yOffset, motionEntry->xPrecision, motionEntry->yPrecision,
	                    motionEntry->downTime, motionEntry->eventTime,
	                    motionEntry->pointerCount, motionEntry->pointerProperties,
	                    usingCoords);
	            break;
	        }
	
	        default:
	            ALOG_ASSERT(false);
	            return;
	        }
	
	        // Check the result.
	        if (status) {
	            ...
	            return;
	        }
	
	        // Re-enqueue the event on the wait queue.
	        connection->outboundQueue.dequeue(dispatchEntry); // 从待派发队列移除已经在派发的
	        connection->waitQueue.enqueueAtTail(dispatchEntry); // 将此派发事件添加到等待队列,等待客户端回复完成处理
	    }
	}
	

接下来调用connection->inputPublisher.publishMotionEvent 方法发布事件，inputPublisher即InputPublisher对象，即调用了InputPublisher的publishMotionEvent方法：


	status_t InputPublisher::publishKeyEvent(
	        uint32_t seq,
	        int32_t deviceId,
	        int32_t source,
	        int32_t action,
	        int32_t flags,
	        int32_t keyCode,
	        int32_t scanCode,
	        int32_t metaState,
	        int32_t repeatCount,
	        nsecs_t downTime,
	        nsecs_t eventTime) {
	
	    if (!seq) {
	        ALOGE("Attempted to publish a key event with sequence number 0.");
	        return BAD_VALUE;
	    }
	
	    InputMessage msg;
	    msg.header.type = InputMessage::TYPE_KEY;
	    msg.body.key.seq = seq;
	    msg.body.key.deviceId = deviceId;
	    msg.body.key.source = source;
	    msg.body.key.action = action;
	    msg.body.key.flags = flags;
	    msg.body.key.keyCode = keyCode;
	    msg.body.key.scanCode = scanCode;
	    msg.body.key.metaState = metaState;
	    msg.body.key.repeatCount = repeatCount;
	    msg.body.key.downTime = downTime;
	    msg.body.key.eventTime = eventTime;
	    return mChannel->sendMessage(&msg); // 通过Channel来发生消息
	}

接下来看InputChannel的sendMessage方法：

	status_t InputChannel::sendMessage(const InputMessage* msg) {
	    size_t msgLength = msg->size();
	    ssize_t nWrite;
	    do { // send a message on a socket
	        nWrite = ::send(mFd, msg, msgLength, MSG_DONTWAIT | MSG_NOSIGNAL);
	    } while (nWrite == -1 && errno == EINTR);
	
	    if (nWrite < 0) {
	        int error = errno;
	
	        if (error == EAGAIN || error == EWOULDBLOCK) {
	            return WOULD_BLOCK;
	        }
	        if (error == EPIPE || error == ENOTCONN || error == ECONNREFUSED || error == ECONNRESET) {
	            return DEAD_OBJECT;
	        }
	        return -error;
	    }
	
	    if (size_t(nWrite) != msgLength) {
	        return DEAD_OBJECT;
	    }
	
	    return OK;
	}


### NativeInputEventReceiver handleEvent

接着Looper唤醒并执行消息，会调用NativeInputEventReceiver的handleEvent函数


    int NativeInputEventReceiver::handleEvent(int receiveFd, int events, void* data) {
        ... 
        if (events & ALOOPER_EVENT_INPUT) {
            JNIEnv* env = AndroidRuntime::getJNIEnv();
            status_t status = consumeEvents(env, false /*consumeBatches*/, -1, NULL);
            mMessageQueue->raiseAndClearException(env, "handleReceiveCallback");
            return status == OK || status == NO_MEMORY ? 1 : 0;
        }
    
        ...
        return 1;
    }


接着调用consumeEvents来消耗事件：

    status_t NativeInputEventReceiver::consumeEvents(JNIEnv* env,
            bool consumeBatches, nsecs_t frameTime, bool* outConsumedBatch) {
    	...
        ScopedLocalRef<jobject> receiverObj(env, NULL);
        bool skipCallbacks = false;
        for (;;) {
            uint32_t seq;
            InputEvent* inputEvent;
            status_t status = mInputConsumer.consume(&mInputEventFactory,
                    consumeBatches, frameTime, &seq, &inputEvent);
            ...
    
            if (!skipCallbacks) {
                ...
    
                jobject inputEventObj;
                switch (inputEvent->getType()) {
                case AINPUT_EVENT_TYPE_KEY:   
                    inputEventObj = android_view_KeyEvent_fromNative(env,
                            static_cast<KeyEvent*>(inputEvent));
                    break;
    
                case AINPUT_EVENT_TYPE_MOTION: {              
                    MotionEvent* motionEvent = static_cast<MotionEvent*>(inputEvent);
                    if ((motionEvent->getAction() & AMOTION_EVENT_ACTION_MOVE) && outConsumedBatch) {
                        *outConsumedBatch = true;
                    }
                    inputEventObj = android_view_MotionEvent_obtainAsCopy(env, motionEvent);
                    break;
                }
    
                default:
                    assert(false); // InputConsumer should prevent this from ever happening
                    inputEventObj = NULL;
                }
    
                if (inputEventObj) {
                    env->CallVoidMethod(receiverObj.get(), //InputEventReceiver的dispatchInputEvent 方法
                            gInputEventReceiverClassInfo.dispatchInputEvent, seq, inputEventObj);
                    if (env->ExceptionCheck()) {
                        ALOGE("Exception dispatching input event.");
                        skipCallbacks = true;
                    }
                    env->DeleteLocalRef(inputEventObj);
                } else {
                    ALOGW("channel '%s' ~ Failed to obtain event object.", getInputChannelName());
                    skipCallbacks = true;
                }
            }
    
            if (skipCallbacks) {
                mInputConsumer.sendFinishedSignal(seq, false);
            }
        }
    }

最终通过CallVoidMethod回调Java层InputEventReceiver的dispatchInputEvent 方法，进行事件派发

    // Called from native code.
    @SuppressWarnings("unused")
    private void dispatchInputEvent(int seq, InputEvent event) {
        mSeqMap.put(event.getSequenceNumber(), seq);
        onInputEvent(event);
    }
    

## 注册InputChannel


### add Window

ViewRootImpl的setView来发起绘制与添加window


	/**
     * We have one child
     */
    public void setView(View view, WindowManager.LayoutParams attrs, View panelParentView) {
        synchronized (this) {
            if (mView == null) {
                mView = view;

                ...
                mAdded = true;
                int res; /* = WindowManagerImpl.ADD_OKAY; */

                // Schedule the first layout -before- adding to the window
                // manager, to make sure we do the relayout before receiving
                // any other events from the system.
                requestLayout();
                if ((mWindowAttributes.inputFeatures
                        & WindowManager.LayoutParams.INPUT_FEATURE_NO_INPUT_CHANNEL) == 0) {
                    mInputChannel = new InputChannel();  // 创建 InputChannel
                }
                mForceDecorViewVisibility = (mWindowAttributes.privateFlags
                        & PRIVATE_FLAG_FORCE_DECOR_VIEW_VISIBILITY) != 0;
                try {
                    mOrigWindowType = mWindowAttributes.type;
                    mAttachInfo.mRecomputeGlobalAttributes = true;
                    collectViewAttributes();
                    res = mWindowSession.addToDisplay(mWindow, mSeq, mWindowAttributes,
                            getHostVisibility(), mDisplay.getDisplayId(),
                            mAttachInfo.mContentInsets, mAttachInfo.mStableInsets,
                            mAttachInfo.mOutsets, mInputChannel);
                } catch (RemoteException e) {... }
                }

			...
			
			if (mInputChannel != null) {
                    if (mInputQueueCallback != null) {
                        mInputQueue = new InputQueue();
                        mInputQueueCallback.onInputQueueCreated(mInputQueue);
                    }
                    // 用于接收输入事件
                    mInputEventReceiver = new WindowInputEventReceiver(mInputChannel,
                            Looper.myLooper());
                }

                view.assignParent(this);
                ...
		}

WMS#addWindow：

    public int addWindow(Session session, IWindow client, int seq,
            WindowManager.LayoutParams attrs, int viewVisibility, int displayId,
            Rect outContentInsets, Rect outStableInsets, Rect outOutsets,
            InputChannel outInputChannel) {
        ...
        WindowState win = new WindowState(this, session, client, token,
                    attachedWindow, appOp[0], seq, attrs, viewVisibility, displayContent);
        ...
        
    	final boolean openInputChannels = (outInputChannel != null
                        && (attrs.inputFeatures & INPUT_FEATURE_NO_INPUT_CHANNEL) == 0);
        if  (openInputChannels) {
            win.openInputChannel(outInputChannel);
        }

        ...
    }

### create channel

调用WindowsState的openInputChannel创建一个Channel：

	void openInputChannel(InputChannel outInputChannel) {
        if (mInputChannel != null) {
            throw new IllegalStateException("Window already has an input channel.");
        }
        String name = getName();	// 创建 InputChannel pair
        InputChannel[] inputChannels = InputChannel.openInputChannelPair(name);
        mInputChannel = inputChannels[0];
        mClientChannel = inputChannels[1]; // 返回给客户端的
        mInputWindowHandle.inputChannel = inputChannels[0];
        if (outInputChannel != null) {
            mClientChannel.transferTo(outInputChannel);
            mClientChannel.dispose();
            mClientChannel = null;
        } else {
            // If the window died visible, we setup a dummy input channel, so that taps
            // can still detected by input monitor channel, and we can relaunch the app.
            // Create dummy event receiver that simply reports all events as handled.
            mDeadWindowEventReceiver = new DeadWindowEventReceiver(mClientChannel);
        }
        mService.mInputManager.registerInputChannel(mInputChannel, mInputWindowHandle);
    }
    
InputChannel的openInputChannelPair方法实际上调用了nativeOpenInputChannelPair方法来创建channel

    /**
     * Creates a new input channel pair.  One channel should be provided to the input
     * dispatcher and the other to the application's input queue.
     * @param name The descriptive (non-unique) name of the channel pair.
     * @return A pair of input channels.  The first channel is designated as the
     * server channel and should be used to publish input events.  The second channel
     * is designated as the client channel and should be used to consume input events.
     */
    public static InputChannel[] openInputChannelPair(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }

        if (DEBUG) {
            Slog.d(TAG, "Opening input channel pair '" + name + "'");
        }
        return nativeOpenInputChannelPair(name);
    }

通过android_view_InputChannel_nativeOpenInputChannelPair方法创建InputChannel pair


    static jobjectArray android_view_InputChannel_nativeOpenInputChannelPair(JNIEnv* env,
            jclass clazz, jstring nameObj) {
        const char* nameChars = env->GetStringUTFChars(nameObj, NULL);
        String8 name(nameChars);
        env->ReleaseStringUTFChars(nameObj, nameChars);
    
        sp<InputChannel> serverChannel;
        sp<InputChannel> clientChannel;
        status_t result = InputChannel::openInputChannelPair(name, serverChannel, clientChannel);
    
        //... check result
    
        jobjectArray channelPair = env->NewObjectArray(2, gInputChannelClassInfo.clazz, NULL);
        ...
    
        jobject serverChannelObj = android_view_InputChannel_createInputChannel(env,
                new NativeInputChannel(serverChannel));
        ...
    
        jobject clientChannelObj = android_view_InputChannel_createInputChannel(env,
                new NativeInputChannel(clientChannel));
        ...
    
        env->SetObjectArrayElement(channelPair, 0, serverChannelObj);
        env->SetObjectArrayElement(channelPair, 1, clientChannelObj);
        return channelPair;
    }

openInputChannelPair函数创建了socket

    status_t InputChannel::openInputChannelPair(const String8& name,
            sp<InputChannel>& outServerChannel, sp<InputChannel>& outClientChannel) {
        int sockets[2];
        if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, sockets)) {
            status_t result = -errno;
            ALOGE("channel '%s' ~ Could not create socket pair.  errno=%d",
                    name.string(), errno);
            outServerChannel.clear();
            outClientChannel.clear();
            return result;
        }
    
        int bufferSize = SOCKET_BUFFER_SIZE;
        setsockopt(sockets[0], SOL_SOCKET, SO_SNDBUF, &bufferSize, sizeof(bufferSize));
        setsockopt(sockets[0], SOL_SOCKET, SO_RCVBUF, &bufferSize, sizeof(bufferSize));
        setsockopt(sockets[1], SOL_SOCKET, SO_SNDBUF, &bufferSize, sizeof(bufferSize));
        setsockopt(sockets[1], SOL_SOCKET, SO_RCVBUF, &bufferSize, sizeof(bufferSize));
    
        String8 serverChannelName = name;
        serverChannelName.append(" (server)");
        outServerChannel = new InputChannel(serverChannelName, sockets[0]);
    
        String8 clientChannelName = name;
        clientChannelName.append(" (client)");
        outClientChannel = new InputChannel(clientChannelName, sockets[1]);
        return OK;
    }



### registerInputChannel

mService.mInputManager.registerInputChannel 实际上是调用了InputManagerService的registerInputChannel方法：

    /**
    
	 * Registers an input channel so that it can be used as an input event target.
     * @param inputChannel The input channel to register.
     * @param inputWindowHandle The handle of the input window associated with the
     * input channel, or null if none.
     */
    public void registerInputChannel(InputChannel inputChannel,
            InputWindowHandle inputWindowHandle) {
        if (inputChannel == null) {
            throw new IllegalArgumentException("inputChannel must not be null.");
        }

        nativeRegisterInputChannel(mPtr, inputChannel, inputWindowHandle, false);
    }

接着进入native层进行注册，nativeRegisterInputChannel对应的同名函数在frameworks\base\services\core\jni\com_android_server_input_InputManagerService.cpp中：

    static void nativeRegisterInputChannel(JNIEnv* env, jclass /* clazz */,
            jlong ptr, jobject inputChannelObj, jobject inputWindowHandleObj, jboolean monitor) {
        NativeInputManager* im = reinterpret_cast<NativeInputManager*>(ptr);
    
        sp<InputChannel> inputChannel = android_view_InputChannel_getInputChannel(env,
                inputChannelObj);
        if (inputChannel == NULL) {
            throwInputChannelNotInitialized(env);
            return;
        }
    
        sp<InputWindowHandle> inputWindowHandle =
                android_server_InputWindowHandle_getHandle(env, inputWindowHandleObj);
    
        status_t status = im->registerInputChannel(
                env, inputChannel, inputWindowHandle, monitor);
        if (status) {
            String8 message;
            message.appendFormat("Failed to register input channel.  status=%d", status);
            jniThrowRuntimeException(env, message.string());
            return;
        }
    
        if (! monitor) {
            android_view_InputChannel_setDisposeCallback(env, inputChannelObj,
                    handleInputChannelDisposed, im);
        }
    }

接着调用NativeInputManager的registerInputChannel函数：

    status_t NativeInputManager::registerInputChannel(JNIEnv* /* env */,
            const sp<InputChannel>& inputChannel,
            const sp<InputWindowHandle>& inputWindowHandle, bool monitor) {
        return mInputManager->getDispatcher()->registerInputChannel(
                inputChannel, inputWindowHandle, monitor);
    }

可以发现，它进一步通过InputManager调用了Dispatcher的registerInputChannel方法：

    status_t InputDispatcher::registerInputChannel(const sp<InputChannel>& inputChannel,
            const sp<InputWindowHandle>& inputWindowHandle, bool monitor) {

        { // acquire lock
            AutoMutex _l(mLock);
    
            if (getConnectionIndexLocked(inputChannel) >= 0) {
                ALOGW("Attempted to register already registered input channel '%s'",
                        inputChannel->getName().string());
                return BAD_VALUE;
            }
            // 创建Connection
            sp<Connection> connection = new Connection(inputChannel, inputWindowHandle, monitor);
    
            int fd = inputChannel->getFd();
            mConnectionsByFd.add(fd, connection);
    
            if (monitor) {
                 mMonitoringChannels.push(inputChannel);
            }
            // looper add fd
            mLooper->addFd(fd, 0, ALOOPER_EVENT_INPUT, handleReceiveCallback, this);
        } // release lock
    
        // Wake the looper because some connections have changed.
        mLooper->wake();
        return OK;
    }


引用一张图

![image](https://note.youdao.com/yws/res/9515/WEBRESOURCE8b0fdf88ac78c04a3684d3250d3a7e0f)

----- 
参考:
> 深入理解Android 卷三
