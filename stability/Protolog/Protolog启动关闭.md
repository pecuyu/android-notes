# 帮助说明

```
255|generic_x86:/ $ wm logging                                                                                                
Unknown command
Window manager logging options:
  start: Start proto logging
  stop: Stop proto logging
  enable [group...]: Enable proto logging for given groups
  disable [group...]: Disable proto logging for given groups
  enable-text [group...]: Enable logcat logging for given groups
  disable-text [group...]: Disable logcat logging for given groups
  
```

# 打开 proto log groups 开关

```
generic_x86:/ $ wm logging enable-text WM_DEBUG_STARTING_WINDOW

generic_x86:/ $ wm logging enable-text WM_DEBUG_ADD_REMOVE


```


# 关闭 proto log groups 开关

```
generic_x86:/ $ wm logging disable-text WM_DEBUG_STARTING_WINDOW     

generic_x86:/ $ wm logging disable-text WM_DEBUG_ADD_REMOVE

```


# groups

```
"groups": {
  "TEST_GROUP": {
    "tag": "WindowManagetProtoLogTest"
  },
  "WM_DEBUG_ADD_REMOVE": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_APP_TRANSITIONS": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_APP_TRANSITIONS_ANIM": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_BOOT": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_DRAW": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_FOCUS": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_FOCUS_LIGHT": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_IME": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_KEEP_SCREEN_ON": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_ORIENTATION": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_RECENTS_ANIMATIONS": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_REMOTE_ANIMATIONS": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_RESIZE": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_SCREEN_ON": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_STARTING_WINDOW": {
    "tag": "WindowManager"
  },
  "WM_DEBUG_WINDOW_MOVEMENT": {
    "tag": "WindowManager"
  },
  "WM_ERROR": {
    "tag": "WindowManager"
  },
  "WM_SHOW_SURFACE_ALLOC": {
    "tag": "WindowManager"
  },
  "WM_SHOW_TRANSACTIONS": {
    "tag": "WindowManager"
  }
}

```
