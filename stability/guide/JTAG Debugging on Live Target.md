JTAG Debugging on Live Target

When there is a target freeze, that is, the device does not show up on adb
and QPST, you can get some information by debugging over JTAG:
- Get dmesg logs over JTAG
- Look at the states, stacktraces of tasks

Please follow these steps to debug over JTAG:
 1. Open T32 sessions for RPM and krait
 2. In RPM T32
  - sys.m a
 3. In Krait T32
  - sys.m a b
  - data.load.elf <vmlinux> /nocode
  - data.save.binary C:\Dropbox\dmesg.txt __log_buf++40000
 4. If you want the stacktrace of a CPU (i.e. what it is doing at a particular time), you can issue the follow command
  - v.F
 5. To know where the program counter (PC) is, try snoop.pc on
 6. If PC is in 0xCyyyyyyy, the CPU is executing in kernel and not in userspace.
 7. If the issue is in android framework, you can load the linux aware module and look at individual tasks, their individual stacktraces. This is especially helpful when certain userspace tasks are in D state
  - task.config C:\T32\demo\arm\kernel\linux\linux.t32
  - menu.reprogram C:\T32\demo\arm\kernel\linux\linux.men

Note that the above commands will create a new menu "Linux" where you can select
the tasks.

 8. Once you have the Linux menu along with other menu items at the top of your T32 session, you can click on "Linux" -> "show tasks" to see the kernel tasks:
  - The following steps (9-11) are based on an example set of logs of a black screen issue where ADB is up. One of system_server's threads is stuck on Surface.nativeScreenshot(). This is a binder call to surfaceflinger.
  - Surfaceflinger in turn is in D state.
 9. Since we see surfaceflinger in D state, we need to know what surfaceflinger is doing in the kernel.
  - We can do this by selecting the process (surfaceflinger) -> right click -> Select StackFrame which will give us the stacktrace for the surfaceflinger task.
 10. Surfaceflinger is waiting on a mutex held by some other task.
  - Find the owner by looking at "comm" field of the owner.
 11. Once you fine the owner in Step 10, you can find what the owner is doing by following the same method as Step 9:
  - Right click on the owner -> Select stackframe
