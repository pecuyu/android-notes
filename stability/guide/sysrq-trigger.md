
# sysrq-trigger 常用命令
* w - 导出blocked task
* m - 导出关于内存分配的信息
* t - 导出线程状态信息
* p - 导出当前CPU寄存器信息和标志位的信息
* c - 故意让系统崩溃(在使用netdump或diskdump的时候有用)
* s - 即时同步所有挂载的文件系统
* u - 即时重新挂载所有的文件系统为只读
* b - 即时重新启动系统
* o - 即时关机(如果机器设置并支持此项功能)

# 触发系统crash
```c
echo c > /proc/sysrq-trigger    // 触发kernel panic
```

# 输出blocked task
```c
# echo w > /proc/sysrq-trigger

02-26 11:23:15.646476  2495  2495 I sysrq   : Show Blocked State
02-26 11:23:15.646501  2495  2495 I task    : crtc_commit:152 state:D stack:    0 pid:  203 ppid:     2 flags:0x00000008
02-26 11:23:15.646504  2495  2495 I Call trace:  
02-26 11:23:15.646509  2495  2495 I         : __switch_to+0x248/0x4e0
02-26 11:23:15.646513  2495  2495 I         : __schedule+0x5b0/0xbe4
02-26 11:23:15.646515  2495  2495 I         : schedule+0x80/0x1ac
02-26 11:23:15.646516  2495  2495 I         : schedule_timeout+0x98/0x144
02-26 11:23:15.646609  2495  2495 I         : sde_encoder_helper_wait_for_irq+0x27c/0x74c [msm_drm]
02-26 11:23:15.646682  2495  2495 I         : _sde_encoder_phys_cmd_wait_for_wr_ptr+0xa8/0x294 [msm_drm]
02-26 11:23:15.646753  2495  2495 I         : sde_encoder_phys_cmd_wait_for_commit_done$e3390b025b33a02c0fd527a5e5e89191+0x60/0x500 [msm_drm]
02-26 11:23:15.646824  2495  2495 I         : sde_encoder_wait_for_event+0x154/0x460 [msm_drm]
02-26 11:23:15.646895  2495  2495 I         : sde_kms_wait_for_commit_done$8e0940721c95d7fa118df1ffe7c2a3a8+0xdc/0x388 [msm_drm]
02-26 11:23:15.646965  2495  2495 I         : complete_commit+0x154/0x648 [msm_drm]
02-26 11:23:15.647035  2495  2495 I         : _msm_drm_commit_work_cb$5a3ad80d0aeb842650cc3d0a1386111f+0x2c/0x1c4 [msm_drm]
02-26 11:23:15.647037  2495  2495 I         : kthread_worker_fn+0x178/0x58c
02-26 11:23:15.647038  2495  2495 I         : kthread+0x150/0x200
02-26 11:23:15.647040  2495  2495 I         : ret_from_fork+0x10/0x30
...
```


# 显示内存信息
```c
# echo m > /proc/sysrq-trigger

02-13 15:06:04.042     0     0 I sysrq   : Show Memory
02-13 15:06:04.042     0     0 W Mem-Info:
02-13 15:06:04.042     0     0 W active_anon: 31206 inactive_anon:110804 isolated_anon:0
02-13 15:06:04.042     0     0 W active_file: 60786 inactive_file:86270 isolated_file:0
02-13 15:06:04.042     0     0 W unevictable: 954 dirty:57 writeback:0
02-13 15:06:04.042     0     0 W slab_reclaimable: 16015 slab_unreclaimable:32277
02-13 15:06:04.042     0     0 W mapped  : 107353 shmem:2130 pagetables:20860 bounce:0
02-13 15:06:04.042     0     0 W kernel_misc_reclaimable: 0
02-13 15:06:04.042     0     0 W free    : 85815 free_pcp:6316 free_cma:42372
02-13 15:06:04.042     0     0 W         : Node 0 active_anon:124824kB inactive_anon:443216kB active_file:243144kB inactive_file:345080kB unevictable:3816kB isolated(anon):0kB isolated(file):0kB mapped:429412kB dirty:228kB writeback:0kB shmem:8520kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:34896kB pagetables:83440kB all_unreclaimable? no
02-13 15:06:04.042     0     0 W DMA32 free: 343260kB min:73728kB low:116616kB high:159504kB reserved_highatomic:0KB active_anon:124824kB inactive_anon:443216kB active_file:243144kB inactive_file:345080kB unevictable:3816kB writepending:228kB present:2096584kB managed:2013564kB mlocked:3816kB bounce:0kB free_pcp:25264kB local_pcp:13380kB free_cma:169488kB
02-13 15:06:04.042     0     0 W lowmem_reserve[]: 0 0 0
02-13 15:06:04.042     0     0 W DMA32   : 9067*4kB (UMEC) 3806*8kB (UMEC) 664*16kB (UMEC) 430*32kB (UMEC) 458*64kB (UMC) 263*128kB (UMEC) 91*256kB (M) 2*512kB (UM) 1*1024kB (C) 0*2048kB 40*4096kB (C) = 343260kB
02-13 15:06:04.042     0     0 W         : 152425 total pagecache pages
02-13 15:06:04.042     0     0 W         : 2526 pages in swap cache
02-13 15:06:04.042     0     0 W         : Swap cache stats: add 174850, delete 172325, find 16494/63459
02-13 15:06:04.042     0     0 W         : Free swap  = 1031448kB
02-13 15:06:04.042     0     0 W         : Total swap = 1510168kB
02-13 15:06:04.042     0     0 W         : 524146 pages RAM
02-13 15:06:04.042     0     0 W         : 0 pages HighMem/MovableOnly
02-13 15:06:04.042     0     0 W         : 20755 pages reserved
02-13 15:06:04.042     0     0 W         : 73728 pages cma reserved
```

# 输出当前tasks和信息
```c
echo t > /proc/sysrq-trigger

02-13 15:41:14.350     0     0 I sysrq   : Show State
02-13 15:41:14.350     0     0 I task    : init            state:S stack:11280 pid:    1 ppid:     0 flags:0x00004000
02-13 15:41:14.350     0     0 I Call Trace:  
02-13 15:41:14.350     0     0 I         : <TASK>
02-13 15:41:14.350     0     0 I         : __schedule+0x46a/0x700
02-13 15:41:14.350     0     0 I         : schedule+0x101/0x1e0
02-13 15:41:14.350     0     0 I         : schedule_hrtimeout_range_clock+0xe1/0x170
02-13 15:41:14.350     0     0 I         : ? hrtimer_reprogram+0xf0/0xf0
02-13 15:41:14.350     0     0 I         : do_epoll_wait+0x85e/0x930
02-13 15:41:14.350     0     0 I         : ? wait_woken+0x70/0x70
02-13 15:41:14.350     0     0 I         : __se_sys_epoll_pwait+0x12a/0x1b0
02-13 15:41:14.350     0     0 I         : __x64_sys_epoll_pwait+0x24/0x30
02-13 15:41:14.350     0     0 I         : do_syscall_64+0x43/0x90
02-13 15:41:14.350     0     0 I         : ? sysvec_apic_timer_interrupt+0x55/0xc0
02-13 15:41:14.350     0     0 I         : entry_SYSCALL_64_after_hwframe+0x44/0xae
02-13 15:41:14.350     0     0 I RIP     : 0033:0x7f7f9632b8aa
02-13 15:41:14.350     0     0 I RSP     : 002b:00007ffc9afa24c8 EFLAGS: 00000246 ORIG_RAX: 0000000000000119
02-13 15:41:14.350     0     0 I RAX     : ffffffffffffffda RBX: 0000000000002710 RCX: 00007f7f9632b8aa
02-13 15:41:14.350     0     0 I RDX     : 0000000000000007 RSI: 00007ffc9afa24d0 RDI: 0000000000000004
02-13 15:41:14.350     0     0 I RBP     : 00007ffc9afa26f0 R08: 0000000000000000 R09: 0000000000000008
02-13 15:41:14.350     0     0 I         : R10: 0000000000002710 R11: 0000000000000246 R12: 0000000000002710
02-13 15:41:14.350     0     0 I         : R13: 0000000000000000 R14: 0000000000000007 R15: 00007ffc9afa2b60
02-13 15:41:14.350     0     0 I         : </TASK>
02-13 15:41:14.350     0     0 I task    : init            state:S stack:12376 pid:  183 ppid:     0 flags:0x00004000
...
```

# 导出当前CPU寄存器信息和标志位的信息
```c
echo p > /proc/sysrq-trigger

02-13 15:49:22.112     0     0 I sysrq   : Show Regs
02-13 15:49:22.112     0     0 E BUG     : using smp_processor_id() in preemptible [00000000] code: sh/3643
02-13 15:49:22.112     0     0 W         : caller is debug_smp_processor_id+0x17/0x20
02-13 15:49:22.112     0     0 W         : CPU: 3 PID: 3643 Comm: sh Tainted: G           OE     5.15.41-android13-8-00205-gf1bf82c3dacd-ab8747247 #1
02-13 15:49:22.112     0     0 W Hardware name: QEMU Standard PC (i440FX + PIIX, 1996), BIOS rel-1.11.1-0-g0551a4be2c-prebuilt.qemu-project.org 04/01/2014
02-13 15:49:22.112     0     0 W Call Trace:  
02-13 15:49:22.112     0     0 W         : <TASK>
02-13 15:49:22.112     0     0 W         : dump_stack+0x10/0x19
02-13 15:49:22.112     0     0 W         : check_preemption_disabled+0x10b/0x120
02-13 15:49:22.112     0     0 W         : debug_smp_processor_id+0x17/0x20
02-13 15:49:22.112     0     0 W         : perf_event_print_debug+0x23/0x540
02-13 15:49:22.112     0     0 W         : ? vprintk+0x4c/0x50
02-13 15:49:22.112     0     0 W         : ? _printk+0x54/0x7c
02-13 15:49:22.112     0     0 W         : sysrq_handle_showregs+0x85/0x90
02-13 15:49:22.112     0     0 W         : __handle_sysrq+0xf6/0x250
02-13 15:49:22.112     0     0 W         : write_sysrq_trigger+0x2d/0x40
02-13 15:49:22.112     0     0 W         : proc_reg_write+0x46/0xa0
02-13 15:49:22.112     0     0 W         : vfs_write+0x160/0x490
02-13 15:49:22.112     0     0 W         : ksys_write+0x70/0xf0
02-13 15:49:22.112     0     0 W         : __x64_sys_write+0x16/0x20
02-13 15:49:22.112     0     0 W         : do_syscall_64+0x43/0x90
02-13 15:49:22.112     0     0 W         : ? asm_common_interrupt+0x8/0x40
02-13 15:49:22.112     0     0 W         : entry_SYSCALL_64_after_hwframe+0x44/0xae
02-13 15:49:22.112     0     0 W RIP     : 0033:0x77fa0811f607
02-13 15:49:22.112     0     0 W Code    : 00 00 00 b8 00 00 00 00 0f 05 48 3d 01 f0 ff ff 72 09 f7 d8 89 c7 e8 08 fb ff ff c3 0f 1f 80 00 00 00 00 b8 01 00 00 00 0f 05 <48> 3d 01 f0 ff ff 72 09 f7 d8 89 c7 e8 e8 fa ff ff c3 0f 1f 80 00
02-13 15:49:22.112     0     0 W RSP     : 002b:00007ffeb50fd558 EFLAGS: 00000217 ORIG_RAX: 0000000000000001
02-13 15:49:22.112     0     0 W RAX     : ffffffffffffffda RBX: 0000000000000002 RCX: 000077fa0811f607
02-13 15:49:22.112     0     0 W RDX     : 0000000000000002 RSI: 000077f8780761c8 RDI: 0000000000000001
02-13 15:49:22.112     0     0 W RBP     : 000077f8780761c8 R08: ffffffffffffffff R09: 0000000000000001
02-13 15:49:22.112     0     0 W         : R10: 00000000ffffff00 R11: 0000000000000217 R12: 0000596443d05170
02-13 15:49:22.112     0     0 W         : R13: 00007ffeb50fd570 R14: 000077f8a80738f0 R15: 00007ffeb50fd5a8
02-13 15:49:22.112     0     0 W         : </TASK>
02-13 15:49:22.112     0     0 I CPU#3   : ctrl:       0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : status:     0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : overflow:   0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : fixed:      0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : active:     0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC0 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC0 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC0 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC1 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC1 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC1 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC2 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC2 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC2 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC3 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC3 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC3 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC4 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC4 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC4 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC5 ctrl:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC5 count: 0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC5 left:  0000000000000000
02-13 15:49:22.112     0     0 I CPU#3   : gen-PMC6 ctrl:  0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : gen-PMC6 count: 0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : gen-PMC6 left:  0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : gen-PMC7 ctrl:  0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : gen-PMC7 count: 0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : gen-PMC7 left:  0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : fixed-PMC0 count: 0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : fixed-PMC1 count: 0000000000000000
02-13 15:49:22.113     0     0 I CPU#3   : fixed-PMC2 count: 0000000000000000
```

# 所有命令 Command	Function

- b	Will immediately reboot the system without syncing or unmounting your disks.
- c	Will perform a system crash and a crashdump will be taken if configured.
- d	Shows all locks that are held.
- e	Send a SIGTERM to all processes, except for init.
- f	Will call the oom killer to kill a memory hog process, but do not panic if nothing can be killed.
- g	Used by kgdb (kernel debugger)
- h	Will display help (actually any other key than those listed here will display help. but h is easy to remember :-)
- i	Send a SIGKILL to all processes, except for init.
- j	Forcibly “Just thaw it” - filesystems frozen by the FIFREEZE ioctl.
- k	Secure Access Key (SAK) Kills all programs on the current virtual console. NOTE: See important comments below in SAK section.
- l	Shows a stack backtrace for all active CPUs.
- m	Will dump current memory info to your console.
- n	Used to make RT tasks nice-able
- o	Will shut your system off (if configured and supported).
- p	Will dump the current registers and flags to your console.
- q	Will dump per CPU lists of all armed hrtimers (but NOT regular timer_list timers) and detailed information about all clockevent devices.
- r	Turns off keyboard raw mode and sets it to XLATE.
- s	Will attempt to sync all mounted filesystems.
- t	Will dump a list of current tasks and their information to your console.
- u	Will attempt to remount all mounted filesystems read-only.
- v	Forcefully restores framebuffer console
- v	Causes ETM buffer dump [ARM-specific]
- w	Dumps tasks that are in uninterruptable (blocked) state.
- x	Used by xmon interface on ppc/powerpc platforms. Show global PMU Registers on sparc64. Dump all TLB entries on MIPS.
- y	Show global CPU Registers [SPARC-64 specific]
- z	Dump the ftrace buffer
- 0-9	Sets the console log level, controlling which kernel messages will be printed to your console. (0, for example would make it so that only emergency messages like PANICs or OOPSes would make it to your console.)

# 参考
https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html
