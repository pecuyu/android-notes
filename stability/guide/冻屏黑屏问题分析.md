
# 调试冻屏问题
## 问题原因分类
- Watchdog
- System server 进程D状态
- Input被某些原因设置成disabled、frozen等状态，导致无事件上报
    - DispatchEnabled: 设为false会调用resetAndDropEverythingLocked函数，丢弃所有事件及重置所有等待状态，并且导致无法派发事件，在派发流程的可能因此原因被丢弃
    - DispatchFrozen: 设为true导致无法派发事件，会移除NoFocusedWindowTimeout
    - InputFilterEnabled: 设置为true同会丢弃和重置，之后事件都交由 InputFilter处理。
    - wms freezing 操作未执行unfreeze
- 输入事件被丢弃
  - 窗口被遮挡，导致事件丢弃
  - 窗口处于stopped状态导致事件丢弃
  - 按HOME键应用切换期间，事件被丢弃
- 应用进程被冻结或处于D状态，无法响应事件
- 驱动没有上报输入事件？getevent看看是否有事件上报
- 硬件tp问题

## 分析思路
若有现场可以调试分析：
- 确认冻住的UI界面、显示时间是否有更新。查看状态栏时间 是否停止了，对问题发生时间点有帮助
- 确认是否发生Watchdog，查看 /data/anr 是否有 WDT 的trace文件，system log里面是否有watchdog卡住打印
- 按power、音量键是否有声音，是否有事件输出，可以初步判断系统活跃状态
  - intercept_power   PhoneWindowManager拦截power事件处理
  - screen_toggled    PhoneWindowManager打印亮灭屏状态，1亮屏 0灭屏
  - volume_changed    音量发生改变AudioService打印
- 确定是否是UI不能响应事件
  - 使用 getevent 看看是否有事件上报，触碰看 getevent 是否有输出。如果有输出但是界面无响应，查看进程状态是否是D状态
  - 使用 adb shell input keyevent 模拟一些事件看看是否有反应，参考KeyEvent.java里面的键值
  - 查看 InputDispatcher、InputReader 相关打印，是否有事件丢弃、窗口遮挡等异常log
  - 使用 am start 命令启动activity 看看能否切换界面，如 am start com.android.settings == 需要现场的先不要执行
- top/ps 查看 system_server 、SurfaceFlinger 等线程是否有处于 D 状态。如是:
  - 使用命令查看所有处于 D 状态线程，命令输出在kernel log： echo w > /proc/sysrq-trigger
  - 可以连接 JTAG 查看 kernel level tasks
- 查看logcat log中的 error 和 top的CPU使用情况
- 如没有adb端口，可尝试触发crash抓死机 ramdump，解析dump分析相关进程状态、任务等
- 如有串口可以抓串口log获取更多线索

## watchdog 原因:
- 线程执行IPC call卡住
  - 同步binder调用被客户端端卡住
  - oneway 调用被kernel卡住
  - socket等通信卡住
  - binder线程耗尽
- 发生死锁：kernel/driver、 android framework / native code
- native执行卡住，如等锁
- 底层状态不正确：hardware, kernel, driver, or modem

## Watchdog导致框架重启流程
- Watchdog导致 system_server kill 自身
- Zygote监听到 system_server 进程退出后，会kill 自身
- init进程监听到 Zygote 进程退出，会重新启动 Zygote，框架发送重启。

## 抓取log方法
- 执行 adb shell kill -3 <pid_of_system_server> 获取系统进程trace来分析，每隔段时间多次执行对比trace看是否有线程卡住，生成的日志在 /data/anr 目录下。这个是通过 SignalCatcher 来抓的，如果该线程卡住则无法通过此方式抓trace
- 使用 debuggerd，如 debuggerd <pid> ，输出进程native stacktraces
  - debuggerd -j  输出java trace，主要针对java 进程
  - debuggerd -b, --backtrace 只输出 backtrace 而非完整 tombstone
- kill -6 <pid> ， 这种通常也能输出进程的trace到 /data/tombstones，但是进程会退出，导致现场丢失，慎重使用此方式。
- dumpsys 系统服务，如 window activity input SurfaceFlinger display
- 获取binder transaction log， pull /dev/binderfs/binder_logs/ 或 /d/binder/
- 抓取ramdump ， echo c > /proc/sysrq-trigger


## 收集log
- Trace:
  - system server WDT trace 路径 /data/anr
  - BinderTraces， 路径 /dev/binderfs/binder_logs/ 或 /d/binder/
- System log:
  - Watchdog: *** WATCHDOG KILLING SYSTEM PROCESS:
  - Blocked in handler on ui thread (android.ui)
- Event log:
  - 03-01 23:29:48.089083  2339  2461 I watchdog: Blocked in handler on ui thread (android.ui)
  - 09-09 09:26:10.089 2912 2912 I boot_progress_start: 4895219
- Kernel log
- 无端口， 可尝试触发crash抓取ramdump



log分析：
- input_interaction  查看交互窗口
- input_focus 查看焦点窗口变化。Focus request 在WMS输出，Focus leaving、Focus entering  在input模块输出
- dumpsys input window  查看窗口状态、事件派发队列是否正常
- 查看 InputDispatcher、InputReader 相关打印，是否有事件丢弃、窗口遮挡等异常log


# 调试黑屏问题
## 问题原因分类
- 死机、关机导致，插电是否有死机口、充电动画
- 系统层面
  - Watchdog卡住导致，通常会系统重启，在某些场景如monkey下可能不重启
  - power键不亮屏
    - wakeup流程没走完
    - power事件没有上报或被拦截
    - 屏幕亮度异常
  - wms异常，如某些窗口没有 performShow，比如窗口动画进行卡住时
  - SurfaceFlinger异常, 如出现卡住或binder线程耗尽
  - hwc异常
  - 显示驱动故障，如报 PANEL_DEAD
- 应用异常
  - SystemUI无法启动，表象是黑屏
  - SystemUI出现ANR或主线程卡住，表象是定屏甚至黑屏，手势上划无反应
  - Launcher无法启动，表象是桌面黑屏
  - 前台显示应用被冻结或处于D状态，界面不刷新，事件不响应
  - app因为卡顿等原因没有完成绘制，dump window信息查看绘制状态
    - mDrawState - 如应用已绘制但是没有显示 需要排查未显示出的原因
    - alpha
- 硬件问题
  - 屏不良，导致出现黑屏
  - TP问题，导致出现定屏
  - DDR、存储等

## 分析思路
- 有现场可以插上USB，排除死机、关机导致(插电是否有死机口、充电动画)
- 判断是否出现watchdog冻屏(event,system log 及 WDT trace)，出现这种情况通常dumpsys会卡住
- 判断屏幕亮屏状态， dumpsys display， 排除是否是power无法唤醒或屏幕亮度异常
  - Display State=ON
  - Display Brightness=0.03930185
- 判断top应用的绘制状态， dumpsys window SurfaceFlinger
  - dumpsys activity top-resumed
  - 查看应用线程状态 应用主线程卡住导致无法绘制，如io、死锁、等锁及其他耗时操作
- 其他关键进程状态，是否有D状态，如 system_server、SurfaceFlinger、hwc
- 关键应用不断crash，如 systemui、launcher
- 查看SurfaceFlinger线程状态，是否有出现binder线程耗尽、binder调用等待或其他导致卡住操作
- 排查是否有驱动异常
- winscope 调试黑屏

# 线程状态
- ZOMBIE − terminated thread
- RUNNABLE − runnable or running now
- TIMED_WAIT − timed waiting in Object.wait()
- MONITOR − blocked on a monitor
- WAIT − waiting in Object.wait()
- INITIALIZING − allocated, not yet running
- STARTING − started, not yet on thread list
- NATIVE − off in a JNI native method
- VMWAIT − waiting on a VM resource
- SUSPENDED − suspended, usually by GC or debugger
- UNKNOWN − thread is in the undefined state

# 线程信息分析

"main" prio=5 tid=1 Native

| group="main" sCount=1 dsCount=0 flags=1 obj=0x75949ae8 self=0x7561414c00

| sysTid=1382 nice=-2 cgrp=default sched=0/0 handle=0x75e72be548

| state=S schedstat=( 2217218466 547125897 2809 ) utm=135 stm=86 core=3 HZ=100

| stack=0x7fe63cd000-0x7fe63cf000 stackSize=8MB

- Thread’s information format – first line
  - name – Thread name
  - priority – Thread priority
  - tid – Thread ID
  - status – Thread status

- Thread’s information format – second line：
  - group – Group name,
  - sCount – Suspend count,
  - dsCount – Debug suspend count,
  - obj – Linux thread that we are associated with
  - self – Self reference

- Thread’s information format – third line:
  - sysTid – Linux thread ID
  - nice – Linux “nice” priority (lower numbers indicate higher priority)
  - sched – Scheduling priority
  - cgrp – Scheduling group buffer
  - handle – Thread handle

- Thread’s information format – fourth line
  - Schedstat: This is CPU schedule statics data:
    - Running time: CPU running time, unit: ns.
    - Runnable time: RQ queue waiting time, unit: ns.
    - Switch number: CPU scheduler switch times.
  - utm: Thread run in user space time, unit: jiffies. jiffies defined by "sysconf(_SC_CLK_TCK)". The default value is 10ms.
  - stm: Thread run in kernel space time, unit: jiffies. The default value is 10ms.

# init阶段无法启动
- 抓串口log
- 修改init crash触发panic，分析死机dump
