# lldb 解析 coredump
可以在下载的最新ndk中找到 lldb, lldb 与 gdb 命令映射关系参考下面链接

GDB to LLDB command map
>https://lldb.llvm.org/use/map.html

```
## 设置搜索路径和core文件
(lldb) settings set target.exec-search-paths ./symbols/apex/com.android.art/lib64/ ./symbols/apex/com.android.runtime/lib64/bionic/ ./symbols/system/bin
(lldb) target create "./symbols/system/bin/app_process64" -c "!system!bin!app_process64.23784.HeapTaskDaemon"
Core file 'E:\log\S\coredump\qssi\out\target\product\qssi\!system!bin!app_process64.23784.HeapTaskDaemon' (aarch64) was loaded.

## 显示当前线程的栈 backtrace
(lldb) bt
## 查看所有线程的
(lldb) thread backtrace all
(lldb) bt all

## 选择查看 frame  
(lldb) frame select 12
(lldb) fr s 12
(lldb) f 12
## 显示frame信息
(lldb) frame info

## 显示调用此frame的 stack frame
(lldb) up
(lldb) frame select --relative=1

## 显示此frame 调用的 stack frame
(lldb) down
(lldb) frame select --relative=-1
(lldb) fr s -r-1

## 显示dump中所有的线程
(lldb) thread list

## 切换线程
(lldb) thread select 1
(lldb) t 1

## 线程dump里加载的所有的共享库 shared libraries
(lldb) image list

## 输出当前frame的汇编代码 assembly codes
(lldb) disassemble --frame
(lldb) di -f

## 打印当前帧(frame)的通用寄存器 general purpose registers
(lldb) register read

## 打印当前帧的变量 variables
(lldb) frame variable

## 打印变量 mutex 的值
(lldb) p *mutex

## 通过变量名或其地址打印mutex变量的内存
(lldb) x/10x mutex // x/10x 0x743ed0474c

## 打印所有的 settings 命令
[Print all settings commands]
(lldb) settings list

## 帮助命令
(lldb) help target

```

# gdb 解析 coredump
12源码下prebuilts里会带gdb，在ndk23版及以前会带 gdb，新的版本可以使用 lldb。

```
## 设置应用需要加载的动态库文件路径，路径请根据时间情况设置，采用分号分割
(gdb) set solib-search-path ./symbols/system/lib64;./symbols/apex/com.android.art/lib64/
另外，可以设置root搜索路径，使用 set sysroot path_xxx

## 设置要加载可执行程序，对于Android应用，64位的是app_process64， 32位的是app_process32
(gdb) file ./symbols/system/bin/app_process64

## 加载coredump文件

(gdb) core-file coredump-xxx

## 查看加载的动态库，确认已经正确加载。如果第二步设置的动态库路径不对，会加载不成功
(gdb)  info sharedlibrary

## 查看最后调用栈
(gdb) bt
(gdb) bt full

## 切换栈帧
(gdb) frame(f) n

## 第几栈帧的信息
(gdb) info frame(f) 0

## 显示当前栈帧函数参数
(gdb) info args

## 当前帧函数局部变量
(gdb) info locals

## 查看线程信息/切换线程
(gdb) info threads / thread n

## 查看各个线程状态
(gdb) info threads

## 查看文件中某行代码在内存中的地址
(gdb) info line filename:func/line

## 反汇编函数/反汇编pc寄存器指向的指令所属函数
(gdb) disassemble fuc / disassemble $pc

## 查看寄存器
(gdb) info registers

## 查看内存中的值
(gdb) x/fu address
f表示显示方式, 可取如下值：
x 按十六进制格式显示变量。
d 按十进制格式显示变量。
u 按十进制格式显示无符号整型。
o 按八进制格式显示变量。
t 按二进制格式显示变量。
a 按十六进制格式显示变量。
i 指令地址格式
c 按字符格式显示变量。
f 按浮点数格式显示变量。

u表示一个地址单元的长度
b表示单字节，
h表示双字节，
w表示四字节，
g表示八字节

## 查看所有线程的调用栈
(gdb) thread apply all bt

```

# 生成 coredump
在项目的 init.rc 中控制，使用 setrlimit 设置core file size 为 unlimited
```
on property:persist.debug.trace=1
  mkdir /data/core 0777 root root
  setrlimit 4 -1 -1
  write /proc/sys/kernel/core_pattern "/data/core/%E.%p.%e"
  write /proc/sys/fs/suid_dumpable 1
```

在debug版本上，设置persist.debug.trace为1，然后通过setenforce 0关闭selinux，最后执行 stop；start命令重启框架。测试 kill -11 pid ， 看/data/core目录下是否生成 coredump 文件
```
adb root
adb shell setprop persist.debug.trace 1
adb shell setenforce 0
adb shell getenforce
adb shell "stop; start"
```

查看某进程的limits，Max core file size 最好都为unlimited，否则可能抓不到coredump
```
# cat /proc/516/limits                                                                                              
Limit                     Soft Limit           Hard Limit           Units     
Max cpu time              unlimited            unlimited            seconds   
Max file size             unlimited            unlimited            bytes     
Max data size             unlimited            unlimited            bytes     
Max stack size            8388608              unlimited            bytes     
Max core file size        0 ==》不能为0         unlimited            bytes     
Max resident set          unlimited            unlimited            bytes     
Max processes             6639                 6639                 processes
Max open files            32768                32768                files     
Max locked memory         8388608              8388608              bytes     
Max address space         unlimited            unlimited            bytes     
Max file locks            unlimited            unlimited            locks     
Max pending signals       6639                 6639                 signals   
Max msgqueue size         819200               819200               bytes     
Max nice priority         40                   40                   
Max realtime priority     0                    0                    
Max realtime timeout      unlimited            unlimited            us        

```
