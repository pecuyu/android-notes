#ifndef HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BNHWSMARTPACALL_H
#define HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BNHWSMARTPACALL_H

#include <vendor/unique/smartpa/1.0/IHwSmartPaCall.h>

namespace vendor {
namespace unique {
namespace smartpa {
namespace V1_0 {

struct BnHwSmartPaCall : public ::android::hidl::base::V1_0::BnHwBase {
    explicit BnHwSmartPaCall(const ::android::sp<ISmartPaCall> &_hidl_impl);
    explicit BnHwSmartPaCall(const ::android::sp<ISmartPaCall> &_hidl_impl, const std::string& HidlInstrumentor_package, const std::string& HidlInstrumentor_interface);

    virtual ~BnHwSmartPaCall();

    ::android::status_t onTransact(
            uint32_t _hidl_code,
            const ::android::hardware::Parcel &_hidl_data,
            ::android::hardware::Parcel *_hidl_reply,
            uint32_t _hidl_flags = 0,
            TransactCallback _hidl_cb = nullptr) override;


    /**
     * The pure class is what this class wraps.
     */
    typedef ISmartPaCall Pure;

    /**
     * Type tag for use in template logic that indicates this is a 'native' class.
     */
    typedef ::android::hardware::details::bnhw_tag _hidl_tag;

    ::android::sp<ISmartPaCall> getImpl() { return _hidl_mImpl; }
    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    static ::android::status_t _hidl_doSmartPa(
            ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
            const ::android::hardware::Parcel &_hidl_data,
            ::android::hardware::Parcel *_hidl_reply,
            TransactCallback _hidl_cb);



private:
    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.

    // Methods from ::android::hidl::base::V1_0::IBase follow.
    ::android::hardware::Return<void> ping();
    using getDebugInfo_cb = ::android::hidl::base::V1_0::IBase::getDebugInfo_cb;
    ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb);

    ::android::sp<ISmartPaCall> _hidl_mImpl;
};

}  // namespace V1_0
}  // namespace smartpa
}  // namespace unique
}  // namespace vendor

#endif  // HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BNHWSMARTPACALL_H
