#include <vendor/unique/smartpa/1.0/ASmartPaCall.h>
#include <hidladapter/HidlBinderAdapter.h>
#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
#include <android/hidl/base/1.0/ABase.h>

namespace vendor {
namespace unique {
namespace smartpa {
namespace V1_0 {

ASmartPaCall::ASmartPaCall(const ::android::sp<::vendor::unique::smartpa::V1_0::ISmartPaCall>& impl) : mImpl(impl) {}// Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
::android::hardware::Return<bool> ASmartPaCall::doSmartPa(uint32_t retryCount){
    auto _hidl_out = mImpl->doSmartPa(
        retryCount);
    if (!_hidl_out.isOkUnchecked()) {
        return _hidl_out;
    }return _hidl_out;
}

// Methods from ::android::hidl::base::V1_0::IBase follow.

}  // namespace V1_0
}  // namespace smartpa
}  // namespace unique
}  // namespace vendor

