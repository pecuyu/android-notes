#ifndef __VTS_PROFILER_vendor_unique_smartpa_V1_0_ISmartPaCall__
#define __VTS_PROFILER_vendor_unique_smartpa_V1_0_ISmartPaCall__


#include <android-base/logging.h>
#include <hidl/HidlSupport.h>
#include <linux/limits.h>
#include <test/vts/proto/ComponentSpecificationMessage.pb.h>
#include "VtsProfilingInterface.h"

// HACK: NAN is #defined by math.h which gets included by
// ComponentSpecificationMessage.pb.h, but some HALs use
// enums called NAN.  Undefine NAN to work around it.
#undef NAN

#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
#include <android/hidl/base/1.0/types.h>


using namespace vendor::unique::smartpa::V1_0;
using namespace android::hardware;

namespace android {
namespace vts {
extern "C" {

    void HIDL_INSTRUMENTATION_FUNCTION_vendor_unique_smartpa_V1_0_ISmartPaCall(
            details::HidlInstrumentor::InstrumentationEvent event,
            const char* package,
            const char* version,
            const char* interface,
            const char* method,
            std::vector<void *> *args);
}

}  // namespace vts
}  // namespace android
#endif
