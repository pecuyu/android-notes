#ifndef HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_ASMARTPACALL_H
#define HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_ASMARTPACALL_H

#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
namespace vendor {
namespace unique {
namespace smartpa {
namespace V1_0 {

class ASmartPaCall : public ::vendor::unique::smartpa::V1_0::ISmartPaCall {
    public:
    typedef ::vendor::unique::smartpa::V1_0::ISmartPaCall Pure;
    ASmartPaCall(const ::android::sp<::vendor::unique::smartpa::V1_0::ISmartPaCall>& impl);
    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    virtual ::android::hardware::Return<bool> doSmartPa(uint32_t retryCount) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

    private:
    ::android::sp<::vendor::unique::smartpa::V1_0::ISmartPaCall> mImpl;
};

}  // namespace V1_0
}  // namespace smartpa
}  // namespace unique
}  // namespace vendor
#endif // HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_ASMARTPACALL_H
