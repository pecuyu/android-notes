#ifndef HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BPHWSMARTPACALL_H
#define HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BPHWSMARTPACALL_H

#include <hidl/HidlTransportSupport.h>

#include <vendor/unique/smartpa/1.0/IHwSmartPaCall.h>

namespace vendor {
namespace unique {
namespace smartpa {
namespace V1_0 {

struct BpHwSmartPaCall : public ::android::hardware::BpInterface<ISmartPaCall>, public ::android::hardware::details::HidlInstrumentor {
    explicit BpHwSmartPaCall(const ::android::sp<::android::hardware::IBinder> &_hidl_impl);

    /**
     * The pure class is what this class wraps.
     */
    typedef ISmartPaCall Pure;

    /**
     * Type tag for use in template logic that indicates this is a 'proxy' class.
     */
    typedef ::android::hardware::details::bphw_tag _hidl_tag;

    virtual bool isRemote() const override { return true; }

    void onLastStrongRef(const void* id) override;

    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    static ::android::hardware::Return<bool>  _hidl_doSmartPa(::android::hardware::IInterface* _hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint32_t retryCount);

    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    ::android::hardware::Return<bool> doSmartPa(uint32_t retryCount) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.
    ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;
    ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;
    ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;
    ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;
    ::android::hardware::Return<void> setHALInstrumentation() override;
    ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;
    ::android::hardware::Return<void> ping() override;
    ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;
    ::android::hardware::Return<void> notifySyspropsChanged() override;
    ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;

private:
    std::mutex _hidl_mMutex;
    std::vector<::android::sp<::android::hardware::hidl_binder_death_recipient>> _hidl_mDeathRecipients;
};

}  // namespace V1_0
}  // namespace smartpa
}  // namespace unique
}  // namespace vendor

#endif  // HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_BPHWSMARTPACALL_H
