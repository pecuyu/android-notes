#include "vendor/unique/smartpa/1.0/SmartPaCall.vts.h"
#include "vts_measurement.h"
#include <android-base/logging.h>
#include <android/hidl/allocator/1.0/IAllocator.h>
#include <fmq/MessageQueue.h>
#include <sys/stat.h>
#include <unistd.h>


using namespace vendor::unique::smartpa::V1_0;
namespace android {
namespace vts {
bool FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall::GetService(bool get_stub, const char* service_name) {
    static bool initialized = false;
    if (!initialized) {
        LOG(INFO) << "HIDL getService";
        if (service_name) {
          LOG(INFO) << "  - service name: " << service_name;
        }
        hw_binder_proxy_ = ::vendor::unique::smartpa::V1_0::ISmartPaCall::getService(service_name, get_stub);
        if (hw_binder_proxy_ == nullptr) {
            LOG(ERROR) << "getService() returned a null pointer.";
            return false;
        }
        LOG(DEBUG) << "hw_binder_proxy_ = " << hw_binder_proxy_.get();
        initialized = true;
    }
    return true;
}


::android::hardware::Return<bool> Vts_vendor_unique_smartpa_V1_0_ISmartPaCall::doSmartPa(
    uint32_t arg0 __attribute__((__unused__))) {
    LOG(INFO) << "doSmartPa called";
    AndroidSystemCallbackRequestMessage callback_message;
    callback_message.set_id(GetCallbackID("doSmartPa"));
    callback_message.set_name("Vts_vendor_unique_smartpa_V1_0_ISmartPaCall::doSmartPa");
    VariableSpecificationMessage* var_msg0 = callback_message.add_arg();
    var_msg0->set_type(TYPE_SCALAR);
    var_msg0->set_scalar_type("uint32_t");
    var_msg0->mutable_scalar_value()->set_uint32_t(arg0);
    RpcCallToAgent(callback_message, callback_socket_name_);
    return static_cast<bool>(0);
}

sp<::vendor::unique::smartpa::V1_0::ISmartPaCall> VtsFuzzerCreateVts_vendor_unique_smartpa_V1_0_ISmartPaCall(const string& callback_socket_name) {
    static sp<::vendor::unique::smartpa::V1_0::ISmartPaCall> result;
    result = new Vts_vendor_unique_smartpa_V1_0_ISmartPaCall(callback_socket_name);
    return result;
}

bool FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall::Fuzz(
    FunctionSpecificationMessage* /*func_msg*/,
    void** /*result*/, const string& /*callback_socket_name*/) {
    return true;
}
bool FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall::GetAttribute(
    FunctionSpecificationMessage* /*func_msg*/,
    void** /*result*/) {
    LOG(ERROR) << "attribute not found.";
    return false;
}
bool FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall::CallFunction(
    const FunctionSpecificationMessage& func_msg,
    const string& callback_socket_name __attribute__((__unused__)),
    FunctionSpecificationMessage* result_msg) {
    const char* func_name = func_msg.name().c_str();
    if (hw_binder_proxy_ == nullptr) {
        LOG(ERROR) << "hw_binder_proxy_ is null. ";
        return false;
    }
    if (!strcmp(func_name, "doSmartPa")) {
        uint32_t arg0 = 0;
        arg0 = func_msg.arg(0).scalar_value().uint32_t();
        LOG(DEBUG) << "local_device = " << hw_binder_proxy_.get();
        bool result0 = hw_binder_proxy_->doSmartPa(arg0);
        result_msg->set_name("doSmartPa");
        VariableSpecificationMessage* result_val_0 = result_msg->add_return_type_hidl();
        result_val_0->set_type(TYPE_SCALAR);
        result_val_0->set_scalar_type("bool_t");
        result_val_0->mutable_scalar_value()->set_bool_t(result0);
        return true;
    }
    if (!strcmp(func_name, "notifySyspropsChanged")) {
        LOG(INFO) << "Call notifySyspropsChanged";
        hw_binder_proxy_->notifySyspropsChanged();
        result_msg->set_name("notifySyspropsChanged");
        return true;
    }
    return false;
}

bool FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall::VerifyResults(const FunctionSpecificationMessage& expected_result __attribute__((__unused__)),
    const FunctionSpecificationMessage& actual_result __attribute__((__unused__))) {
    if (!strcmp(actual_result.name().c_str(), "doSmartPa")) {
        if (actual_result.return_type_hidl_size() != expected_result.return_type_hidl_size() ) { return false; }
        if (actual_result.return_type_hidl(0).scalar_value().bool_t() != expected_result.return_type_hidl(0).scalar_value().bool_t()) { return false; }
        return true;
    }
    return false;
}

extern "C" {
android::vts::DriverBase* vts_func_4_vendor_unique_smartpa_V1_0_ISmartPaCall_() {
    return (android::vts::DriverBase*) new android::vts::FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall();
}

android::vts::DriverBase* vts_func_4_vendor_unique_smartpa_V1_0_ISmartPaCall_with_arg(uint64_t hw_binder_proxy) {
    ::vendor::unique::smartpa::V1_0::ISmartPaCall* arg = nullptr;
    if (hw_binder_proxy) {
        arg = reinterpret_cast<::vendor::unique::smartpa::V1_0::ISmartPaCall*>(hw_binder_proxy);
    } else {
        LOG(INFO) << " Creating DriverBase with null proxy.";
    }
    android::vts::DriverBase* result =
        new android::vts::FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall(
            arg);
    if (arg != nullptr) {
        arg->decStrong(arg);
    }
    return result;
}

}
}  // namespace vts
}  // namespace android
