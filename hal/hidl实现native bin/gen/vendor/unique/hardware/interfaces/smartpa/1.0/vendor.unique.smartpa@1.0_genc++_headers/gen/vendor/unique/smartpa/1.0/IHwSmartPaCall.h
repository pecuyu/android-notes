#ifndef HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_IHWSMARTPACALL_H
#define HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_IHWSMARTPACALL_H

#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>

#include <android/hidl/base/1.0/BnHwBase.h>
#include <android/hidl/base/1.0/BpHwBase.h>

#include <hidl/Status.h>
#include <hwbinder/IBinder.h>
#include <hwbinder/Parcel.h>

namespace vendor {
namespace unique {
namespace smartpa {
namespace V1_0 {
}  // namespace V1_0
}  // namespace smartpa
}  // namespace unique
}  // namespace vendor

#endif  // HIDL_GENERATED_VENDOR_UNIQUE_SMARTPA_V1_0_IHWSMARTPACALL_H
