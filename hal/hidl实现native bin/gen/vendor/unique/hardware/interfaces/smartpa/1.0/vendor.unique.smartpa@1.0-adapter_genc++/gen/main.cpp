#include <hidladapter/HidlBinderAdapter.h>
#include <vendor/unique/smartpa/1.0/ASmartPaCall.h>
int main(int argc, char** argv) {
    return ::android::hardware::adapterMain<
        ::vendor::unique::smartpa::V1_0::ASmartPaCall>("vendor.unique.smartpa@1.0", argc, argv);
}
