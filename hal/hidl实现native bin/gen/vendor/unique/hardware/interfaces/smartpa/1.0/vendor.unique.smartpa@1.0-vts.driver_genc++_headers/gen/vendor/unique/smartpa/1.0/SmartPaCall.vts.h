#ifndef __VTS_DRIVER__vendor_unique_smartpa_V1_0_ISmartPaCall__
#define __VTS_DRIVER__vendor_unique_smartpa_V1_0_ISmartPaCall__

#undef LOG_TAG
#define LOG_TAG "FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall"

#include <log/log.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <driver_base/DriverBase.h>
#include <driver_base/DriverCallbackBase.h>

#include <VtsDriverCommUtil.h>

#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
#include <hidl/HidlSupport.h>
#include <android/hidl/base/1.0/types.h>


using namespace vendor::unique::smartpa::V1_0;
namespace android {
namespace vts {

class Vts_vendor_unique_smartpa_V1_0_ISmartPaCall : public ::vendor::unique::smartpa::V1_0::ISmartPaCall, public DriverCallbackBase {
 public:
    Vts_vendor_unique_smartpa_V1_0_ISmartPaCall(const string& callback_socket_name)
        : callback_socket_name_(callback_socket_name) {};

    virtual ~Vts_vendor_unique_smartpa_V1_0_ISmartPaCall() = default;

    ::android::hardware::Return<bool> doSmartPa(
        uint32_t arg0) override;


 private:
    string callback_socket_name_;
};

sp<::vendor::unique::smartpa::V1_0::ISmartPaCall> VtsFuzzerCreateVts_vendor_unique_smartpa_V1_0_ISmartPaCall(const string& callback_socket_name);

class FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall : public DriverBase {
 public:
    FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall() : DriverBase(HAL_HIDL), hw_binder_proxy_() {}

    explicit FuzzerExtended_vendor_unique_smartpa_V1_0_ISmartPaCall(::vendor::unique::smartpa::V1_0::ISmartPaCall* hw_binder_proxy) : DriverBase(HAL_HIDL), hw_binder_proxy_(hw_binder_proxy) {}
    uint64_t GetHidlInterfaceProxy() const {
        return reinterpret_cast<uintptr_t>(hw_binder_proxy_.get());
    }
 protected:
    bool Fuzz(FunctionSpecificationMessage* func_msg, void** result, const string& callback_socket_name);
    bool CallFunction(const FunctionSpecificationMessage& func_msg, const string& callback_socket_name, FunctionSpecificationMessage* result_msg);
    bool VerifyResults(const FunctionSpecificationMessage& expected_result, const FunctionSpecificationMessage& actual_result);
    bool GetAttribute(FunctionSpecificationMessage* func_msg, void** result);
    bool GetService(bool get_stub, const char* service_name);

 private:
    sp<::vendor::unique::smartpa::V1_0::ISmartPaCall> hw_binder_proxy_;
};


extern "C" {
extern android::vts::DriverBase* vts_func_4_vendor_unique_smartpa_V1_0_ISmartPaCall_();
extern android::vts::DriverBase* vts_func_4_vendor_unique_smartpa_V1_0_ISmartPaCall_with_arg(uint64_t hw_binder_proxy);
}
}  // namespace vts
}  // namespace android
#endif
