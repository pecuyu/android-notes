out/soong/.intermediates/vendor/unique/hardware/interfaces/smartpa/1.0/
├── vendor.unique.smartpa@1.0
│   ├── android_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0.so
│   │   │   └── vendor.unique.smartpa@1.0.so.rsp
│   │   ├── vendor.unique.smartpa@1.0.so
│   │   ├── vendor.unique.smartpa@1.0.so.d
│   │   ├── vendor.unique.smartpa@1.0.so.toc
│   │   └── vendor.unique.smartpa@1.0.so.toc.d
│   ├── android_arm_armv8-a_cortex-a53_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0.a
│   │   └── vendor.unique.smartpa@1.0.a.rsp
│   ├── android_arm_armv8-a_cortex-a53_static_fuzzer
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0.a
│   │   └── vendor.unique.smartpa@1.0.a.rsp
│   ├── android_recovery_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0.so
│   │   │   └── vendor.unique.smartpa@1.0.so.rsp
│   │   ├── vendor.unique.smartpa@1.0.so
│   │   ├── vendor.unique.smartpa@1.0.so.d
│   │   ├── vendor.unique.smartpa@1.0.so.toc
│   │   └── vendor.unique.smartpa@1.0.so.toc.d
│   ├── android_recovery_arm_armv8-a_cortex-a53_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0.a
│   │   └── vendor.unique.smartpa@1.0.a.rsp
│   ├── android_vendor.30_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0.so
│   │   │   └── vendor.unique.smartpa@1.0.so.rsp
│   │   ├── vendor.unique.smartpa@1.0.so
│   │   ├── vendor.unique.smartpa@1.0.so.d
│   │   ├── vendor.unique.smartpa@1.0.so.toc
│   │   └── vendor.unique.smartpa@1.0.so.toc.d
│   ├── android_vendor.30_arm_armv8-a_cortex-a53_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0.a
│   │   └── vendor.unique.smartpa@1.0.a.rsp
│   ├── linux_glibc_x86_64_shared
│   │   ├── vendor.unique.smartpa@1.0.so
│   │   ├── vendor.unique.smartpa@1.0.so.rsp
│   │   ├── vendor.unique.smartpa@1.0.so.toc
│   │   └── vendor.unique.smartpa@1.0.so.toc.d
│   ├── linux_glibc_x86_64_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0.a
│   │   └── vendor.unique.smartpa@1.0.a.rsp
│   ├── linux_glibc_x86_shared
│   │   ├── vendor.unique.smartpa@1.0.so
│   │   ├── vendor.unique.smartpa@1.0.so.rsp
│   │   ├── vendor.unique.smartpa@1.0.so.toc
│   │   └── vendor.unique.smartpa@1.0.so.toc.d
│   └── linux_glibc_x86_static
│       ├── obj
│       ├── vendor.unique.smartpa@1.0.a
│       └── vendor.unique.smartpa@1.0.a.rsp
├── vendor.unique.smartpa@1.0-adapter
│   └── android_arm_armv8-a_cortex-a53
│       ├── obj
│       ├── unstripped
│       │   ├── vendor.unique.smartpa@1.0-adapter
│       │   └── vendor.unique.smartpa@1.0-adapter.rsp
│       ├── vendor.unique.smartpa@1.0-adapter
│       ├── vendor.unique.smartpa@1.0-adapter.config
│       └── vendor.unique.smartpa@1.0-adapter.d
├── vendor.unique.smartpa@1.0-adapter_genc++
│   └── gen
│       ├── main.cpp
│       └── main.cpp.d
├── vendor.unique.smartpa@1.0-adapter-helper
│   ├── android_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so
│   │   │   └── vendor.unique.smartpa@1.0-adapter-helper.so.rsp
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so.d
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so.toc
│   │   └── vendor.unique.smartpa@1.0-adapter-helper.so.toc.d
│   ├── android_arm_armv8-a_cortex-a53_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.a
│   │   └── vendor.unique.smartpa@1.0-adapter-helper.a.rsp
│   ├── android_vendor.30_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so
│   │   │   └── vendor.unique.smartpa@1.0-adapter-helper.so.rsp
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so.d
│   │   ├── vendor.unique.smartpa@1.0-adapter-helper.so.toc
│   │   └── vendor.unique.smartpa@1.0-adapter-helper.so.toc.d
│   └── android_vendor.30_arm_armv8-a_cortex-a53_static
│       ├── obj
│       ├── vendor.unique.smartpa@1.0-adapter-helper.a
│       └── vendor.unique.smartpa@1.0-adapter-helper.a.rsp
├── vendor.unique.smartpa@1.0-adapter-helper_genc++
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       ├── ASmartPaCall.cpp
│                       └── ASmartPaCall.cpp.d
├── vendor.unique.smartpa@1.0-adapter-helper_genc++_headers
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       ├── ASmartPaCall.h
│                       └── ASmartPaCall.h.d
├── vendor.unique.smartpa@1.0_genc++
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       ├── SmartPaCallAll.cpp
│                       └── SmartPaCallAll.cpp.d
├── vendor.unique.smartpa@1.0_genc++_headers
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       ├── BnHwSmartPaCall.h
│                       ├── BpHwSmartPaCall.h
│                       ├── BsSmartPaCall.h
│                       ├── IHwSmartPaCall.h
│                       ├── ISmartPaCall.h
│                       └── ISmartPaCall.h.d
├── vendor.unique.smartpa@1.0-vts.driver
│   ├── android_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0-vts.driver.so
│   │   │   └── vendor.unique.smartpa@1.0-vts.driver.so.rsp
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so.d
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so.toc
│   │   └── vendor.unique.smartpa@1.0-vts.driver.so.toc.d
│   ├── android_arm_armv8-a_cortex-a53_shared_fuzzer
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0-vts.driver.so
│   │   │   └── vendor.unique.smartpa@1.0-vts.driver.so.rsp
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so.d
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.so.toc
│   │   └── vendor.unique.smartpa@1.0-vts.driver.so.toc.d
│   ├── android_arm_armv8-a_cortex-a53_static
│   │   ├── obj
│   │   ├── vendor.unique.smartpa@1.0-vts.driver.a
│   │   └── vendor.unique.smartpa@1.0-vts.driver.a.rsp
│   └── android_arm_armv8-a_cortex-a53_static_fuzzer
│       └── obj
├── vendor.unique.smartpa@1.0-vts.driver_genc++
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       └── SmartPaCall.vts.cpp
├── vendor.unique.smartpa@1.0-vts.driver_genc++_headers
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       └── SmartPaCall.vts.h
├── vendor.unique.smartpa@1.0-vts.fuzzer
│   └── android_arm_armv8-a_cortex-a53_fuzzer
│       ├── config
│       │   └── config.json
│       ├── data
│       │   └── vendor
│       │       └── unique
│       │           └── smartpa
│       │               └── 1.0
│       │                   └── SmartPaCall.vts
│       ├── obj
│       │   └── test
│       │       └── vts-testcase
│       │           └── fuzz
│       │               └── iface_fuzzer
│       │                   ├── ProtoFuzzerMain.o
│       │                   ├── ProtoFuzzerMain.o.d
│       │                   ├── ProtoFuzzerMutateFns.o
│       │                   ├── ProtoFuzzerMutateFns.o.d
│       │                   ├── ProtoFuzzerMutator.o
│       │                   ├── ProtoFuzzerMutator.o.d
│       │                   ├── ProtoFuzzerRunner.o
│       │                   ├── ProtoFuzzerRunner.o.d
│       │                   ├── ProtoFuzzerStaticParams.o
│       │                   ├── ProtoFuzzerStaticParams.o.d
│       │                   ├── ProtoFuzzerStats.o
│       │                   ├── ProtoFuzzerStats.o.d
│       │                   ├── ProtoFuzzerUtils.o
│       │                   └── ProtoFuzzerUtils.o.d
│       ├── unstripped
│       │   ├── vendor.unique.smartpa@1.0-vts.fuzzer
│       │   └── vendor.unique.smartpa@1.0-vts.fuzzer.rsp
│       ├── vendor.unique.smartpa@1.0-vts.fuzzer
│       └── vendor.unique.smartpa@1.0-vts.fuzzer.d
├── vendor.unique.smartpa@1.0-vts.profiler
│   ├── android_arm_armv8-a_cortex-a53_shared
│   │   ├── unstripped
│   │   │   ├── vendor.unique.smartpa@1.0-vts.profiler.so
│   │   │   └── vendor.unique.smartpa@1.0-vts.profiler.so.rsp
│   │   ├── vendor.unique.smartpa@1.0-vts.profiler.so
│   │   ├── vendor.unique.smartpa@1.0-vts.profiler.so.d
│   │   ├── vendor.unique.smartpa@1.0-vts.profiler.so.toc
│   │   └── vendor.unique.smartpa@1.0-vts.profiler.so.toc.d
│   └── android_arm_armv8-a_cortex-a53_static
│       ├── obj
│       ├── vendor.unique.smartpa@1.0-vts.profiler.a
│       └── vendor.unique.smartpa@1.0-vts.profiler.a.rsp
├── vendor.unique.smartpa@1.0-vts.profiler_genc++
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       └── SmartPaCall.vts.cpp
├── vendor.unique.smartpa@1.0-vts.profiler_genc++_headers
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       └── SmartPaCall.vts.h
├── vendor.unique.smartpa@1.0-vts.spec
│   └── gen
│       └── vendor
│           └── unique
│               └── smartpa
│                   └── 1.0
│                       ├── SmartPaCall.vts
│                       └── SmartPaCall.vts.d
├── vendor.unique.smartpa-V1.0-java
│   └── android_common
│       ├── aligned
│       │   └── vendor.unique.smartpa-V1.0-java.jar
│       ├── combined
│       │   └── vendor.unique.smartpa-V1.0-java.jar
│       ├── dex
│       │   ├── classes.dex
│       │   ├── classes.dex.jar
│       │   └── vendor.unique.smartpa-V1.0-java.jar
│       ├── javac
│       │   ├── anno
│       │   ├── classes
│       │   │   └── vendor
│       │   │       └── unique
│       │   │           └── smartpa
│       │   │               └── V1_0
│       │   │                   ├── ISmartPaCall.class
│       │   │                   ├── ISmartPaCall$Proxy.class
│       │   │                   └── ISmartPaCall$Stub.class
│       │   ├── vendor.unique.smartpa-V1.0-java.jar
│       │   └── vendor.unique.smartpa-V1.0-java.jar.rsp
│       ├── turbine
│       │   ├── classes
│       │   ├── vendor.unique.smartpa-V1.0-java.jar
│       │   └── vendor.unique.smartpa-V1.0-java.jar.rsp
│       └── turbine-combined
│           └── vendor.unique.smartpa-V1.0-java.jar
├── vendor.unique.smartpa-V1.0-java_gen_java
│   └── gen
│       ├── srcs
│       │   └── vendor
│       │       └── unique
│       │           └── smartpa
│       │               └── V1_0
│       │                   └── ISmartPaCall.java
│       ├── srcs.srcjar
│       └── srcs.srcjar.d
└── vendor.unique.smartpa-V1.0-java-shallow
    └── android_common
        ├── aligned
        │   └── vendor.unique.smartpa-V1.0-java-shallow.jar
        ├── dex
        │   ├── classes.dex
        │   ├── classes.dex.jar
        │   └── vendor.unique.smartpa-V1.0-java-shallow.jar
        ├── javac
        │   ├── anno
        │   ├── classes
        │   │   └── vendor
        │   │       └── unique
        │   │           └── smartpa
        │   │               └── V1_0
        │   │                   ├── ISmartPaCall.class
        │   │                   ├── ISmartPaCall$Proxy.class
        │   │                   └── ISmartPaCall$Stub.class
        │   ├── vendor.unique.smartpa-V1.0-java-shallow.jar
        │   └── vendor.unique.smartpa-V1.0-java-shallow.jar.rsp
        ├── turbine
        │   ├── classes
        │   ├── vendor.unique.smartpa-V1.0-java-shallow.jar
        │   └── vendor.unique.smartpa-V1.0-java-shallow.jar.rsp
        └── turbine-combined
            └── vendor.unique.smartpa-V1.0-java-shallow.jar

154 directories, 149 files
