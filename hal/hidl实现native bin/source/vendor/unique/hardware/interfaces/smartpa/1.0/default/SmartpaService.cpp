#define LOG_TAG "SmartpaService"

#include <log/log.h>
#include <hidl/HidlTransportSupport.h>
#include "SmartPaCall.h"  // include vendor/unique/smartpa/1.0/ISmartPaCall.h


// libhidl HidlTransportSupport.h
using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;

// hidl cpp interface and its implementation
using ::vendor::unique::smartpa::V1_0::ISmartPaCall;
using ::vendor::unique::smartpa::implementation::SmartPaCall;

using ::android::sp;
using ::android::OK;
using ::android::status_t;


int main()
{
    sp<ISmartPaCall> service = new SmartPaCall();
    
    if(service == nullptr) {
        ALOGE("Failed to create Smartpa service..");
        return -1;
    }
    
    /*Configures the threadpool used for handling incoming RPC calls in this process.
      This method MUST be called before interacting with any HIDL interfaces,
      including the IFoo::getService and IFoo::registerAsService methods. */
    configureRpcThreadpool(1, true /* callerWillJoin */);

    status_t status = service->registerAsService(); // 注册binder服务
    if(status != OK) {
        ALOGE("Failed to register Smartpa service..");
        return -1; 
    }
    
    ALOGD("Smartpa service started successfully.");
    
    /* Joins a threadpool that you configured earlier with configureRpcThreadPool(x, true);*/
    joinRpcThreadpool();
    
    return 0;
}
