// FIXME: your file license if you have one

#pragma once

#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace vendor::unique::smartpa::implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct SmartPaCall : public V1_0::ISmartPaCall {
    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    Return<bool> doSmartPa(uint32_t retryCount) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" ISmartPaCall* HIDL_FETCH_ISmartPaCall(const char* name);

}  // namespace vendor::unique::smartpa::implementation
