// FIXME: your file license if you have one

#include "SmartPaCall.h"
#include <log/log.h>

namespace vendor::unique::smartpa::implementation {

// Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
Return<bool> SmartPaCall::doSmartPa(uint32_t retryCount) {
    // TODO implement
    ALOGD("SmartPaCall::doSmartPa retryCount=%d",retryCount);
    return bool {};
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//ISmartPaCall* HIDL_FETCH_ISmartPaCall(const char* /* name */) {
    //return new SmartPaCall();
//}
//
}  // namespace vendor::unique::smartpa::implementation
