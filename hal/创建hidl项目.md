## 创建项目根目录
unique 目录通常是公司或组织的名字. hardware/interfaces 通常用以表示目录下是hidl项目



```
mkdir -p vendor/unique/hardware/interfaces
```
    

## 创建项目目录
smartpa 是项目的名字. 1.0 是hidl项目的版本号


```
mkdir -p  vendor/unique/hardware/interfaces/smartpa/1.0
```


## 创建.hal文件
定义ISmartPaCall.hal

```
package vendor.unique.smartpa@1.0;

interface ISmartPaCall {
     // 定义一个doSmartPa , generates 后面定义的是返回值
    doSmartPa(uint32_t retryCount) generates (bool success);
};  // 注意此处的;
```


## 创建根目录映射
注意到上面ISmartPaCall.hal 文件的package是vendor.unique.smartpa@1.0 , 而它所在的目录是vendor/unique/hardware/interfaces/smartpa/1.0.  因此需要建立两者映射关系. 对比来看,只需要 vendor.unique 和 vendor/unique/hardware/interfaces/ 建立映射关系即可.

我们需要在 vendor/unique/hardware/interfaces/ 目录下创建一个 Android.bp 文件，在这个文件中创建这个映射关系。内容如下:


```
hidl_package_root {
    name: "vendor.unique",
    path: "vendor/unique/hardware/interfaces",
}
```



此时vendor/unique/hardware/interfaces下的文件目录 


```
├── Android.bp    // 根目录映射
    └── smartpa
        └── 1.0
            └── ISmartPaCall.hal
```



## 生成项目bp文件
目前针对1.0项目还没有编译脚本. 使用如下命令生成对应的bp文件: 
    
    hidl-gen -Landroidbp -r vendor.unique:vendor/unique/hardware/interfaces  -r android.hidl:system/libhidl/transport vendor.unique.smartpa@1.0
    
执行完命令后的目录结构:


```
├── Android.bp    // 根目录映射
    └── smartpa
        └── 1.0
            ├── Android.bp       // 1.0版本的编译脚本
            └── ISmartPaCall.hal
```



生成的bp文件内容如下:


```
// This file is autogenerated by hidl-gen -Landroidbp.
    
hidl_interface {
    name: "vendor.unique.smartpa@1.0",
    root: "vendor.unique",
    system_ext_specific: true,
    srcs: [
        "ISmartPaCall.hal",
    ],
    interfaces: [
        "android.hidl.base@1.0",
    ],
    gen_java: true,  // 是否生成java层代码
}
```



另外一种方法是修改 hardware/interfaces/update-makefiles.sh 脚本, 添加如下内容


```
do_makefiles_update \
  "android.hardware:hardware/interfaces" \
  "android.hidl:system/libhidl/transport"
    
# 添加来生成vendor/unique/hardware/interfaces目录下的
+do_makefiles_update \
+  "vendor.unique:vendor/unique/hardware/interfaces" \
+  "android.hidl:system/libhidl/transport"
```

    

然后在android目录下执行此脚本即可自动生成.


## 编译1.0版本

lunch项目后, 执行编译命令

    mmm vendor/unique/hardware/interfaces/smartpa/1.0

生成文件目录:

    out/soong/.intermediates/vendor/unique/hardware/interfaces/smartpa/1.0/


目录下的子目录:
    

```
vendor.unique.smartpa@1.0
vendor.unique.smartpa@1.0-adapter-helper_genc++_headers
vendor.unique.smartpa@1.0-vts.driver_genc++_headers
vendor.unique.smartpa@1.0-vts.spec
vendor.unique.smartpa@1.0-adapter 
vendor.unique.smartpa@1.0_genc++   // cpp实现
vendor.unique.smartpa@1.0-vts.fuzzer       
vendor.unique.smartpa-V1.0-java
vendor.unique.smartpa@1.0-adapter_genc++  
vendor.unique.smartpa@1.0_genc++_headers    // cpp头文件 
vendor.unique.smartpa@1.0-vts.profiler                
vendor.unique.smartpa-V1.0-java_gen_java   // java接口文件
vendor.unique.smartpa@1.0-adapter-helper  
vendor.unique.smartpa@1.0-vts.driver       
vendor.unique.smartpa@1.0-vts.profiler_genc++     
vendor.unique.smartpa-V1.0-java-shallow
vendor.unique.smartpa@1.0-adapter-helper_genc++ 
vendor.unique.smartpa@1.0-vts.driver_genc++          
vendor.unique.smartpa@1.0-vts.profiler_genc++_headers
```

    

vendor.unique.smartpa@1.0_genc++_headers 目录下生成的是标准的头文件


```
BnHwSmartPaCall.h     // binder server
BpHwSmartPaCall.h     // binder proxy
BsSmartPaCall.h       // passthrough
IHwSmartPaCall.h  
ISmartPaCall.h        // binder common interface
```


vendor.unique.smartpa@1.0_genc++ 目录下生成实现文件

    SmartPaCallAll.cpp

vendor.unique.smartpa-V1.0-java_gen_java 目录下生成的是java 接口文件

    ISmartPaCall.java


官方生成文件描述 https://source.android.google.cn/devices/architecture/hidl-cpp/packages

> HIDL 软件包中自动生成的文件会关联到与该软件包同名的单个共享库（例如 android.hardware.samples@1.0）。该共享库还会导出单个头文件 IFoo.h，它可以包含在客户端和服务器中。在 Binder 化模式下，使用 hidl-gen 编译器以 IFoo.hal 接口文件作为输入会自动生成以下文件

此处的共享库是 vendor.unique.smartpa@1.0.so , 客户端和服务端都将链接此库.

    
在 Android.mk 中：
    
    LOCAL_SHARED_LIBRARIES += android.hardware.samples@1.0
   
在 Android.bp 中：
    

```
shared_libs: [
        /* ... */
        "android.hardware.samples@1.0",
    ],
```



## 实现服务端代码

首先创建default目录, 默认实现将在此目录:

       mkdir -p  vendor/unique/hardware/interfaces/smartpa/1.0/default
      

使用命令生成样板代码, 提供了一个服务端默认的实现

  
```
hidl-gen -o vendor/unique/hardware/interfaces/smartpa/1.0/default/ -L c++-impl  -r vendor.unique:vendor/unique/hardware/interfaces -r android.hidl:system/libhidl/transport vendor.unique.smartpa@1.0
```

生成的文件内容如下:
- SmartPaCall.h

    

```
#include <vendor/unique/smartpa/1.0/ISmartPaCall.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace vendor::unique::smartpa::implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct SmartPaCall : public V1_0::ISmartPaCall {
    // Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
    Return<bool> doSmartPa(int32_t retryCount) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" ISmartPaCall* HIDL_FETCH_ISmartPaCall(const char* name);

}  // namespace vendor::unique::smartpa::implementation
```


- SmartPaCall.cpp


```
#include "SmartPaCall.h"
    
namespace vendor::unique::smartpa::implementation {

// Methods from ::vendor::unique::smartpa::V1_0::ISmartPaCall follow.
Return<bool> SmartPaCall::doSmartPa(int32_t retryCount) { // doSmartPa 默认实现
    // TODO implement
    return bool {};
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//ISmartPaCall* HIDL_FETCH_ISmartPaCall(const char* /* name */) {
    //return new SmartPaCall();
//}
//
}  // namespace vendor::unique::smartpa::implementation
```

    
给default目录添加编译bp文件


```
hidl-gen -o vendor/unique/hardware/interfaces/smartpa/1.0/default/ -L androidbp-impl -r vendor.unique:vendor/unique/hardware/interfaces -r android.hidl:system/libhidl/transport vendor.unique.smartpa@1.0
```


这个bp文件将要生成一个名为 vendor.unique.smartpa@1.0-impl.so 的共享库.  这个库对于直通式(passthrough hal)的比较有用. 而对于binder化(binderized) 的实现, 通常会将service端实现成一个bin文件, 并添加rc启动项.

下面是生成的bp文件, 描述的很清楚:


```
cc_library_shared {
    // FIXME: this should only be -impl for a passthrough hal.
    // In most cases, to convert this to a binderized implementation, you should:
    // - change '-impl' to '-service' here and make it a cc_binary instead of a
    //   cc_library_shared.
    // - add a *.rc file for this module.
    // - delete HIDL_FETCH_I* functions.
    // - call configureRpcThreadpool and registerAsService on the instance.
    // You may also want to append '-impl/-service' with a specific identifier like
    // '-vendor' or '-<hardware identifier>' etc to distinguish it.
    name: "vendor.unique.smartpa@1.0-impl",
    relative_install_path: "hw",
    // FIXME: this should be 'vendor: true' for modules that will eventually be
    // on AOSP.
    proprietary: true,
    srcs: [
        "SmartPaCall.cpp",
    ],
    shared_libs: [
        "libhidlbase",
        "libutils",
        "vendor.unique.smartpa@1.0",
    ],
}
```




经过上面的操作,目录结构如下:

 
```
├── Android.bp
    └── smartpa
        ├── 1.0
        │   ├── Android.bp
        │   ├── default
        │   │   ├── Android.bp
        │   │   ├── SmartPaCall.cpp
        │   │   └── SmartPaCall.h
        │   └── ISmartPaCall.hal
        └── readme
```



编译一下default模块

    
    mmm vendor/unique/hardware/interfaces/smartpa/1.0/default/


生成如下so

    out/target/product/msm8937_32/vendor/lib/hw/vendor.unique.smartpa@1.0-impl.so
    

## 编写服务端可执行程序
这个就是之前提到的,会将service端实现成一个bin文件. 现在在default目录下新建一个SmartpaService.cpp, 编写内容如下:


```
#define LOG_TAG "SmartpaService"

#include <log/log.h>
#include <hidl/HidlTransportSupport.h>
#include "SmartPaCall.h"  // include vendor/unique/smartpa/1.0/ISmartPaCall.h


// libhidl HidlTransportSupport.h
using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;

// hidl cpp interface and its implementation
using ::vendor::unique::smartpa::V1_0::ISmartPaCall;
using ::vendor::unique::smartpa::implementation::SmartPaCall;

using ::android::sp;
using ::android::OK;
using ::android::status_t;


int main()
{
    sp<ISmartPaCall> service = new SmartPaCall();
    
    if(service == nullptr) {
        ALOGE("Failed to create Smartpa service..");
        return -1;
    }
    
    /*Configures the threadpool used for handling incoming RPC calls in this process.
      This method MUST be called before interacting with any HIDL interfaces,
      including the IFoo::getService and IFoo::registerAsService methods. */
    configureRpcThreadpool(1, true /* callerWillJoin */);

    status_t status = service->registerAsService(); // 注册binder服务
    if(status != OK) {
        ALOGE("Failed to register Smartpa service..");
        return -1; 
    }
    
    ALOGD("Smartpa service started successfully.");
    
    /* Joins a threadpool that you configured earlier with configureRpcThreadPool(x, true);*/
    joinRpcThreadpool();
    
    return 0;
}

```

接下来添加bin编译, 在default目录的Android.bp 添加如下代码:


```

cc_binary {
    name: "vendor.unique.smartpa@1.0-service",
    relative_install_path: "hw",
    proprietary: true,
    srcs: [
        "SmartPaCall.cpp",
        "SmartpaService.cpp",
    ],
    shared_libs: [
        "libhidlbase",
        "libhidltransport",
        "libutils",
        "liblog",
        "vendor.unique.smartpa@1.0",
    ],
}

```

执行编译命令


```
mmm vendor/unique/hardware/interfaces/smartpa/1.0/default/
```

生成的service bin文件的路径:

```
out/target/product/msm8937_32/vendor/bin/hw/vendor.unique.smartpa@1.0-service
```

看一下system和vender目录的所有生成文件:


```
$ find out/target/product/msm8937_32/vendor -name vendor.unique.smartpa*
out/target/product/msm8937_32/vendor/bin/hw/vendor.unique.smartpa@1.0-service  --> service bin
out/target/product/msm8937_32/vendor/lib/vendor.unique.smartpa@1.0.so       --> 通信层c++代码库
out/target/product/msm8937_32/vendor/lib/vendor.unique.smartpa@1.0-adapter-helper.so
out/target/product/msm8937_32/vendor/lib/hw/vendor.unique.smartpa@1.0-impl.so    --> service端实现的库

$ find out/target/product/msm8937_32/system -name vendor.unique.smartpa*
out/target/product/msm8937_32/system/system_ext/framework/vendor.unique.smartpa-V1.0-java.jar   --> 通信层java库
out/target/product/msm8937_32/system/system_ext/framework/vendor.unique.smartpa-V1.0-java-shallow.jar
out/target/product/msm8937_32/system/system_ext/lib/vendor.unique.smartpa@1.0.so
out/target/product/msm8937_32/system/system_ext/lib/vendor.unique.smartpa@1.0-adapter-helper.so
out/target/product/msm8937_32/system/system_ext/lib/vendor.unique.smartpa@1.0-vts.profiler.so
out/target/product/msm8937_32/system/system_ext/lib/vendor.unique.smartpa@1.0-vts.driver.so
```


## VINTF 与开机自启动
此时直接在shell中执行 vendor.unique.smartpa@1.0-service , 会发现报错了, 服务也没注册上:

    
```
I/hwservicemanager: getTransport: Cannot find entry vendor.unique.smartpa@1.0::ISmartPaCall/default in either framework or device manifest.
E/HidlServiceManagement: Service vendor.unique.smartpa@1.0::ISmartPaCall/default must be in VINTF manifest in order to register/get.
E/SmartpaService: Failed to register Smartpa service..
```

可以发现hwservicemanager在 framework or device manifest 找不到我们的接口,这是VINTF的限制. 了解更多VINTF的知识，请参考https://source.android.google.cn/devices/architecture/vintf
针对以上问题需要进行配置. 在default目录下创建vendor.unique.smartpa@1.0-service.xml, 添加如下内容:


```
<manifest version="1.0" type="device">
    <hal format="hidl">
        <name>vendor.unique.smartpa</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>ISmartPaCall</name>
            <instance>default</instance>
        </interface>
    </hal>
</manifest>

```

同时,需要将此文件到bp文件进行配置, 由 vintf_fragments 指定文件名称.

下面来处理另外一个问题. 上面service可执行文件不是自启动的, 需要我们自己添加rc文件, 如此它将被init拉起. 创建vendor.unique.smartpa@1.0-service.rc 如下:


```
service SmartpaService /vendor/bin/hw/vendor.unique.smartpa@1.0-service
    class hal
    user system
    group system

```

也需要进行配置, 使用 init_rc 来指定. 下面看都修改好了的编译脚本:


```
cc_binary {
    name: "vendor.unique.smartpa@1.0-service",
    relative_install_path: "hw",
    proprietary: true,
    vintf_fragments: ["vendor.unique.smartpa@1.0-service.xml"],
    init_rc: ["vendor.unique.smartpa@1.0-service.rc"],
    srcs: [
        "SmartPaCall.cpp",
        "SmartpaService.cpp",
    ],
    shared_libs: [
        "libhidlbase",
        "libhidltransport",
        "libutils",
        "liblog",
        "vendor.unique.smartpa@1.0",
    ],
}

```

重新编译后,多生成的文件:


```
out/target/product/msm8937_32/vendor/etc/init/vendor.unique.smartpa@1.0-service.rc
out/target/product/msm8937_32/vendor/etc/vintf/manifest/vendor.unique.smartpa@1.0-service.xml
```

将所有生成的文件全部push到对应的目录然后重启. 发现服务没有启动起来,抓dmesg可以发现如下错误

```
init: Could not start service 'SmartpaService' as part of class 'hal': File /vendor/bin/hw/vendor.unique.smartpa@1.0-service(labeled "u:object_r:vendor_file:s0") has incorrect label or no domain transition from u:r:init:s0 to another SELinux domain defined. Have you configured your service correctly? https://source.android.com/security/selinux/device-policy#label_new_services_and_address_denials
```

这是情况说明缺少selabel

## 添加selinux 权限

创建文件 hal_smartpa_service_default.te


```
#Define Domain
type vendor_hal_smartpa_service_default, domain;
type vendor_hal_smartpa_service_default_exec, exec_type, vendor_file_type, file_type;

hal_server_domain(vendor_hal_smartpa_service_default, vendor_hal_smartpa_service)

init_daemon_domain(vendor_hal_smartpa_service_default)

```

修改 file_context 添加:


```
/(vendor|system/vendor)/bin/hw/vendor\.vendor\.unique\.smartpa@1\.0-service u:object_r:vendor_hal_smartpa_service_default_exec:s0
```

## Vendor VINTF

```
check_vintf.cpp:554] files are incompatible: The following instances are in the device manifest but not specified in framework compatibility matrix
```
修改如下文件进行配置
- vendor/qcom/opensource/core-utils/vendor_framework_compatibility_matrix.xml

```
    <hal format="hidl">
        <name>vendor.unique.smartpa</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>ISmartPaCall</name>
            <instance>default</instance>
        </interface>
    </hal>

```

## 参考
- https://source.android.google.cn/devices/architecture/hidl
- https://juejin.cn/post/6921604765843783688
