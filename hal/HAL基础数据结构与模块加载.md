# HAL 基础数据结构

三个基础数据结构：

	struct hw_module_t;
	struct hw_module_methods_t;
	struct hw_device_t;

## hw_module_methods_t
此结构体用来声明一个通用的接口，不同模块实现此open方法，就可以用来打开模块指定的设备，device是一个传出参数，返回给调用者：

	typedef struct hw_module_methods_t {
	    /** Open a specific device */
	    int (*open)(const struct hw_module_t* module, const char* id,
	            struct hw_device_t** device);
	
	} hw_module_methods_t;

## hw_module_t

struct hw_module_t 用来定义一个硬件模块，每个硬件模块都必须有一个名字为HAL_MODULE_INFO_SYM的数据结构变量，并且这个数据结构的起始数据成员必须以hw_module_t开始：


	/**
	 * Name of the hal_module_info
	 */
	#define HAL_MODULE_INFO_SYM         HMI
	
	/**
	 * Name of the hal_module_info as a string
	 */
	#define HAL_MODULE_INFO_SYM_AS_STR  "HMI"


	
	/**
	 * Every hardware module must have a data structure named HAL_MODULE_INFO_SYM
	 * and the fields of this data structure must begin with hw_module_t
	 * followed by module specific information.
	 */
	typedef struct hw_module_t {
	    /** tag must be initialized to HARDWARE_MODULE_TAG */
	    uint32_t tag;
	
	    /**
	     * The API version of the implemented module. The module owner is
	     * responsible for updating the version when a module interface has
	     * changed.
	     *
	     * The derived modules such as gralloc and audio own and manage this field.
	     * The module user must interpret the version field to decide whether or
	     * not to inter-operate with the supplied module implementation.
	     * For example, SurfaceFlinger is responsible for making sure that
	     * it knows how to manage different versions of the gralloc-module API,
	     * and AudioFlinger must know how to do the same for audio-module API.
	     *
	     * The module API version should include a major and a minor component.
	     * For example, version 1.0 could be represented as 0x0100. This format
	     * implies that versions 0x0100-0x01ff are all API-compatible.
	     *
	     * In the future, libhardware will expose a hw_get_module_version()
	     * (or equivalent) function that will take minimum/maximum supported
	     * versions as arguments and would be able to reject modules with
	     * versions outside of the supplied range.
	     */
	    uint16_t module_api_version;
	#define version_major module_api_version
	    /**
	     * version_major/version_minor defines are supplied here for temporary
	     * source code compatibility. They will be removed in the next version.
	     * ALL clients must convert to the new version format.
	     */
	
	    /**
	     * The API version of the HAL module interface. This is meant to
	     * version the hw_module_t, hw_module_methods_t, and hw_device_t
	     * structures and definitions.
	     *
	     * The HAL interface owns this field. Module users/implementations
	     * must NOT rely on this value for version information.
	     *
	     * Presently, 0 is the only valid value.
	     */
	    uint16_t hal_api_version;
	#define version_minor hal_api_version
	
	    /** Identifier of module */
	    const char *id;
	
	    /** Name of this module */
	    const char *name;
	
	    /** Author/owner/implementor of the module */
	    const char *author;
	
	    /** Modules methods */
	    struct hw_module_methods_t* methods;  // 模块方法
	
	    /** module's dso */
	    void* dso;	// 保存打开的.so的指针
	
	#ifdef __LP64__
	    uint64_t reserved[32-7];
	#else
	    /** padding to 128 bytes, reserved for future use */
	    uint32_t reserved[32-7];
	#endif
	
	} hw_module_t;


以sensors_module_t为例，如下为sensor模块的数据结构：


	/**
	 * Every hardware module must have a data structure named HAL_MODULE_INFO_SYM
	 * and the fields of this data structure must begin with hw_module_t
	 * followed by module specific information.
	 */
	struct sensors_module_t {
	    struct hw_module_t common;  // 以hw_module_t成员开始
	
	    /**
	     * Enumerate all available sensors. The list is returned in "list".
	     * return number of sensors in the list
	     */
	    int (*get_sensors_list)(struct sensors_module_t* module,
	            struct sensor_t const** list);
	
	    /**
	     *  Place the module in a specific mode. The following modes are defined
	     *
	     *  0 - Normal operation. Default state of the module.
	     *  1 - Loopback mode. Data is injected for the supported
	     *      sensors by the sensor service in this mode.
	     * return 0 on success
	     *         -EINVAL if requested mode is not supported
	     *         -EPERM if operation is not allowed
	     */
	    int (*set_operation_mode)(unsigned int mode);
	};

并且需要定义一个名字为HAL_MODULE_INFO_SYM的模块变量：

	struct sensors_module_t HAL_MODULE_INFO_SYM = {
	    .common = {
	        .tag = HARDWARE_MODULE_TAG,
	        .version_major = 1,
	        .version_minor = 1,
	        .id = SENSORS_HARDWARE_MODULE_ID,
	        .name = "MultiHal Sensor Module",
	        .author = "Google, Inc",
	        .methods = &sensors_module_methods,
	        .dso = NULL,
	        .reserved = {0},
	    },
	    .get_sensors_list = module__get_sensors_list
	};


## hw_device_t

此数据结构用来表示一个硬件设备，每个设备的数据结构必须以hw_device_t成员开头，后面跟着特定的方法和属性：

	/**
	 * Every device data structure must begin with hw_device_t
	 * followed by module specific public methods and attributes.
	 */
	typedef struct hw_device_t {
	    /** tag must be initialized to HARDWARE_DEVICE_TAG */
	    uint32_t tag;
	
	    /**
	     * Version of the module-specific device API. This value is used by
	     * the derived-module user to manage different device implementations.
	     *
	     * The module user is responsible for checking the module_api_version
	     * and device version fields to ensure that the user is capable of
	     * communicating with the specific module implementation.
	     *
	     * One module can support multiple devices with different versions. This
	     * can be useful when a device interface changes in an incompatible way
	     * but it is still necessary to support older implementations at the same
	     * time. One such example is the Camera 2.0 API.
	     *
	     * This field is interpreted by the module user and is ignored by the
	     * HAL interface itself.
	     */
	    uint32_t version;
	
	    /** reference to the module this device belongs to */
	    struct hw_module_t* module; // 指向硬件模块结构
	
	    /** padding reserved for future use */
	#ifdef __LP64__
	    uint64_t reserved[12];
	#else
	    uint32_t reserved[12];
	#endif
	
	    /** Close this device */
	    int (*close)(struct hw_device_t* device); // 用来关闭此硬件设备
	
	} hw_device_t;

以sensor为例：

	
	/*
	 * sensors_poll_device_t is used with SENSORS_DEVICE_API_VERSION_0_1
	 * and is present for backward binary and source compatibility.
	 * See the Sensors HAL interface section for complete descriptions of the
	 * following functions:
	 * http://source.android.com/devices/sensors/index.html#hal
	 */
	struct sensors_poll_device_t {
	    struct hw_device_t common;
	    int (*activate)(struct sensors_poll_device_t *dev,
	            int sensor_handle, int enabled);
	    int (*setDelay)(struct sensors_poll_device_t *dev,
	            int sensor_handle, int64_t sampling_period_ns);
	    int (*poll)(struct sensors_poll_device_t *dev,
	            sensors_event_t* data, int count);
	};


versions >= SENSORS_DEVICE_API_VERSION_1_0


	/*
	 * struct sensors_poll_device_1 is used in HAL versions >= SENSORS_DEVICE_API_VERSION_1_0
	 */
	typedef struct sensors_poll_device_1 {
	    union {
	        /* sensors_poll_device_1 is compatible with sensors_poll_device_t,
	         * and can be down-cast to it
	         */
	        struct sensors_poll_device_t v0;
	
	        struct {
	            struct hw_device_t common;
	
	          
	            int (*activate)(struct sensors_poll_device_t *dev,
	                    int sensor_handle, int enabled);
	
	            int (*setDelay)(struct sensors_poll_device_t *dev,
	                    int sensor_handle, int64_t sampling_period_ns);
	
	            int (*poll)(struct sensors_poll_device_t *dev,
	                    sensors_event_t* data, int count);
	        };
	    };
	
	
	  
	    int (*batch)(struct sensors_poll_device_1* dev,
	            int sensor_handle, int flags, int64_t sampling_period_ns,
	            int64_t max_report_latency_ns);
	
	    
	    int (*flush)(struct sensors_poll_device_1* dev, int sensor_handle);
	
	    int (*inject_sensor_data)(struct sensors_poll_device_1 *dev, const sensors_event_t *data);
	
	    int (*register_direct_channel)(struct sensors_poll_device_1 *dev,
	            const struct sensors_direct_mem_t* mem, int channel_handle);
	
	    int (*config_direct_report)(struct sensors_poll_device_1 *dev,
	            int sensor_handle, int channel_handle, const struct sensors_direct_cfg_t *config);
	
	    /*
	     * Reserved for future use, must be zero.
	     */
	    void (*reserved_procs[5])(void);
	
	} sensors_poll_device_1_t;


# 加载模块

## hw_get_module

传入一个模块id，以及一个hw_module_t的传出参数：
	
	int hw_get_module(const char *id, const struct hw_module_t **module)
	{
	    return hw_get_module_by_class(id, NULL, module);
	}

## hw_get_module_by_class

具体实现在hw_get_module_by_class函数中，此函数首先查找对应的so是否存在，若存在则调用load函数来加载它，否则返回失败：

 动态库的名称格式为 MODULE_ID.variant.so


	/**
	 * There are a set of variant filename for modules. The form of the filename
	 * is "<MODULE_ID>.variant.so" so for the led module the Dream variants 
	 * of base "ro.product.board", "ro.board.platform" and "ro.arch" would be:
	 *
	 * led.trout.so
	 * led.msm7k.so
	 * led.ARMV6.so
	 * led.default.so
	 */
	
	// key数组
	static const char *variant_keys[] = {
	    "ro.hardware",  /* This goes first so that it can pick up a different
	                       file on the emulator. */
	    "ro.product.board",
	    "ro.board.platform",
	    "ro.arch"
	};
	// key数组的长度
	static const int HAL_VARIANT_KEYS_COUNT =(sizeof(variant_keys)/sizeof(variant_keys[0]));
	
	// class_id即是模块的id
	//比如sensor模块  #define SENSORS_HARDWARE_MODULE_ID "sensors"
	int hw_get_module_by_class(const char *class_id, const char *inst, 
												const struct hw_module_t **module)
	{
	    int i = 0;
	    char prop[PATH_MAX] = {0};
	    char path[PATH_MAX] = {0};
	    char name[PATH_MAX] = {0};
	    char prop_name[PATH_MAX] = {0};
	
	
	    if (inst)
	        snprintf(name, PATH_MAX, "%s.%s", class_id, inst);
	    else
	        strlcpy(name, class_id, PATH_MAX);  // 以sensor为例，此时name为“sensors”，带'\0'
	
	    /*
	     * Here we rely on the fact that calling dlopen multiple times on
	     * the same .so will simply increment a refcount (and not load
	     * a new copy of the library).
	     * We also assume that dlopen() is thread-safe.
	     */
	
	    /* First try a property specific to the class and possibly instance */
		// sensor为例，此时为prop_name为 ro.hardware.sensors
	    snprintf(prop_name, sizeof(prop_name), "ro.hardware.%s", name); 
	    if (property_get(prop_name, prop, NULL) > 0) {
	        if (hw_module_exists(path, sizeof(path), name, prop) == 0) {
	            goto found;
	        }
	    }
	
	    /* Loop through the configuration variants looking for a module */
	    for (i=0 ; i<HAL_VARIANT_KEYS_COUNT; i++) {	 // 循环key数组
	        if (property_get(variant_keys[i], prop, NULL) == 0) { // 获取key对应的prop
	            continue;
	        }
	        if (hw_module_exists(path, sizeof(path), name, prop) == 0) { // 查找动态库是否存在，路径存放在path中
	            goto found;
	        }
	    }
	
	    /* Nothing found, try the default */  // 尝试查找默认，class_id.default.so
	    if (hw_module_exists(path, sizeof(path), name, "default") == 0) { 
	        goto found;
	    }
	
	    return -ENOENT;
	
	found:
	    /* load the module, if this fails, we're doomed, and we should not try
	     * to load a different variant. */
	    return load(class_id, path, module);
	}

## hw_module_exists

使用hw_module_exists函数来查找动态库是否存在，会尝试几个路径:

	/** Base path of the hal modules */
	#if defined(__LP64__)
	#define HAL_LIBRARY_PATH1 "/system/lib64/hw"
	#define HAL_LIBRARY_PATH2 "/vendor/lib64/hw"
	#define HAL_LIBRARY_PATH3 "/odm/lib64/hw"
	#else
	#define HAL_LIBRARY_PATH1 "/system/lib/hw"
	#define HAL_LIBRARY_PATH2 "/vendor/lib/hw"
	#define HAL_LIBRARY_PATH3 "/odm/lib/hw"
	#endif

	/*
	 * Check if a HAL with given name and subname exists, if so return 0, otherwise
	 * otherwise return negative.  On success path will contain the path to the HAL.
	 */
	static int hw_module_exists(char *path, size_t path_len, const char *name,
	                            const char *subname)
	{
	    snprintf(path, path_len, "%s/%s.%s.so",
	             HAL_LIBRARY_PATH3, name, subname); /odm/lib/hw/
	    if (access(path, R_OK) == 0)
	        return 0;
	
	    snprintf(path, path_len, "%s/%s.%s.so",
	             HAL_LIBRARY_PATH2, name, subname);  // /vendor/lib/hw
	    if (access(path, R_OK) == 0)
	        return 0;
	
	    snprintf(path, path_len, "%s/%s.%s.so",
	             HAL_LIBRARY_PATH1, name, subname); // /system/lib/hw
	    if (access(path, R_OK) == 0)
	        return 0;
	
	    return -ENOENT;
	}


## load

load函数实现如下，首先打开动态库，然后调用dlsym来找到hw_module_t，参数是动态库的handle以及一个symbol。这个symbol也就是HAL_MODULE_INFO_SYM_AS_STR，在之前定义时可知，它的值为"HMI"。之前自定义模块(比如sensors_module_t)时需要定义一个名字为HAL_MODULE_INFO_SYM变量，这个HAL_MODULE_INFO_SYM即是HMI，并且模块要以hw_module_t类型变量开始，由此使用symbol查询得到的实际上hw_module_t模块的地址：

	/**
	 * Load the file defined by the variant and if successful
	 * return the dlopen handle and the hmi.
	 * @return 0 = success, !0 = failure.
	 */
	static int load(const char *id,
	        const char *path,
	        const struct hw_module_t **pHmi)
	{
	    int status = -EINVAL;
	    void *handle = NULL;
	    struct hw_module_t *hmi = NULL;
	
	    /*
	     * load the symbols resolving undefined symbols before
	     * dlopen returns. Since RTLD_GLOBAL is not or'd in with
	     * RTLD_NOW the external symbols will not be global
	     */
	    if (strncmp(path, "/system/", 8) == 0) {  // 在system分区下，直接使用dlopen
	        /* If the library is in system partition, no need to check
	         * sphal namespace. Open it with dlopen.
	         */
	        handle = dlopen(path, RTLD_NOW);
	    } else {	// 否则使用android_load_sphal_library加载
	        handle = android_load_sphal_library(path, RTLD_NOW);
	    }
	    if (handle == NULL) {
	        char const *err_str = dlerror();
	        ALOGE("load: module=%s\n%s", path, err_str?err_str:"unknown");
	        status = -EINVAL;
	        goto done;
	    }
	
	    /* Get the address of the struct hal_module_info. */
	    const char *sym = HAL_MODULE_INFO_SYM_AS_STR;
	    hmi = (struct hw_module_t *)dlsym(handle, sym);  //  得到hw_module_t的指针
	    if (hmi == NULL) {
	        ALOGE("load: couldn't find symbol %s", sym);
	        status = -EINVAL;
	        goto done;
	    }
	
	    /* Check that the id matches */
	    if (strcmp(id, hmi->id) != 0) {
	        ALOGE("load: id=%s != hmi->id=%s", id, hmi->id);
	        status = -EINVAL;
	        goto done;
	    }
	
	    hmi->dso = handle;	// 保存动态库的句柄
	
	    /* success */
	    status = 0;
	
	    done:
	    if (status != 0) {
	        hmi = NULL;
	        if (handle != NULL) {
	            dlclose(handle);
	            handle = NULL;
	        }
	    } else {
	        ALOGV("loaded HAL id=%s path=%s hmi=%p handle=%p",
	                id, path, *pHmi, handle);
	    }
	
	    *pHmi = hmi; // 返回hw_module_t给调用者
	
	    return status;
	}

