# Android Notes

### 介绍
Android 系统相关学习笔记

### 关联
本仓库文章会同步更新到CSDN博客 https://blog.csdn.net/qq_28261343

### 目录说明

- system  表示系统相关的文章
   1. window   表示和window(包含wm,wms等)相关的文章
   2. input    输入事件相关的文章
   3. activity 界面相关
- stability    稳定性相关文的章
- art          Android runtime相关的文章

目录树：

```
.
├── art
│   ├── 线程创建.md
│   └── art虚拟机启动流程.md
├── hal
│   ├── 创建hidl项目.md
│   ├── HAL基础数据结构与模块加载.md
│   └── hidl实现native bin
├── LICENSE
├── README.md
├── stability
│   ├── 常用命令.md
│   ├── Android 12 进程native crash流程分析.md
│   ├── Android 12 应用Java crash流程分析.md
│   ├── Android 12 关机重启流程.md
│   ├── Android coredump解析与常用命令.md
│   ├── EventLogTags
│   │   └── 11
│   ├── imgs
│   │   └── native crash框架概述图.png
│   ├── Protolog
│   │   ├── Protolog启动关闭.md
│   │   ├── README-R.md
│   │   └── R-protolog.conf.json
│   ├── sample_log
│   └── watchdog
│       ├── Watchdog(1) 启动.md
│       ├── Watchdog(2) 工作过程.md
│       ├── Watchdog(3) monitor实现.md
│       ├── Watchdog(4) Trace生成过程.md
│       └── Watchdog(5) 案例分析集.md
├── system
│   ├── activity
│   │   ├── Android10 启动activity event事件.md
│   │   ├── Android R startActivity 流程(二) ActivityTaskManagerService处理.md
│   │   ├── Android R startActivity 流程(三) Resume Activity.md
│   │   └── Android R startActivity 流程(一) client发送启动请求.md
│   ├── binder
│   ├── imgs
│   │   ├── Cpp IBinder 类关系图.mdj
│   │   ├── InputReader投递event.jpg
│   │   ├── input reader.mdj
│   │   └── Main.jpg
│   ├── init
│   │   └── readme-11
│   ├── input
│   │   ├── Android R Input之ANR的产生与显示流程.md
│   │   ├── Android R input 之 InputDispatcher 工作流程.md
│   │   ├── Android R input 之 InputReader 工作流程.md
│   │   ├── Android R InputChannel之发送事件处理反馈.md
│   │   ├── Android R InputManagerService 的建立.md
│   │   ├── Android R PointerEventDispatcher 触摸事件监听帮助类.md
│   │   ├── files
│   │   ├── imgs
│   │   └── InputManagerService的创建与InputChannel的注册.md
│   └── window
│       ├── Android学习笔记之理解Window与WindowManager.md
│       ├── Android Activity之Window的创建过程.md
│       ├── Android R WindowManagerService 添加window过程分析 (二) .md
│       ├── Android R WindowManagerService  添加window过程分析 (三)  之InputChannel的建立.md
│       ├── Android R WindowManagerService 添加window过程分析 (一) .md
│       └── imgs
└── tree.txt
```

### 交流
欢迎指正文章中的相关错误与不足之处.
